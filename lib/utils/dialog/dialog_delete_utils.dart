import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_snack_bar_notify_delete.dart';

class DialogDeleteUtils {
  static createNotify(
      {@required String message,
      @required String positiveLabel,
      Function onPositiveTap,
      Function onPositiveTaps,
      @required String negativeLabel,
      @required String negativeLabels,
      Function onNegativeTap}) {
    Get.dialog(
      WidgetSnackBarNotifyDelete(
        message: message,
        positiveLabel: positiveLabel,
        onPositiveTap: onPositiveTap,
        onNegativeTaps: onPositiveTaps,
        negativeLabels: negativeLabels,
        negativeLabel: negativeLabel,
        onTouchOutsizeEnable: true,
        onNegativeTap: onNegativeTap ??
            () {
              Get.back();
            },
      ),
    );
  }
}
