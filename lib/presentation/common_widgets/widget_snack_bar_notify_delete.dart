import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'widget_logo.dart';

class WidgetSnackBarNotifyDelete extends StatelessWidget {
  final String message;
  final String positiveLabel;
  final Function onPositiveTap;
  final String negativeLabel;
  final String negativeLabels;
  final Function onNegativeTap;
  final Function onNegativeTaps;
  final bool onTouchOutsizeEnable;

  const WidgetSnackBarNotifyDelete(
      {Key key,
      @required this.message,
      @required this.positiveLabel,
      this.onPositiveTap,
      @required this.negativeLabel,
      @required this.negativeLabels,
      this.onNegativeTap,
      this.onNegativeTaps,
      this.onTouchOutsizeEnable = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.transparent,
        body: Stack(
          children: [
            Positioned.fill(
                child: GestureDetector(
              onTap: onTouchOutsizeEnable ? onNegativeTap : null,
              child: Container(
                color: Colors.transparent,
              ),
            )),
            Align(
              alignment: Alignment.center,
              child: ConstrainedBox(
                constraints: new BoxConstraints(
                  minWidth: MediaQuery.of(context).size.width * .9,
                  maxHeight: MediaQuery.of(context).size.height * .6,
                  maxWidth: MediaQuery.of(context).size.width * .9,
                ),
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10)),
                  padding: EdgeInsets.all(20),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    //  crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        message,
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Row(
                        //mainAxisAlignment: MainAxisAlignment.spaceAround,
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          SizedBox(
                            width: 150,
                          ),
                          GestureDetector(
                            onTap: onNegativeTaps,
                            child: Text(
                              negativeLabels,
                              style: TextStyle(
                                  color: Color(0xff0b4d89),
                                  fontStyle: FontStyle.italic),
                              textAlign: TextAlign.right,
                            ),
                          ),
                          SizedBox(
                            width: 20,
                          ),
                          GestureDetector(
                            onTap: onNegativeTap,
                            child: Text(
                              negativeLabel,
                              style: TextStyle(
                                  color: Color(0xff0b4d89),
                                  fontStyle: FontStyle.italic),
                              textAlign: TextAlign.right,
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ));
  }

  _buildLogo() => WidgetLogo(
        widthPercent: 0.5,
      );
}
