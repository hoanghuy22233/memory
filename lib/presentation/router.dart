import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:memory_lifes/model/repo/user_repository.dart';
import 'package:memory_lifes/presentation/screen/change_password/change_pass.dart';
import 'package:memory_lifes/presentation/screen/diary_detail/bloc/news_detail_bloc.dart';
import 'package:memory_lifes/presentation/screen/diary_detail/sc_news_detail.dart';
import 'package:memory_lifes/presentation/screen/diary_edit/barrel_product_update.dart';
import 'package:memory_lifes/presentation/screen/diary_edit/diary_edit_form/bloc/bloc.dart';
import 'package:memory_lifes/presentation/screen/diary_post/diary_post_form/bloc/diary_post_form_bloc.dart';
import 'package:memory_lifes/presentation/screen/diary_post/sc_post_diary.dart';
import 'package:memory_lifes/presentation/screen/forgot_password/forgot_password.dart';
import 'package:memory_lifes/presentation/screen/forgot_password_reset/sc_forgot_password_reset.dart';
import 'package:memory_lifes/presentation/screen/forgot_password_verify/sc_forgot_password_verify.dart';
import 'package:memory_lifes/presentation/screen/home_page/sc_navigation.dart';
import 'package:memory_lifes/presentation/screen/login/login.dart';
import 'package:memory_lifes/presentation/screen/map_user/position_get/bloc/get_position_bloc.dart';
import 'package:memory_lifes/presentation/screen/map_user/position_library.dart';
import 'package:memory_lifes/presentation/screen/map_user/update_locations/bloc/bloc.dart';
import 'package:memory_lifes/presentation/screen/menu/account/bloc/bloc.dart';
import 'package:memory_lifes/presentation/screen/menu/calendar.dart';
import 'package:memory_lifes/presentation/screen/menu/diary/bloc/post_bloc.dart';
import 'package:memory_lifes/presentation/screen/menu/imageStock/bloc/images_bloc.dart';
import 'package:memory_lifes/presentation/screen/menu/image_library.dart';
import 'package:memory_lifes/presentation/screen/menu/map.dart';
import 'package:memory_lifes/presentation/screen/new_password/new_password.dart';
import 'package:memory_lifes/presentation/screen/note/create_note.dart';
import 'package:memory_lifes/presentation/screen/otp/otp.dart';
import 'package:memory_lifes/presentation/screen/pincode/change_pin.dart';
import 'package:memory_lifes/presentation/screen/profile_user/avatar/bloc/profile_detail_avatar_bloc.dart';
import 'package:memory_lifes/presentation/screen/profile_user/background_image/bloc/profile_detail_background_image_bloc.dart';
import 'package:memory_lifes/presentation/screen/profile_user/birthday_form/bloc/birthday_form_bloc.dart';
import 'package:memory_lifes/presentation/screen/profile_user/bloc/profile_detail_bloc.dart';
import 'package:memory_lifes/presentation/screen/profile_user/email_form/bloc/email_form_bloc.dart';
import 'package:memory_lifes/presentation/screen/profile_user/gender/bloc/bloc.dart';
import 'package:memory_lifes/presentation/screen/profile_user/name_form/bloc/name_form_bloc.dart';
import 'package:memory_lifes/presentation/screen/profile_user/phone_form/bloc/phone_form_bloc.dart';
import 'package:memory_lifes/presentation/screen/profile_user/profile_user.dart';
import 'package:memory_lifes/presentation/screen/search/search_screen.dart';
import 'package:memory_lifes/presentation/screen/select_icon/sc_select_icon.dart';
import 'package:memory_lifes/presentation/screen/sign_up/sign_up.dart';
import 'package:memory_lifes/presentation/screen/splash/intro.dart';
import 'package:memory_lifes/presentation/screen/splash/sc_begin.dart';
import 'package:memory_lifes/presentation/screen/splash/sc_three_screen.dart';
import 'package:memory_lifes/presentation/screen/splash/sc_two_screen.dart';
import 'package:memory_lifes/presentation/screen/splash/splash.dart';

class BaseRouter {
  static const String SPLASH = '/splash';
  static const String LOGIN = '/login';
  static const String SPLASH_ONE = '/splash_one';
  static const String SPLASH_TWO = '/splash_two';
  static const String SPLASH_THREE = '/splash_three';
  static const String OTP = '/otp';
  static const String NAVIGATION = '/navigation';
  static const String CREATE_DIARY = '/create_diary';
  static const String EDIT_DIARY = '/edit_diary';
  static const String CREATE_NOTE = '/create_note';
  static const String CHANGE_PIN = '/change_pin';
  static const String CALENDAR = '/calendar';
  static const String PROFILE_USER = '/profile_user';
  static const String IMAGE_LIBRARY = '/library_image';
  static const String MAP = '/map';
  static const String NEW_PASS = '/new_pass';
  static const String CHANGE_PASS = '/change_pass';
  static const String FORGOT_PASS = '/forgot_pass';
  static const String SIGN_UP = '/sign_up';
  static const String FORGOT_PASSWORD_VERIFY = '/forgot_password_verify';
  static const String FORGOT_PASSWORD_RESET = '/forgot_password_reset';
  static const String NEWS_DETAIL = '/news_detail';
  static const String SELECT_ICON = '/select_icon';
  static const String SELECT_RUN = '/select_run';
  static const String SEARCH = '/search';
  static const String GET_LOCATION = '/get_location';
  static const String INTRO = '/intro';

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case SPLASH:
        return MaterialPageRoute(builder: (_) => SplashScreen());
      case SPLASH_ONE:
        return MaterialPageRoute(builder: (_) => BeginScreen());
      case SPLASH_TWO:
        return MaterialPageRoute(builder: (_) => TwoScreen());
      case SPLASH_THREE:
        return MaterialPageRoute(builder: (_) => ThreeScreen());
      case NAVIGATION:
        return MaterialPageRoute(builder: (_) => NavigationScreen());
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                  body: Center(
                    child: Text('No route defined for ${settings.name}'),
                  ),
                ));
    }
  }

  static Map<String, WidgetBuilder> routes(BuildContext context) {
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    return {
      SPLASH: (context) => SplashScreen(),
      SPLASH_ONE: (context) => BeginScreen(),
      SPLASH_TWO: (context) => TwoScreen(),
      SPLASH_THREE: (context) => ThreeScreen(),
      LOGIN: (context) => LoginScreen(),
      OTP: (context) => OTPcode(),
      //    NAVIGATION: (context) => MyHomePage(),
      // CREATE_DIARY: (context) => CreateNewDiary(),
      // CREATE_DIARY:(context) =>PostDiaryScreen(),
      CREATE_DIARY: (context) => MultiBlocProvider(providers: [
            BlocProvider(
              create: (context) =>
                  DiaryPostFormBloc(userRepository: userRepository),
            ),
          ], child: PostDiaryScreen()),
      SEARCH: (context) => MultiBlocProvider(providers: [
            BlocProvider(
              create: (context) => PostBloc(userRepository: userRepository),
            ),
          ], child: SearchScreen()),
      EDIT_DIARY: (context) => MultiBlocProvider(providers: [
            BlocProvider(
              create: (context) =>
                  EditUpdateFormBloc(userRepository: userRepository),
            ),
          ], child: EditDiaryScreen()),
      CREATE_NOTE: (context) => CreateNewNote(),
      CHANGE_PIN: (context) => ChangePin(),
      CALENDAR: (context) => Calendar(),
      PROFILE_USER: (context) => MultiBlocProvider(providers: [
            BlocProvider(
              create: (context) =>
                  ProfileDetailBloc(userRepository: userRepository),
            ),
            BlocProvider(
              create: (context) => NameFormBloc(userRepository: userRepository),
            ),
            BlocProvider(
              create: (context) =>
                  PhoneFormBloc(userRepository: userRepository),
            ),
            BlocProvider(
              create: (context) =>
                  EmailFormBloc(userRepository: userRepository),
            ),
            BlocProvider(
              create: (context) =>
                  BirthdayFormBloc(userRepository: userRepository),
            ),
            BlocProvider(
              create: (context) =>
                  ProfileDetailAvatarBloc(userRepository: userRepository),
            ),
            BlocProvider(
              create: (context) => ProfileDetailBackgroundImageBloc(
                  userRepository: userRepository),
            ),
            BlocProvider(
              create: (context) => GenderBloc(userRepository: userRepository),
            ),
          ], child: ProfileUser()),
      IMAGE_LIBRARY: (context) => ImageLibrary(),
      MAP: (context) => MapImage(),
      NEW_PASS: (context) => NewPassword(),
      CHANGE_PASS: (context) => ChangePassScreen(),
      FORGOT_PASS: (context) => ForgotPassword(),
      SIGN_UP: (context) => SignUp(),
      FORGOT_PASSWORD_VERIFY: (context) => ForgotPasswordVerifyScreen(),
      FORGOT_PASSWORD_RESET: (context) => ForgotPasswordResetScreen(),
      SELECT_ICON: (context) => SelectIconScreen(),
      INTRO: (context) => Intro(),
      // GET_LOCATION: (context) => MapUser(),
      // SELECT_RUN: (context) => SelectIconScreen(),
      NEWS_DETAIL: (context) => MultiBlocProvider(providers: [
            BlocProvider(
              create: (context) =>
                  NewsDetailBloc(userRepository: userRepository),
            ),

//            ),
          ], child: NewDetailScreen()),

      GET_LOCATION: (context) => MultiBlocProvider(providers: [
            BlocProvider(
              create: (context) =>
                  PositionGetBloc(userRepository: userRepository),
            ),
          ], child: PositionsLibrary()),

      NAVIGATION: (context) => MultiBlocProvider(providers: [
            BlocProvider(
              create: (context) => PostBloc(userRepository: userRepository),
            ),
            BlocProvider(
              create: (context) => ImagesBloc(userRepository: userRepository),
            ),
            BlocProvider(
              create: (context) => ProfileBloc(userRepository: userRepository),
            ),
            BlocProvider(
              create: (context) =>
                  PositionGetBloc(userRepository: userRepository),
            ),
            BlocProvider(
              create: (context) =>
                  UpdateLocationBloc(userRepository: userRepository),
            ),
          ], child: NavigationScreen()),
    };
  }
}
