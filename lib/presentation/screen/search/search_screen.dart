import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:memory_lifes/app/constants/navigator/navigator.dart';
import 'package:memory_lifes/model/entity/new.dart';
import 'package:memory_lifes/presentation/screen/menu/diary/bloc/post_bloc.dart';
import 'package:memory_lifes/presentation/screen/menu/diary/bloc/post_event.dart';
import 'package:memory_lifes/presentation/screen/menu/diary/bloc/post_state.dart';
import 'package:memory_lifes/presentation/screen/search/widget_list_diary.dart';
import 'package:memory_lifes/utils/handler/http_handler.dart';

class SearchScreen extends StatefulWidget {
  const SearchScreen();

  @override
  _SearchScreenState createState() => new _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen>
    with SingleTickerProviderStateMixin {
  static final GlobalKey<ScaffoldState> scaffoldKey =
      new GlobalKey<ScaffoldState>();

  TextEditingController _searchQuery;
  bool _isSearching = false;

  List<News> filteredRecored;
  List<News> allRecord;
  String msgStatus = '';
  bool _isVisible = true;

  @override
  void initState() {
    super.initState();
    _searchQuery = new TextEditingController();
    // BlocProvider.of<PostBloc>(context).add(LoadPost());
    WidgetsBinding.instance.addPostFrameCallback((_) {
      setState(() => _isSearching = true);
      BlocProvider.of<PostBloc>(context).add(LoadPost());
    });
  }

  void _startSearch() {
    ModalRoute.of(context)
        .addLocalHistoryEntry(new LocalHistoryEntry(onRemove: _stopSearching));

    setState(() {
      _isSearching = true;
    });
  }

  void _stopSearching() {
    _clearSearchQuery();

    setState(() {
      _isSearching = false;
      filteredRecored.addAll(allRecord);
    });
  }

  void _clearSearchQuery() {
    setState(() {
      _searchQuery.clear();
      updateSearchQuery("Search query");
    });
  }

  Widget _buildTitle(BuildContext context) {
    var horizontalTitleAlignment =
        Platform.isIOS ? CrossAxisAlignment.center : CrossAxisAlignment.start;

    return new InkWell(
      onTap: () => scaffoldKey.currentState.openDrawer(),
      child: new Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12.0),
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: horizontalTitleAlignment,
          children: <Widget>[
            new Text(
              'Seach box',
              style: new TextStyle(color: Colors.white),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildSearchFiel() {
    return new Stack(
      alignment: AlignmentDirectional.topEnd,
      children: <Widget>[
        Row(
          children: <Widget>[
            SizedBox(
              width: 15.0,
            ),
            Padding(
              padding: EdgeInsets.only(top: 0),
              child: Stack(
                alignment: AlignmentDirectional.topCenter,
                children: <Widget>[
                  new Container(
                    height: 40.0,
                    width: MediaQuery.of(context).size.width / 2,
                    child: StreamBuilder(
                      stream: null,
                      builder: (context, snapshot) => TextField(
                        style: TextStyle(fontSize: 15, color: Colors.black87),
                        controller: _searchQuery,
                        autofocus: false,
                        onTap: () {
                          showToast();
                        },
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(left: 30.0),
                            filled: true,
                            fillColor: Colors.white,
                            hintText: "Tìm kiếm nhật ký",
                            focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.blue, width: 1.0),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(40)),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Colors.grey[300], width: 1.0),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(40)),
                            ),
                            errorText:
                                snapshot.hasError ? snapshot.error : null,
                            labelStyle: TextStyle(fontSize: 15.0)),
                        onChanged: updateSearchQuery,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              width: 5,
            ),
          ],
        ),
      ],
    );
  }

  void updateSearchQuery(String newQuery) {
    filteredRecored.clear();
    if (newQuery.length > 0) {
      Set<News> set = Set.from(allRecord);
      set.forEach((element) => filterList(element, newQuery));
    }

    if (newQuery.isEmpty) {
      filteredRecored.addAll(allRecord);
    }

    setState(() {});
  }

  filterList(News item, String searchQuery) {
    setState(() {
      if (item.title.toLowerCase().contains(searchQuery) ||
          item.title.contains(searchQuery) ||
          item.content.toLowerCase().contains(searchQuery) ||
          item.content.contains(searchQuery) ||
          item.date.toLowerCase().contains(searchQuery) ||
          item.date.contains(searchQuery)) {
        filteredRecored.add(item);
      }
    });
  }

  List<Widget> _buildActions() {
    if (_isSearching) {
      return <Widget>[
        new GestureDetector(
            onTap: () {
              if (_searchQuery == null || _searchQuery.text.isEmpty) {
                AppNavigator.navigateBack();
                return;
              }
              _clearSearchQuery();
            },
            child: Center(
                child: Padding(
              padding: EdgeInsets.only(right: 20),
              child: Text(
                "Hủy",
                style: TextStyle(fontSize: 16.0, color: Colors.blue),
              ),
            ))),
      ];
    }

    return <Widget>[
      new IconButton(
        icon: const Icon(
          Icons.search,
          color: Colors.red,
        ),
        onPressed: _startSearch,
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: _isSearching ? _buildSearchFiel() : _buildTitle(context),
        actions: _buildActions(),
        leading: IconButton(
          icon: Icon(
            Icons.chevron_left,
            color: Colors.blue,
          ),
          onPressed: () {
            AppNavigator.navigateBack();
          },
        ),
      ),
      body: BlocListener<PostBloc, PostState>(
        listener: (context, state) async {
          if (state.isLoading) {
            await HttpHandler.resolve(status: state.status);
          }

          if (state.isSuccess) {
            await HttpHandler.resolve(status: state.status);
            allRecord = state.post;
            filteredRecored = new List<News>();
            filteredRecored.addAll(allRecord);
          }

          if (state.isFailure) {
            await HttpHandler.resolve(status: state.status);
          }
        },
        child: BlocBuilder<PostBloc, PostState>(
          builder: (context, state) {
            // filteredRecored = state.post;
            return Container(
              child: Visibility(
                  visible: _isVisible,
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 5, horizontal: 20),
                    child: Container(
                      child: filteredRecored != null &&
                              filteredRecored.length > 0
                          ? Column(
                              children: [
                                Expanded(
                                  child: WidgetListDiary(news: filteredRecored),
                                )
                              ],
                            )
                          : allRecord == null
                              ? new Center(
                                  child: new CircularProgressIndicator())
                              : new Center(
                                  child: new Text("Không tìm thấy nhật ký!"),
                                ),
                    ),
                  )),
            );
          },
        ),
      ),
    );
  }

  void Finishs() {
    Navigator.of(context).pop();
  }

  void showToast() {
    setState(() {
      _isVisible = _isVisible;
    });
  }
}
