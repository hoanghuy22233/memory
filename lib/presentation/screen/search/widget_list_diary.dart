import 'package:flutter/cupertino.dart';
import 'package:memory_lifes/model/entity/new.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_spacer.dart';
import 'package:memory_lifes/presentation/screen/menu/diary/widget_post_item.dart';

class WidgetListDiary extends StatefulWidget {
  final List<News> news;
  WidgetListDiary({this.news});

  @override
  _WidgetListDiaryState createState() => _WidgetListDiaryState();
}

class _WidgetListDiaryState extends State<WidgetListDiary> {
  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      scrollDirection: Axis.vertical,
      itemBuilder: (context, index) {
        return WidgetPostItem(
          post: widget.news[index],
        );
      },
//                  itemExtent: 100.0,
      itemCount: widget.news.length,
      separatorBuilder: (context, index) {
        return WidgetSpacer(width: 1);
      },
      physics: BouncingScrollPhysics(),
    );
  }
}
