import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:memory_lifes/app/constants/navigator/navigator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:memory_lifes/app/constants/navigator/navigator.dart';
import 'dart:async';

class ThreeScreen extends StatefulWidget {
  @override
  _ThreeScreenState createState() => _ThreeScreenState();
}

class _ThreeScreenState extends State<ThreeScreen> {
  @override
  void initState() {
    super.initState();
    // openThree();
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: Colors.transparent));
  }
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(),
      home: Scaffold(
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: false,
        body: Builder(
          builder: (context) {
            return SingleChildScrollView(
              child: Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    Positioned(
                      top: 60.0,
                      child: Text(
                        "Memory Life",
                        style: TextStyle(
                            color: Colors.blue,
                            fontSize: 25,
                            fontWeight: FontWeight.w700),
                      ),
                    ),
                    Positioned(
                      top: 150,
                      child: Container(
                        height: 230,
                        child: Image.asset(
                          "assets/images/man_cho3.png",
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                    Positioned(
                        top: 430.0,
                        left: 20,
                        right: 20,
                        child: Column(
                          children: <Widget>[
                            Text(
                              "An toàn và bảo mật",
                              style: TextStyle(
                                  color: Colors.blue,
                                  fontSize: 20,
                                  fontWeight: FontWeight.w700),
                            ),
                            SizedBox(
                              height: 30,
                            ),
                            Text(
                              "Nhật ký của bạn thuộc về bạn. Thêm tài khoản để lưu giữ nhật ký ở chế độ riêng tư",
                              style:
                              TextStyle(color: Colors.black, fontSize: 16),
                              textAlign: TextAlign.center,
                            ),
                          ],
                        )),
                    Positioned(
                        bottom: 60,
                        child: Container(
                          height: 40,
                          width: 170,
                          child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              textColor: Colors.white,
                              color: Colors.blue,
                              onPressed: () {
                                AppNavigator.navigateSignUp();
                              },
                              child: Row(
                                children: <Widget>[
                                  Text(
                                    "Tạo tài khoản",
                                    style: TextStyle(fontSize: 18),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 3),
                                    child: Image.asset(
                                      "assets/images/start_ico.png", width: 25, height: 18,),
                                  )
                                ],
                              )),
                        )),
                    Positioned(
                      bottom: 20,
                      child: RichText(
                          text: TextSpan(
                              text: 'Bỏ qua',
                              style: TextStyle(
                                  color: Colors.red,
                                  fontWeight: FontWeight.bold,
                                  decoration: TextDecoration.underline),
                              recognizer: TapGestureRecognizer()
                                ..onTap = () {
                                  AppNavigator.navigateLogin();
                                })),
                    )
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
  void openThree() async {
    //  Future.delayed(Duration(seconds: 3), () {
    CircularProgressIndicator();
    AppNavigator.navigateLogin();
    // });
  }
}
