import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intro_slider/intro_slider.dart';
import 'package:memory_lifes/app/constants/navigator/navigator.dart';


class Intro extends StatefulWidget {
  @override
  _IntroState createState() => _IntroState();
}

class _IntroState extends State<Intro> {
  List<Slide> slides = new List();

  @override
  void initState() {
    super.initState();

    slides.add(
      new Slide(
        title: "Memory Life",
        styleTitle:
          TextStyle(color: Colors.blue, fontSize: 30.0, fontWeight: FontWeight.bold),
        description: "Nhật kí cá nhân. Nhật kí của bạn, người đồng hành của bạn",
        styleDescription:
          TextStyle(color: Colors.black, fontSize: 15.0),
        pathImage: "assets/images/man_cho1.png",
        backgroundColor: Colors.white
      ),
    );
    slides.add(
      new Slide(
        title: "Memory Life",
        styleTitle:
          TextStyle(color: Colors.blue, fontSize: 30.0, fontWeight: FontWeight.bold),
        description: "Người bạn đồng hành. Nhật ký của bạn, lưu giữ những khoảnh khắc trong cuộc sống, hành trình, tâm sự riêng tư",
        styleDescription:
          TextStyle(color: Colors.black, fontSize: 15.0),
          pathImage: "assets/images/man_cho2.png",
        backgroundColor: Colors.white
      ),
    );
    slides.add(
      new Slide(
        title: "Memory Life",
        styleTitle:
          TextStyle(color: Colors.blue, fontSize: 30.0, fontWeight: FontWeight.bold),
        description:
        "An toàn và bảo mật. Nhật ký của bạn thuộc về bạn. Thêm tài khoản để lưu giữ nhật ký ở chế độ riêng tư",
        styleDescription:
          TextStyle(color: Colors.black, fontSize: 15.0),
        pathImage: "assets/images/man_cho3.png",
        backgroundColor: Colors.white
      ),
    );
  }

  void onDonePress() {
    AppNavigator.navigateLogin();
  }

  void onSkipPress() {
    AppNavigator.navigateLogin();
  }

  @override
  Widget build(BuildContext context) {
    return new IntroSlider(
      slides: this.slides,
      onDonePress: this.onDonePress,
      onSkipPress: this.onSkipPress,
    );
  }
}