import 'dart:async';
import 'package:memory_lifes/app/constants/preferences/app_preferences.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:memory_lifes/app/constants/navigator/navigator.dart';
import 'package:memory_lifes/model/repo/user_repository.dart';

import 'package:memory_lifes/utils/handler/with_auth.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with AutomaticKeepAliveClientMixin<SplashScreen> {
  @override
  void initState() {
    super.initState();
    openLogin();
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: Colors.transparent));

  }

  @override
  Widget build(BuildContext context) {
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    //   openLogin(userRepository);
    return Scaffold(
      body: SafeArea(
        top: false,
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Image.asset(
            "assets/images/splash.png",
            fit: BoxFit.fill,
          ),
        ),
      ),
    );
  }

  void openLogin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isSelectedCountry = prefs.containsKey(AppPreferences.LOGIN);
    Future.delayed(Duration(seconds: 1), () {
      WithAuth.isAuth(ifNotAuth: () {
        if (isSelectedCountry) {
          AppNavigator.navigateLogin();
        } else {
          AppNavigator.navigateBegin();
        }
      }, ifAuth: () {
        AppNavigator.navigateNavigation();
      });
    });
  }

  // void openLogin(UserRepository repository) async {
  //   try {
  //     final profileResponse = await repository.getProfile();
  //     if (profileResponse.status == Endpoint.SUCCESS) {
  //       final prefs = LocalPref();
  //       final token = await prefs.getString(AppPreferences.auth_token);
  //       BlocProvider.of<AuthenticationBloc>(Get.context).add(LoggedIn(token));
  //       //  AppNavigator.navigateNavigation();
  //     }
  //     // AppNavigator.navigateNavigation();
  //   } on DioError catch (e) {} finally {
  //     Future.delayed(Duration(milliseconds: 350), () {
  //       CircularProgressIndicator();
  //       AppNavigator.navigateNavigation();
  //     });
  //   }
  // }

  @override
  bool get wantKeepAlive => true;
}
