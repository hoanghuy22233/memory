import 'package:flutter/material.dart';
import 'package:memory_lifes/model/entity/icon.dart';

class WidgetPostIconItem extends StatelessWidget {
  final Function onTap;
  final int id;

  const WidgetPostIconItem({Key key, this.id, @required this.onTap})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 20),
      child: GestureDetector(
        onTap: onTap,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Image.asset(
                  "assets/${iconSet[id].icon}",
                  height: 25,
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  child: Text(
                    "${iconSet[id].name}",
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              ],
            ),
            Container(
              child: Divider(
                height: 1,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
