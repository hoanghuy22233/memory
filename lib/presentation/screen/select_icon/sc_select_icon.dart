import 'package:flutter/material.dart';
import 'package:memory_lifes/model/entity/icon.dart';
import 'package:memory_lifes/presentation/screen/select_icon/widget_icon_post_item.dart';
import 'package:memory_lifes/presentation/screen/select_icon/widget_post_categories_appbar.dart';

class SelectIconScreen extends StatefulWidget {
  List<IconItem> icon;
  @override
  _SelectIconScreenState createState() => _SelectIconScreenState();
}

class _SelectIconScreenState extends State<SelectIconScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: _buildContent(),
      ),
    );
  }

  Widget _buildContent() {
    return Column(
      children: [
        _buildAppbar(),
        Expanded(
          child: Padding(
            padding: EdgeInsets.only(left: 20, right: 20),
            child: ListView.builder(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              controller: ScrollController(),
              itemCount: iconSet?.length ?? 0,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: WidgetPostIconItem(
                      id: index,
                      onTap: () async {
                        // BlocProvider.of<DiaryPostFormBloc>(context)
                        //     .add(IconChanged(iconSet[index]));
                        // AppNavigator.navigateBack();
                      },
                    ),
                  ),
                );
              },
            ),
          ),
        )
      ],
    );
  }

  Widget _buildAppbar() => WidgetPostIconAppbar();
}
