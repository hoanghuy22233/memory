import 'package:flutter/material.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_appbar.dart';

class WidgetPostIconAppbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: WidgetAppbar(
        title: "Bạn đang cảm thấy thế nào?",
        left: [
          Padding(
            padding: EdgeInsets.only(left: 20, top: 20),
            child: GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Image.asset(
                "assets/images/back_all.png",
                height: 25,
                width: 25,
              ),
            ),
          )
        ],
      ),
    );
  }
}
