import 'package:double_back_to_close/double_back_to_close.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:memory_lifes/app/constants/color/color.dart';
import 'package:memory_lifes/app/constants/navigator/navigator.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_fab_bottom_nav.dart';
import 'package:memory_lifes/presentation/screen/drawer/app_drawer.dart';
import 'package:memory_lifes/presentation/screen/menu/account.dart';
import 'package:memory_lifes/presentation/screen/menu/account/bloc/profile_bloc.dart';
import 'package:memory_lifes/presentation/screen/menu/account/bloc/profile_event.dart';
import 'package:memory_lifes/presentation/screen/menu/calendar.dart';
import 'package:memory_lifes/presentation/screen/menu/diary.dart';
import 'package:memory_lifes/presentation/screen/menu/image_library.dart';
import 'package:memory_lifes/presentation/screen/menu/map.dart';
import 'package:memory_lifes/utils/handler/with_auth.dart';
import 'package:provider/provider.dart';


class TabNavigatorRoutes {
//  static const String root = '/';
  static const String diary = '/diary';
  static const String note = '/note';
  static const String image = '/image';
  static const String map = '/map';
  static const String account = '/account';
}

class NavigationScreen extends StatefulWidget {
  NavigationScreen();

  @override
  _NavigationScreenState createState() => _NavigationScreenState();
}

class _NavigationScreenState extends State<NavigationScreen> {
  List<FABBottomNavItem> _navMenus = List();
  PageController _pageController;
  int _selectedIndex = 0;
  bool appbar = true;
  bool allowClose = false;

  GlobalKey<ScaffoldState> _drawerKey = GlobalKey();

  @override
  void initState() {
    _pageController =
        new PageController(initialPage: _selectedIndex, keepPage: true);
    super.initState();
    BlocProvider.of<ProfileBloc>(context).add(LoadProfile());
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _navMenus = _getTab();
  }

  List<FABBottomNavItem> _getTab() {
    return List.from([
      FABBottomNavItem.asset(
          route: TabNavigatorRoutes.diary,
          tabItem: TabItem.diary,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/images/note.png',
          text: "Nhật ký"),
      FABBottomNavItem.asset(
          route: TabNavigatorRoutes.note,
          tabItem: TabItem.note,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/images/calendar.png',
          text: "Lịch"),
      FABBottomNavItem.asset(
          route: TabNavigatorRoutes.image,
          tabItem: TabItem.image,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/images/card_image.png',
          text: "Kho ảnh"),
      FABBottomNavItem.asset(
          route: TabNavigatorRoutes.map,
          tabItem: TabItem.map,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/images/map.png',
          text: "Bản đồ"),
      FABBottomNavItem.asset(
          route: TabNavigatorRoutes.account,
          tabItem: TabItem.account,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/images/user.png',
          text: "Tài khoản"),
    ]);
  }

  void goToPage({int page}) {
    WithAuth.isAuth(ifNotAuth: () {
      if (page == 2 || page == 3) {
        AppNavigator.navigateLogin();
      } else {
        if (page != _selectedIndex) {
          setState(() {
            this._selectedIndex = page;
          });
          _pageController.jumpToPage(_selectedIndex);
        }
      }
    }, ifAuth: () {
      if (page != _selectedIndex) {
        setState(() {
          this._selectedIndex = page;
        });
        _pageController.jumpToPage(_selectedIndex);
      }
    });
    setState(() {
      this._selectedIndex = page;
    });
    _pageController.jumpToPage(_selectedIndex);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _drawerKey,
      appBar: appbar
          ? AppBar(
              backgroundColor: Colors.blue,
              actions: <IconButton>[
                IconButton(
                  icon: Icon(Icons.search),
                  onPressed: () {
                    AppNavigator.navigateSearch();
                  },
                ),
              ],
            )
          : null,
      bottomNavigationBar: WidgetFABBottomNav(
        backgroundColor: AppColor.PRIMARY_COLOR,
        selectedIndex: _selectedIndex,
        onTabSelected: (index) async {
          goToPage(page: index);
        },
        items: _navMenus,
        selectedColor: Colors.blue,
        color: AppColor.NAV_ITEM_COLOR,
      ),
      drawer: AppDrawer(
        drawer: _drawerKey,
        action: goToPage,
        cendalar: goToPage,
        map: goToPage,
        account: goToPage,
      ),
      body: DoubleBack(
        condition: allowClose,
        onConditionFail: () {
          setState(() {
            allowClose = true;
          });
        },
        message: "Nhấn đúp để thoát ứng dụng!",
        child: PageView(
          controller: _pageController,
          physics: NeverScrollableScrollPhysics(),
          onPageChanged: (newPage) {
            setState(() {
              this._selectedIndex = newPage;
            });
          },
          children: [
            Diary(),
            Calendar(
              drawer: _drawerKey,
            ),
            ImageLibrary(
              drawer: _drawerKey,
            ),
            // ChangeNotifierProvider(
            //   create: (context) => PlaceTrackerHomePage(),
            //   child: MapImage(
            //     drawer: _drawerKey,
            //   ),
            // ),
            MapImage(),
            Account(
              drawer: _drawerKey,
            )
          ],
        ),
        onFirstBackPress: (context) {
          Flushbar(
            message: "Nhấn đúp để thoát ứng dụng!",
            duration: Duration(seconds: 3),
          )..show(context);
        },
        waitForSecondBackPress: 3,
      ),
    );
  }
}
