import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:memory_lifes/model/repo/barrel_repo.dart';
import 'package:memory_lifes/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:memory_lifes/presentation/screen/forgot_password_verify/barrel_forgot_password_verify.dart';
import 'package:memory_lifes/presentation/screen/forgot_password_verify/bloc/bloc.dart';

class ForgotPasswordVerifyScreen extends StatefulWidget {
  @override
  _ForgotPasswordVerifyScreenState createState() =>
      _ForgotPasswordVerifyScreenState();
}

class _ForgotPasswordVerifyScreenState extends State<ForgotPasswordVerifyScreen> {

  String _username;

  _onArgument() {
    Future.delayed(Duration.zero, () async {
      final Map arguments = ModalRoute.of(context).settings.arguments as Map;
      setState(() {
        _username = arguments['username'];
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _onArgument();
  }

  @override
  Widget build(BuildContext context) {
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    return WidgetTouchHideKeyBoard(
      child: Scaffold(
        body: SafeArea(
          top: false,
          child: MultiBlocProvider(
              providers: [
                BlocProvider(
                  create: (context) =>
                      ForgotPasswordVerifyBloc(userRepository: userRepository),
                ),
              ],
              child: Stack(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage('assets/images/background.png'),
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      // WidgetSpacer(
                      //   height: MediaQuery.of(context).size.height / 3,
                      // ),
                      Center(
                        child: _buildUsername(),
                      ),
                      WidgetSpacer(
                        height: 10,
                      ),
                      _buildForm(),
                      WidgetSpacer(
                        height: 20,
                      ),
                    ],
                  )
                ],
              )),
        ),
      ),
    );
  }

  _buildUsername() => WidgetForgotPasswordVerifyUsername(
        username: _username,
      );

  _buildForm() => WidgetForgotPasswordVerifyForm(username: _username);
}
