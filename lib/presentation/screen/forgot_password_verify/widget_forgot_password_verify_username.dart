import 'package:flutter/material.dart';
import 'package:memory_lifes/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:memory_lifes/utils/utils.dart';

class WidgetForgotPasswordVerifyUsername extends StatelessWidget {
  final String username;

  const WidgetForgotPasswordVerifyUsername({Key key, @required this.username})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
              AppLocalizations.of(context).translate('register_verify.message'),
              style: TextStyle(
                  color: Colors.blue,
                  fontSize: 14,
                  fontWeight: FontWeight.w700)),
          WidgetSpacer(
            height: 10,
          ),
          Text('${AppCommonUtils.hideUserName(username)}',
              style: TextStyle(
                  color: Colors.blue,
                  fontSize: 14,
                  fontWeight: FontWeight.w700)),
        ],
      ),
    );
  }
}
