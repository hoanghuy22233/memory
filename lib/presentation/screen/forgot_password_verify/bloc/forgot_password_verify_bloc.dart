import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:memory_lifes/app/constants/barrel_constants.dart';
import 'package:memory_lifes/model/repo/barrel_repo.dart';
import 'package:memory_lifes/presentation/screen/forgot_password_verify/bloc/bloc.dart';
import 'package:memory_lifes/utils/utils.dart';
import 'package:meta/meta.dart';

class ForgotPasswordVerifyBloc
    extends Bloc<ForgotPasswordVerifyEvent, ForgotPasswordVerifyState> {
  final UserRepository _userRepository;

  ForgotPasswordVerifyBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  ForgotPasswordVerifyState get initialState =>
      ForgotPasswordVerifyState.empty();

  @override
  Stream<ForgotPasswordVerifyState> mapEventToState(
      ForgotPasswordVerifyEvent event) async* {
    if (event is OtpCodeChanged) {
      yield* _mapOtpCodeChangeToState(event.otpCode);
    } else if (event is ForgotPasswordVerifySubmitted) {
      yield* _mapFormSubmittedToState(event.username, event.otpCode);
    }
  }

  Stream<ForgotPasswordVerifyState> _mapOtpCodeChangeToState(
      String otpCode) async* {
    yield state.update(isOtpCodeValid: Validator.isValidOtp(otpCode));
  }

  Stream<ForgotPasswordVerifyState> _mapFormSubmittedToState(
      String username, String otpCode) async* {
    yield ForgotPasswordVerifyState.loading();

    //need refactor
    var isOtpCodeValid = Validator.isValidOtp(otpCode);

    var newState = state.update(
      isOtpCodeValid: isOtpCodeValid,
    );

    yield newState;

    if (newState.isFormValid) {
      try {
        var response = await _userRepository.forgotPasswordVerify(
            username: username, otpCode: otpCode);
        if (response.status == Endpoint.SUCCESS) {
          yield ForgotPasswordVerifyState.success(message: response.message);
        } else {
          yield ForgotPasswordVerifyState.failure(message: response.message);
        }
      } catch (e) {
        print(e.toString());
        yield ForgotPasswordVerifyState.failure(message: e.toString());
      }
    }
  }
}
