import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:memory_lifes/app/constants/barrel_constants.dart';
import 'package:memory_lifes/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_login_button.dart';
import 'package:memory_lifes/presentation/screen/forgot_password_verify/bloc/bloc.dart';
import 'package:memory_lifes/utils/utils.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class WidgetForgotPasswordVerifyForm extends StatefulWidget {
  final String username;

  const WidgetForgotPasswordVerifyForm({Key key, @required this.username})
      : super(key: key);

  @override
  _WidgetForgotPasswordVerifyFormState createState() =>
      _WidgetForgotPasswordVerifyFormState();
}

class _WidgetForgotPasswordVerifyFormState
    extends State<WidgetForgotPasswordVerifyForm> {
  ForgotPasswordVerifyBloc _registerVerifyBloc;

  TextEditingController _otpCodeController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _registerVerifyBloc = BlocProvider.of<ForgotPasswordVerifyBloc>(context);
    _otpCodeController.addListener(_onOtpCodeChange);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ForgotPasswordVerifyBloc, ForgotPasswordVerifyState>(
      listener: (context, state) async {
        if (state.isSubmitting) {
          GetSnackBarUtils.createProgress();
        }
        if (state.isSuccess) {
          await GetSnackBarUtils.createSuccess(message: state.message);
          AppNavigator.navigateForgotPasswordReset(
              username: widget.username, otpCode: _otpCodeController.text);
        }
        if (state.isFailure) {
          GetSnackBarUtils.createError(message: state.message);
        }
      },
      child: BlocBuilder<ForgotPasswordVerifyBloc, ForgotPasswordVerifyState>(
        builder: (context, state) {
          return Padding(
            padding: EdgeInsets.all(20),
            child: Form(
              child: Column(
                children: [
                  _buildCodeField(),
                  WidgetSpacer(
                    height: 10,
                  ),
                  _buildButtonVerify(state),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  bool get isPopulated => _otpCodeController.text.isNotEmpty;

  bool isForgotPasswordButtonEnabled() {
    return _registerVerifyBloc.state.isFormValid &&
        isPopulated &&
        !_registerVerifyBloc.state.isSubmitting;
  }

  _buildButtonVerify(ForgotPasswordVerifyState state) {
    return WidgetLoginButton(
      onTap: () {
        if (isForgotPasswordButtonEnabled()) {
          _registerVerifyBloc.add(ForgotPasswordVerifySubmitted(
            username: widget.username,
            otpCode: _otpCodeController.text,
          ));
        }
      },
      isEnable: isForgotPasswordButtonEnabled(),
      text: "Xác nhận",
    );
  }

  Widget _buildCodeField() {
    return PinCodeTextField(
      onChanged: (changed) {},
      length: 6,
      obsecureText: false,
      animationType: AnimationType.fade,
      pinTheme: PinTheme(
        shape: PinCodeFieldShape.box,
        borderWidth: 1,
        inactiveColor: Colors.blue,
        inactiveFillColor: AppColor.GREY_LIGHTER_3,
        selectedColor: AppColor.GREY,
        selectedFillColor: AppColor.GREY,
        activeColor: AppColor.GREY_LIGHTER_3,
        activeFillColor: AppColor.GREY_LIGHTER_3,
      ),
      animationDuration: Duration(milliseconds: 300),
      backgroundColor: Colors.transparent,
      enableActiveFill: true,
      textInputType: TextInputType.phone,
      controller: _otpCodeController,
      onCompleted: (completed) {
        print("Completed: $completed");
      },
      beforeTextPaste: (text) {
        return true;
      },
    );
  }

  void _onOtpCodeChange() {
    _registerVerifyBloc.add(OtpCodeChanged(otpCode: _otpCodeController.text));
  }

  @override
  void dispose() {
    super.dispose();
  }
}
