import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:memory_lifes/app/auth_bloc/authentication_bloc.dart';
import 'package:memory_lifes/app/constants/navigator/navigator.dart';
import 'package:memory_lifes/model/repo/user_repository.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_spacer.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_touch_hide_keyboard.dart';
import 'package:memory_lifes/presentation/screen/login/widget_login_form.dart';

import 'bloc/login_bloc.dart';

class LoginScreen extends StatefulWidget {
  @override
  LoginScreenState createState() => LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> {
  Future<bool> _onWillPop() {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text('Bạn chắc chứ?'),
        content: Text('Bạn muốn thoát ứng dụng?'),
        actions: <Widget>[
          FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: Text('Không'),
          ),
          FlatButton(
            onPressed: () => exit(0),
            /*Navigator.of(context).pop(true)*/
            child: Text('Có'),
          ),
        ],
      ),
    ) ??
        false;
  }
  @override
  Widget build(BuildContext context) {
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    return WidgetTouchHideKeyBoard(
      child: Scaffold(
        body:WillPopScope(
          onWillPop: _onWillPop,
          child:  BlocProvider(
            create: (context) => LoginBloc(
                userRepository: userRepository,
                authenticationBloc:
                BlocProvider.of<AuthenticationBloc>(context)),
            child: Stack(
              alignment: Alignment.center,
              children: [
                Image.asset("assets/images/background.png",
                    fit: BoxFit.fill, width: double.infinity),
                SingleChildScrollView(
                  physics: BouncingScrollPhysics(),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      WidgetSpacer(
                        height: 100,
                      ),
                      _buildLoginForm(),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Center(
                          child: RichText(
                            text: TextSpan(
                                text: 'Bạn chưa có tài khoản?  ',
                                style: TextStyle(color: Colors.black),
                                children: <TextSpan>[
                                  TextSpan(
                                      text: 'Đăng ký',
                                      style: TextStyle(
                                          color: Colors.blueAccent,
                                          fontWeight: FontWeight.bold,
                                          decoration:
                                          TextDecoration.underline),
                                      recognizer: TapGestureRecognizer()
                                        ..onTap = () {
                                          AppNavigator.navigateSignUp();
                                        })
                                ]),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
        );


  }

  _buildLoginForm() => WidgetLoginForm();
}
