
import 'package:flutter/material.dart';
import 'package:memory_lifes/model/entity/user.dart';

import 'barrel_profile_detail.dart';

class WidgetProfileDetailImage extends StatelessWidget {
  final String backgroundUrl;
  final String avatarUrl;
  final User user;

  const WidgetProfileDetailImage(
      {Key key,
      @required this.backgroundUrl,
      @required this.avatarUrl,
      this.user})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: [
          user.backgroundImage != null
              ? WidgetProfileDetailBackgroundImage(
                  backgroundImageUrl: backgroundUrl,
                )
              : WidgetProfileDetailBackgroundImage(
                  backgroundImageUrl: "assets/images/background_account.jpg",
                ),
          user.avatar != null
              ? WidgetProfileDetailAvatar(
                  avatarUrl: avatarUrl,
                )
              : WidgetProfileDetailAvatar(
                  avatarUrl: "assets/images/default.png",
                ),
        ],
      ),
    );
  }
}
