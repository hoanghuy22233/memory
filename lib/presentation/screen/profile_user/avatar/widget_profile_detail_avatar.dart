import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:memory_lifes/app/constants/barrel_constants.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_cached_image.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_circle_avatar.dart';
import 'package:memory_lifes/presentation/screen/menu/account/bloc/profile_bloc.dart';
import 'package:memory_lifes/presentation/screen/menu/account/bloc/profile_event.dart';
import 'package:memory_lifes/presentation/screen/profile_user/avatar/bloc/profile_detail_avatar_state.dart';
import 'package:memory_lifes/utils/handler/http_handler.dart';
import 'package:memory_lifes/utils/snackbar/get_snack_bar_utils.dart';

import 'bloc/profile_detail_avatar_bloc.dart';
import 'bloc/profile_detail_avatar_event.dart';

class WidgetProfileDetailAvatar extends StatefulWidget {
  final String avatarUrl;

  const WidgetProfileDetailAvatar({Key key, @required this.avatarUrl})
      : super(key: key);

  @override
  _WidgetProfileDetailAvatarState createState() =>
      _WidgetProfileDetailAvatarState();
}

class _WidgetProfileDetailAvatarState extends State<WidgetProfileDetailAvatar> {
  final _picker = ImagePicker();
  PickedFile avatarFile;
  File croppedFile;

  @override
  Widget build(BuildContext context) {
    return BlocListener<ProfileDetailAvatarBloc, ProfileDetailAvatarState>(
      listener: (context, state) async {
        if (state.isSubmitting) {
          GetSnackBarUtils.createProgress();
        }

        if (state.isSuccess) {
          BlocProvider.of<ProfileBloc>(context).add(RefreshProfile());
          await HttpHandler.resolve(status: state.status);
          AppNavigator.navigateNavigation();
        }

        if (state.isFailure) {
          await HttpHandler.resolve(status: state.status);
        }
      },
      child: BlocBuilder<ProfileDetailAvatarBloc, ProfileDetailAvatarState>(
          builder: (context, state) {
        return Container(
          child: Align(
            alignment: Alignment.center,
            child: FractionallySizedBox(
              widthFactor: 0.3,
              child: AspectRatio(
                aspectRatio: 1,
                child: Stack(
                  children: [
                    WidgetCircleAvatar(
                      image: WidgetCachedImage(
                        url: widget.avatarUrl ?? '',
                        fit: BoxFit.cover,
                      ),
                    ),
                    Positioned.fill(
                      child: Align(
                        alignment: Alignment.bottomRight,
                        child: GestureDetector(
                          onTap: _onPickAvatar,
                          child: FractionallySizedBox(
                            widthFactor: 0.3,
                            child: AspectRatio(
                              aspectRatio: 1,
                              child: WidgetCircleAvatar(
                                padding: EdgeInsets.all(10.0),
                                image: Image.asset(
                                  'assets/images/img_camera.png',
                                  fit: BoxFit.fitWidth,
                                ),
                                backgroundColor: Color(0xFFD6D5D5),
                              ),
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      }),
    );
  }

  Future<Null> _onPickAvatar() async {
    avatarFile = null;
    avatarFile = await _picker.getImage(source: ImageSource.gallery,imageQuality: 25);
    croppedFile = File(avatarFile.path);
    if (avatarFile != null) {
      BlocProvider.of<ProfileDetailAvatarBloc>(context)
          .add(ProfileDetailAvatarUploadEvent(avatarFile: croppedFile));
    }
  }
}
