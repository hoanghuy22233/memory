import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:memory_lifes/app/constants/endpoint/app_endpoint.dart';
import 'package:memory_lifes/model/repo/user_repository.dart';
import 'package:memory_lifes/presentation/screen/profile_user/avatar/bloc/profile_detail_avatar_event.dart';
import 'package:memory_lifes/presentation/screen/profile_user/avatar/bloc/profile_detail_avatar_state.dart';
import 'package:memory_lifes/utils/dio/dio_error_util.dart';
import 'package:memory_lifes/utils/dio/dio_status.dart';

class ProfileDetailAvatarBloc
    extends Bloc<ProfileDetailAvatarEvent, ProfileDetailAvatarState> {
  final UserRepository _userRepository;

  ProfileDetailAvatarBloc({
    @required UserRepository userRepository,
  })  : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  get initialState => ProfileDetailAvatarState.empty();


  @override
  Stream<ProfileDetailAvatarState> mapEventToState(
      ProfileDetailAvatarEvent event) async* {
    if (event is ProfileDetailAvatarUploadEvent) {
      yield* _mapProfileDetailAvatarUploadEventToState(event.avatarFile);
    }
  }

  Stream<ProfileDetailAvatarState> _mapProfileDetailAvatarUploadEventToState(
      File avatarFile) async* {
    try {
      yield ProfileDetailAvatarState.loading();

      var response = await _userRepository.updateAvatar(avatarFile: avatarFile);
      print('---token----');
      print(response);

      if (response.status == Endpoint.SUCCESS) {
        yield ProfileDetailAvatarState.success(
            status: DioStatus(
                message: response.message, code: DioStatus.API_SUCCESS_NOTIFY));
      } else {
        yield ProfileDetailAvatarState.failure(
            status: DioStatus(
                message: response.message, code: DioStatus.API_FAILURE_NOTIFY));
      }
    } catch (e) {
      yield ProfileDetailAvatarState.failure(
          status: DioErrorUtil.handleError(e));
    }
  }

}
