export 'avatar/barrel_profile_detail_avatar.dart';
export 'background_image/barrel_profile_detail_background_image.dart';
export 'birthday_form/barrel_birthday_form.dart';
export 'email_form/barrel_email_form.dart';
export 'name_form/barrel_name_form.dart';
export 'phone_form/barrel_phone_form.dart';
export 'profile_user.dart';
export 'widget_profile_detail_image.dart';
