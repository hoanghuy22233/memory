import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:memory_lifes/app/constants/endpoint/app_endpoint.dart';
import 'package:memory_lifes/model/repo/user_repository.dart';
import 'package:memory_lifes/presentation/screen/profile_user/background_image/bloc/profile_detail_background_image_event.dart';
import 'package:memory_lifes/presentation/screen/profile_user/background_image/bloc/profile_detail_background_image_state.dart';
import 'package:memory_lifes/utils/dio/dio_error_util.dart';
import 'package:memory_lifes/utils/dio/dio_status.dart';

class ProfileDetailBackgroundImageBloc extends Bloc<
    ProfileDetailBackgroundImageEvent, ProfileDetailBackgroundImageState> {
  final UserRepository _userRepository;

  ProfileDetailBackgroundImageBloc({
    @required UserRepository userRepository,
  })  : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  get initialState => ProfileDetailBackgroundImageState.empty();

  @override
  Stream<ProfileDetailBackgroundImageState> mapEventToState(
      ProfileDetailBackgroundImageEvent event) async* {
    if (event is ProfileDetailBackgroundImageUploadEvent) {
      yield* _mapProfileDetailBackgroundImageUploadEventToState(
          event.backgroundImageFile);
    }
  }

  Stream<ProfileDetailBackgroundImageState>
      _mapProfileDetailBackgroundImageUploadEventToState(
          File backgroundImageFile) async* {
    try {
      yield ProfileDetailBackgroundImageState.loading();

      var response = await _userRepository.updateBackgroundImage(
          backgroundImageFile: backgroundImageFile);

      if (response.status == Endpoint.SUCCESS) {
        yield ProfileDetailBackgroundImageState.success(
            status: DioStatus(
                message: response.message, code: DioStatus.API_SUCCESS_NOTIFY));
      } else {
        yield ProfileDetailBackgroundImageState.failure(
            status: DioStatus(
                message: response.message, code: DioStatus.API_FAILURE_NOTIFY));
      }
    } catch (e) {
      yield ProfileDetailBackgroundImageState.failure(
          status: DioErrorUtil.handleError(e));
    }
  }
}
