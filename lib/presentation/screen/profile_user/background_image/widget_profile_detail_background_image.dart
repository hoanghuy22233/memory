import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:memory_lifes/app/constants/navigator/navigator.dart';
import 'package:memory_lifes/app/constants/value/value.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_circle_avatar.dart';
import 'package:memory_lifes/presentation/screen/menu/account/bloc/profile_bloc.dart';
import 'package:memory_lifes/presentation/screen/menu/account/bloc/profile_event.dart';
import 'package:memory_lifes/presentation/screen/profile_user/background_image/bloc/profile_detail_background_image_bloc.dart';
import 'package:memory_lifes/presentation/screen/profile_user/background_image/bloc/profile_detail_background_image_event.dart';
import 'package:memory_lifes/utils/handler/http_handler.dart';
import 'package:memory_lifes/utils/snackbar/get_snack_bar_utils.dart';

import 'bloc/profile_detail_background_image_state.dart';

class WidgetProfileDetailBackgroundImage extends StatefulWidget {
  final String backgroundImageUrl;

  const WidgetProfileDetailBackgroundImage(
      {Key key, @required this.backgroundImageUrl})
      : super(key: key);

  @override
  _WidgetProfileDetailBackgroundImageState createState() =>
      _WidgetProfileDetailBackgroundImageState();
}

class _WidgetProfileDetailBackgroundImageState
    extends State<WidgetProfileDetailBackgroundImage> {
  final _picker = ImagePicker();
  PickedFile backgroundImageFile;
  File croppedFile;

  @override
  Widget build(BuildContext context) {
    return BlocListener<ProfileDetailBackgroundImageBloc,
        ProfileDetailBackgroundImageState>(
      listener: (context, state) async {
        if (state.isSubmitting) {
          GetSnackBarUtils.createProgress();
        }

        if (state.isSuccess) {
          BlocProvider.of<ProfileBloc>(context).add(RefreshProfile());
          await HttpHandler.resolve(status: state.status);
          AppNavigator.navigateNavigation();
        }

        if (state.isFailure) {
          await HttpHandler.resolve(status: state.status);
        }
      },
      child: BlocBuilder<ProfileDetailBackgroundImageBloc,
          ProfileDetailBackgroundImageState>(builder: (context, state) {
        return Stack(
          children: [
            Container(
              child: AspectRatio(
                aspectRatio: AppValue.BANNER_RATIO,
                child: FadeInImage.assetNetwork(
                  image: widget.backgroundImageUrl ?? '',
                  placeholder: 'assets/images/cover_image.png',
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Positioned(
              left: 20, top: 20,
              child: GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Image.asset(
                  "assets/images/back_all.png",
                  height: 25,
                  width: 25,
                ),
              ),
            ),
            Positioned(
              bottom: 10,
              right: 10,
              child: GestureDetector(
                onTap: _onPickBackgroundImage,
                child: Container(
                  width: 40,
                  height: 40,
                  child: WidgetCircleAvatar(
                    padding: EdgeInsets.all(10.0),
                    image: Image.asset(
                      'assets/images/img_camera.png',
                      fit: BoxFit.fitWidth,
                    ),
                    backgroundColor: Color(0xFFD6D5D5),
                  ),
                ),
              ),
            ),
            Positioned(
              top: 20,
              left: 20,
              child: GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Image.asset(
                  "assets/images/back_all.png",
                  height: 25,
                  width: 25,
                ),
              ),
            ),
          ],
        );
      }),
    );
  }

  Future<Null> _onPickBackgroundImage() async {
    backgroundImageFile = null;
    backgroundImageFile = await _picker.getImage(source: ImageSource.gallery,imageQuality: 25);
    croppedFile = File(backgroundImageFile.path);
    if (backgroundImageFile != null) {
      BlocProvider.of<ProfileDetailBackgroundImageBloc>(context).add(
          ProfileDetailBackgroundImageUploadEvent(
              backgroundImageFile: croppedFile));
    }
  }
}
