import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:memory_lifes/app/constants/color/color.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_circle_progress.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_profile_detail_row.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_spacer.dart';
import 'package:memory_lifes/presentation/screen/menu/account/bloc/profile_bloc.dart';
import 'package:memory_lifes/presentation/screen/menu/account/bloc/profile_event.dart';
import 'package:memory_lifes/presentation/screen/menu/account/bloc/profile_state.dart';
import 'package:memory_lifes/presentation/screen/profile_user/avatar/widget_profile_detail_avatar.dart';
import 'package:memory_lifes/presentation/screen/profile_user/birthday_form/widget_birthday_form.dart';
import 'package:memory_lifes/presentation/screen/profile_user/bloc/profile_detail_bloc.dart';
import 'package:memory_lifes/presentation/screen/profile_user/bloc/profile_detail_event.dart';
import 'package:memory_lifes/presentation/screen/profile_user/bloc/profile_detail_state.dart';
import 'package:memory_lifes/presentation/screen/profile_user/email_form/widget_email_form.dart';
import 'package:memory_lifes/presentation/screen/profile_user/gender/widget_gender.dart';
import 'package:memory_lifes/presentation/screen/profile_user/phone_form/widget_phone_form.dart';
import 'package:memory_lifes/presentation/screen/view_image.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

import 'background_image/widget_profile_detail_background_image.dart';
import 'name_form/widget_name_form.dart';

class ProfileUser extends StatefulWidget {
  const ProfileUser({Key key}) : super(key: key);

  @override
  ProfileUserState createState() => ProfileUserState();
}

class ProfileUserState extends State<ProfileUser>
    with AutomaticKeepAliveClientMixin<ProfileUser> {
  PanelController _panelController = new PanelController();

  @override
  void initState() {
    super.initState();
    BlocProvider.of<ProfileBloc>(context).add(LoadProfile());
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      body: SafeArea(
        top: false,
        child: Container(
          child: _buildContent(),
        ),
      ),
    );
  }

  Widget _buildContent() {
    return BlocBuilder<ProfileBloc, ProfileState>(builder: (context, state) {
      if (state is ProfileLoaded) {
        return SlidingUpPanel(
          controller: _panelController,
          body: _buildMenu(state),
          panel: _buildPanel(state),
          minHeight: 0,
          maxHeight: 250,
          backdropEnabled: true,
          renderPanelSheet: false,
        );
      } else if (state is ProfileLoading) {
        return Center(
          child: WidgetCircleProgress(),
        );
      } else if (state is ProfileNotLoaded) {
        return Center(
          child: Text('${state.error}'),
        );
      } else {
        return Center(
          child: Text('Unknown state'),
        );
      }
    });
  }

  Widget _buildPanel(ProfileLoaded profileState) {
    return BlocBuilder<ProfileDetailBloc, ProfileDetailState>(
        builder: (context, state) {
      if (state is ProfileDetailFullnameFormOpened) {
        return WidgetNameForm(
          name: profileState.user.name,
          onCloseTap: () {
            _panelController.close();
          },
        );
      }
      if (state is ProfileDetailBirthdayFormOpened) {
        return WidgetBirthdayForm(
          birthDay: profileState.user.dateOfBirth.toString(),
          onCloseTap: () {
            _panelController.close();
          },
        );
      }
      if (state is ProfileDetailPhoneFormOpened) {
        return WidgetPhoneForm(
          phone: profileState.user.phoneNumber.toString(),
          onCloseTap: () {
            _panelController.close();
          },
        );
      }
      if (state is ProfileDetailEmailFormOpened) {
        return WidgetEmailForm(
          email: profileState.user.email,
          onCloseTap: () {
            _panelController.close();
          },
        );
      }

      return WidgetSpacer(
        height: 10,
      );
    });
  }

  Widget _buildMenu(ProfileLoaded state) {
    return RefreshIndicator(
      onRefresh: () async {
        BlocProvider.of<ProfileBloc>(context).add(LoadProfile());
        await Future.delayed(Duration(seconds: 3));
        return true;
      },
      color: AppColor.PRIMARY_COLOR,
      backgroundColor: AppColor.THIRD_COLOR,
      child: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          children: [
            Stack(
              alignment: Alignment.bottomCenter,
              overflow: Overflow.visible,
              children: [
                GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => ViewImage(
                              image: state.user.backgroundImage,
                            )));
                  },
                  child: WidgetProfileDetailBackgroundImage(
                    backgroundImageUrl: state.user.backgroundImage,
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => ViewImage(
                              image: state.user.avatar,
                            )));
                  },
                  child: Container(
                    child: WidgetProfileDetailAvatar(
                      avatarUrl: state.user.avatar,
                    ),
                  ),
                ),
              ],
            ),
            WidgetProfileDetailRow(
              image: Icons.person,
              title: "Tên tài khoản",
              content: state.user.name ?? "chưa thiết lập",
              onTap: () {
                _panelController.open();
                BlocProvider.of<ProfileDetailBloc>(context)
                    .add(OpenFullnameForm());
              },
            ),
            WidgetProfileDetailRow(
              image: Icons.calendar_today,
              title: "Ngày sinh",
              content: state.user.dateOfBirth ?? "chưa thiết lập",
              onTap: () {
                _panelController.open();
                BlocProvider.of<ProfileDetailBloc>(context)
                    .add(OpenBirthdayForm());
              },
            ),
            WidgetProfileDetailRow(
              image: Icons.phone,
              title: "Số điện thoại",
              content: state?.user?.phoneNumber ?? "chưa thiết lập",
              onTap: () {
                _panelController.open();
                BlocProvider.of<ProfileDetailBloc>(context)
                    .add(OpenPhoneForm());
              },
            ),
            Row(
              children: [
                SizedBox(
                  width: 10,
                ),
                Icon(
                  Icons.wc,
                  color: Colors.blue,
                ),
                SizedBox(
                  width: 10,
                ),
                WidgetGender(
                  title: state?.user.gender,
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
