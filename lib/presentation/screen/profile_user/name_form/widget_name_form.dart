
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:memory_lifes/app/constants/color/color.dart';
import 'package:memory_lifes/app/constants/style/style.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_login_button.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_spacer.dart';
import 'package:memory_lifes/presentation/screen/menu/account/bloc/profile_bloc.dart';
import 'package:memory_lifes/presentation/screen/menu/account/bloc/profile_event.dart';
import 'package:memory_lifes/presentation/screen/profile_user/name_form/bloc/name_form_bloc.dart';
import 'package:memory_lifes/presentation/screen/profile_user/name_form/bloc/name_form_event.dart';
import 'package:memory_lifes/utils/handler/http_handler.dart';
import 'package:memory_lifes/utils/locale/app_localization.dart';
import 'package:memory_lifes/utils/snackbar/get_snack_bar_utils.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

import 'bloc/name_form_state.dart';

class WidgetNameForm extends StatefulWidget {
  final String name;
  final Function onCloseTap;

  const WidgetNameForm({Key key, @required this.onCloseTap, @required this.name}) : super(key: key);

  @override
  _WidgetNameFormState createState() => _WidgetNameFormState();
}

class _WidgetNameFormState extends State<WidgetNameForm> {
  NameFormBloc _nameFormBloc;
  final TextEditingController _nameController = TextEditingController();

  final underline = new UnderlineInputBorder(
    borderSide: BorderSide(
      color: AppColor.GREY,
    ),
  );

  @override
  void initState() {
    super.initState();
    _nameFormBloc = BlocProvider.of<NameFormBloc>(context);
    _nameController.text = widget.name ?? '';
    _nameController.addListener(_onNameChange);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<NameFormBloc, NameFormState>(
      listener: (context, state) async {
        if (state.isSubmitting) {
          GetSnackBarUtils.createProgress();
        }

        if (state.isSuccess) {
          BlocProvider.of<ProfileBloc>(context).add(RefreshProfile());
          await HttpHandler.resolve(status: state.status);
        }

        if (state.isFailure) {
          await HttpHandler.resolve(status: state.status);
        }
      },
      child:
          BlocBuilder<NameFormBloc, NameFormState>(builder: (context, state) {
        return Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10), topRight: Radius.circular(10))),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Row(
                children: [
                  Expanded(
                    child: Text(
                      "Họ tên",
                      style: AppStyle.DEFAULT_LARGE_BOLD,
                    ),
                  ),
                  GestureDetector(
                    onTap: widget.onCloseTap,
                    child: Container(
                      width: 25,
                      height: 25,
                      child: Image.asset('assets/images/img_close_round.png'),
                    ),
                  )
                ],
              ),
              WidgetSpacer(
                height: 5,
              ),
              _buildTextFieldName(),
              WidgetSpacer(
                height: 10,
              ),
              _buildButtonSubmit()
            ],
          ),
        );
      }),
    );
  }

  bool get isPopulated => _nameController.text.isNotEmpty;

  bool isSubmitButtonEnabled() {
    return _nameFormBloc.state.isFormValid &&
        isPopulated &&
        !_nameFormBloc.state.isSubmitting;
  }

  _buildButtonSubmit() {
    return WidgetLoginButton(
      onTap: () {
        if (isSubmitButtonEnabled()) {
          _nameFormBloc.add(NameFormSubmitEvent(
            fullname: _nameController.text.trim(),
          ));
        }
      },
      isEnable: isSubmitButtonEnabled(),
      text: "Cập nhật",
    );
  }

  _buildTextFieldName() {
    return TextFormField(
        controller: _nameController,
        validator: (_) {
          print('validator');
          return !_nameFormBloc.state.isNameValid
              ? ''
              : null;
        },
        decoration: InputDecoration(
            disabledBorder: underline,
            enabledBorder: underline,
            focusedBorder: underline,
            hintText: "Nhập tên của bạn",
            hintStyle: AppStyle.DEFAULT_LARGE.copyWith(color: AppColor.GREY)));
  }

  void _onNameChange() {
    _nameFormBloc.add(FullnameChanged(
      fullname: _nameController.text,
    ));
  }
}
