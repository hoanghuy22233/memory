
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:memory_lifes/app/constants/color/color.dart';
import 'package:memory_lifes/app/constants/style/style.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_login_button.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_spacer.dart';
import 'package:memory_lifes/presentation/screen/menu/account/bloc/profile_bloc.dart';
import 'package:memory_lifes/presentation/screen/menu/account/bloc/profile_event.dart';
import 'package:memory_lifes/presentation/screen/profile_user/phone_form/bloc/phone_form_bloc.dart';
import 'package:memory_lifes/presentation/screen/profile_user/phone_form/bloc/phone_form_event.dart';
import 'package:memory_lifes/utils/handler/http_handler.dart';
import 'package:memory_lifes/utils/locale/app_localization.dart';
import 'package:memory_lifes/utils/snackbar/get_snack_bar_utils.dart';

import 'bloc/phone_form_state.dart';

class WidgetPhoneForm extends StatefulWidget {
  final String phone;
  final Function onCloseTap;

  const WidgetPhoneForm({Key key, @required this.onCloseTap, @required this.phone}) : super(key: key);

  @override
  _WidgetPhoneFormState createState() => _WidgetPhoneFormState();
}

class _WidgetPhoneFormState extends State<WidgetPhoneForm> {
  PhoneFormBloc _phoneFormBloc;
  final TextEditingController _phoneController = TextEditingController();

  final underline = new UnderlineInputBorder(
    borderSide: BorderSide(
      color: AppColor.GREY,
    ),
  );

  @override
  void initState() {
    super.initState();
    _phoneFormBloc = BlocProvider.of<PhoneFormBloc>(context);
    if(widget.phone!=null){
      _phoneController.text = widget?.phone ?? '';
    }else{
      _phoneController.text = '';
    }
    _phoneController.addListener(_onPhoneChange);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<PhoneFormBloc, PhoneFormState>(
      listener: (context, state) async {
        if (state.isSubmitting) {
          GetSnackBarUtils.createProgress();
        }

        if (state.isSuccess) {
          BlocProvider.of<ProfileBloc>(context).add(RefreshProfile());
          await HttpHandler.resolve(status: state.status);
        }

        if (state.isFailure) {
          await HttpHandler.resolve(status: state.status);
        }
      },
      child:
          BlocBuilder<PhoneFormBloc, PhoneFormState>(builder: (context, state) {
        return Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10), topRight: Radius.circular(10))),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Row(
                children: [
                  Expanded(
                    child: Text(
                      "Số điện thoại",
                      style: AppStyle.DEFAULT_LARGE_BOLD,
                    ),
                  ),
                  GestureDetector(
                    onTap: widget.onCloseTap,
                    child: Container(
                      width: 25,
                      height: 25,
                      child: Image.asset('assets/images/img_close_round.png'),
                    ),
                  )
                ],
              ),
              WidgetSpacer(
                height: 5,
              ),
              _buildTextFieldPhone(),
              WidgetSpacer(
                height: 10,
              ),
              _buildButtonSubmit()
            ],
          ),
        );
      }),
    );
  }

  bool get isPopulated => _phoneController.text.isNotEmpty;

  bool isSubmitButtonEnabled() {
    return _phoneFormBloc.state.isFormValid &&
        isPopulated &&
        !_phoneFormBloc.state.isSubmitting;
  }

  _buildButtonSubmit() {
    return WidgetLoginButton(
      onTap: () {
        if (isSubmitButtonEnabled()) {
          _phoneFormBloc.add(PhoneFormSubmitEvent(
            phone: _phoneController.text.trim(),
          ));
        }
      },
      isEnable: isSubmitButtonEnabled(),
      text: "Cập nhật",
    );
  }

  _buildTextFieldPhone() {
    return TextFormField(
        controller: _phoneController,
        validator: (_) {
          print('validator');
          return !_phoneFormBloc.state.isPhoneValid
              ? ''
              : null;
        },
        decoration: InputDecoration(
            disabledBorder: underline,
            enabledBorder: underline,
            focusedBorder: underline,
            hintText: "Nhập số điện thoại của bạn",
            hintStyle: AppStyle.DEFAULT_LARGE.copyWith(color: AppColor.GREY)));
  }

  void _onPhoneChange() {
    _phoneFormBloc.add(PhoneChanged(
      phone: _phoneController.text,
    ));
  }
}
