
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:memory_lifes/app/constants/color/color.dart';
import 'package:memory_lifes/app/constants/style/style.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_login_button.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_spacer.dart';
import 'package:memory_lifes/presentation/screen/menu/account/bloc/profile_bloc.dart';
import 'package:memory_lifes/presentation/screen/menu/account/bloc/profile_event.dart';
import 'package:memory_lifes/presentation/screen/profile_user/email_form/bloc/email_form_bloc.dart';
import 'package:memory_lifes/presentation/screen/profile_user/email_form/bloc/email_form_event.dart';
import 'package:memory_lifes/utils/handler/http_handler.dart';
import 'package:memory_lifes/utils/locale/app_localization.dart';
import 'package:memory_lifes/utils/snackbar/get_snack_bar_utils.dart';

import 'bloc/email_form_state.dart';

class WidgetEmailForm extends StatefulWidget {
  final String email;
  final Function onCloseTap;

  const WidgetEmailForm(
      {Key key, @required this.onCloseTap, @required this.email})
      : super(key: key);

  @override
  _WidgetEmailFormState createState() => _WidgetEmailFormState();
}

class _WidgetEmailFormState extends State<WidgetEmailForm> {
  EmailFormBloc _emailFormBloc;
  final TextEditingController _emailController = TextEditingController();

  final underline = new UnderlineInputBorder(
    borderSide: BorderSide(
      color: AppColor.GREY,
    ),
  );

  @override
  void initState() {
    super.initState();
    _emailFormBloc = BlocProvider.of<EmailFormBloc>(context);
    _emailController.text = widget?.email ?? '';
    _emailController.addListener(_onEmailChange);
    _dropdownMenuItems = buildDropdownMenuItems(_companies);
    _selectedCompany = _dropdownMenuItems[0].value;
  }

  List<Company> _companies = Company.getCompanies();
  List<DropdownMenuItem<Company>> _dropdownMenuItems;
  Company _selectedCompany;

  List<DropdownMenuItem<Company>> buildDropdownMenuItems(List companies) {
    List<DropdownMenuItem<Company>> items = List();
    for (Company company in companies) {
      items.add(
        DropdownMenuItem(
          value: company,
          child: Text(company.name),
        ),
      );
    }
    return items;
  }

  onChangeDropdownItem(Company selectedCompany) {
    setState(() {
      _selectedCompany = selectedCompany;
    });
  }


  @override
  Widget build(BuildContext context) {
    return BlocListener<EmailFormBloc, EmailFormState>(
      listener: (context, state) async {
        if (state.isSubmitting) {
          GetSnackBarUtils.createProgress();
        }

        if (state.isSuccess) {
          BlocProvider.of<ProfileBloc>(context).add(RefreshProfile());
          await HttpHandler.resolve(status: state.status);
        }

        if (state.isFailure) {
          await HttpHandler.resolve(status: state.status);
        }
      },
      child:
          BlocBuilder<EmailFormBloc, EmailFormState>(builder: (context, state) {
        return Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10), topRight: Radius.circular(10))),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Row(
                children: [
                  Expanded(
                    child: Text(
                      "Email",
                      style: AppStyle.DEFAULT_LARGE_BOLD,
                    ),
                  ),
                  GestureDetector(
                    onTap: widget.onCloseTap,
                    child: Container(
                      width: 25,
                      height: 25,
                      child: Image.asset('assets/images/img_close_round.png'),
                    ),
                  )
                ],
              ),
              WidgetSpacer(
                height: 5,
              ),
              _buildTextFieldEmail(),
              WidgetSpacer(
                height: 10,
              ),
              _buildButtonSubmit()
            ],
          ),
        );
      }),
    );
  }

  bool get isPopulated => _emailController.text.isNotEmpty;

  bool isSubmitButtonEnabled() {
    return _emailFormBloc.state.isFormValid &&
        isPopulated &&
        !_emailFormBloc.state.isSubmitting;
  }

  _buildButtonSubmit() {
    return WidgetLoginButton(
      onTap: () {
        if (isSubmitButtonEnabled()) {
          _emailFormBloc.add(EmailFormSubmitEvent(
            email: _emailController.text.trim(),
          ));
        }
      },
      isEnable: isSubmitButtonEnabled(),
      text:"Cập nhật",
    );
  }

  _buildTextFieldEmail() {
    return TextFormField(
        controller: _emailController,
//        validator: (_) {
//          print('validator');
//          return !_emailFormBloc.state.isEmailValid ? '' : null;
//        },
        decoration: InputDecoration(
            disabledBorder: underline,
            enabledBorder: underline,
            focusedBorder: underline,
            hintText:
                "Nhập email của bạn",
            hintStyle: AppStyle.DEFAULT_LARGE.copyWith(color: AppColor.GREY)));
  }

  void _onEmailChange() {
    _emailFormBloc.add(EmailChanged(
      email: _emailController.text,
    ));
  }
}
class Company {
  int id;
  String name;

  Company(this.id, this.name);

  static List<Company> getCompanies() {
    return <Company>[
      Company(1, 'name'),
      Company(2, 'nữ'),
      Company(3, 'khác'),
    ];
  }
}
