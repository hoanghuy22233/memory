import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_spacer.dart';
import 'package:memory_lifes/presentation/screen/menu/account/bloc/bloc.dart';
import 'package:memory_lifes/presentation/screen/profile_user/gender/bloc/bloc.dart';
import 'package:memory_lifes/utils/dio/dio_status.dart';
import 'package:memory_lifes/utils/handler/http_handler.dart';

class WidgetGender extends StatefulWidget {
  final int title;

  const WidgetGender({Key key, this.title}) : super(key: key);
  @override
  _WidgetGenderState createState() => _WidgetGenderState();
}

class _WidgetGenderState extends State<WidgetGender> {
  List<Company> _companies = Company.getCompanies();
  List<DropdownMenuItem<Company>> _dropdownMenuItems;
  Company _selectedCompany;
  @override
  void initState() {
    super.initState();
    _dropdownMenuItems = buildDropdownMenuItems(_companies);
    if (widget.title != 0 && widget.title != null && widget.title > 0) {
      _selectedCompany = _dropdownMenuItems[widget.title - 1].value;
    } else {
      _selectedCompany = _dropdownMenuItems[0].value;
    }
  }

  List<DropdownMenuItem<Company>> buildDropdownMenuItems(List companies) {
    List<DropdownMenuItem<Company>> items = List();
    for (Company company in companies) {
      items.add(
        DropdownMenuItem(
          value: company,
          child: Text(company.name),
        ),
      );
    }
    return items;
  }

  onChangeDropdownItem(Company selectedCompany) {
    setState(() {
      _selectedCompany = selectedCompany;
    });
    BlocProvider.of<GenderBloc>(context)
        .add(GenderSubmitted(_selectedCompany.id));
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileBloc, ProfileState>(
      builder: (context, ProfileState) {
        if (ProfileState is ProfileLoaded) {
          return BlocListener<GenderBloc, GenderState>(
            listener: (context, state) async {
              if (state.status != null) {
                if (state.status.code == DioStatus.API_PROGRESS_NOTIFY) {
                  await HttpHandler.resolve(status: state.status);
                }
                if (state.status.code == DioStatus.API_SUCCESS) {
                  BlocProvider.of<ProfileBloc>(context).add(LoadProfile());
                  await HttpHandler.resolve(status: state.status);
                }
                if (state.status.code == DioStatus.API_FAILURE_NOTIFY) {
                  await HttpHandler.resolve(status: state.status);
                }
              }
            },
            child: DropdownButton(
              value: _selectedCompany,
              items: _dropdownMenuItems,
              onChanged: onChangeDropdownItem,
            ),
          );
        } else {
          return WidgetSpacer();
        }
      },
    );
  }
}

class Company {
  int id;
  String name;

  Company(this.id, this.name);

  static List<Company> getCompanies() {
    return <Company>[
      Company(1, 'Nam'),
      Company(2, 'Nữ'),
      Company(3, 'Khác'),
    ];
  }
}
