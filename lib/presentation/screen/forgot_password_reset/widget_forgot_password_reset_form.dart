import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:memory_lifes/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_login_button.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_login_input.dart';
import 'package:memory_lifes/presentation/router.dart';
import 'package:memory_lifes/presentation/screen/forgot_password_reset/bloc/bloc.dart';
import 'package:memory_lifes/utils/common/common_utils.dart';
import 'package:memory_lifes/utils/snackbar/barrel_snack_bar.dart';

class WidgetForgotPasswordResetForm extends StatefulWidget {
  final String username;
  final String otpCode;

  const WidgetForgotPasswordResetForm({Key key, this.username, this.otpCode})
      : super(key: key);

  @override
  _WidgetForgotPasswordResetFormState createState() =>
      _WidgetForgotPasswordResetFormState();
}

class _WidgetForgotPasswordResetFormState
    extends State<WidgetForgotPasswordResetForm> {
  ForgotPasswordResetBloc _forgotPasswordBloc;

  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _confirmPasswordController =
      TextEditingController();

  bool autovalidate = false;
  bool obscurePassword = true;
  bool obscureConfirmPassword = true;

  bool get isPopulated =>
      _passwordController.text.isNotEmpty &&
      _confirmPasswordController.text.isNotEmpty;

  @override
  void initState() {
    super.initState();
    _forgotPasswordBloc = BlocProvider.of<ForgotPasswordResetBloc>(context);
    _passwordController.addListener(_onPasswordChange);
    _confirmPasswordController.addListener(_onConfirmPasswordChange);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ForgotPasswordResetBloc, ForgotPasswordResetState>(
      listener: (context, state) async {
        if (state.isSubmitting) {
          GetSnackBarUtils.createProgress();
        }

        if (state.isSuccess) {
          await GetSnackBarUtils.createSuccess(message: state.message);
          Get.until((route) => route.settings.name == BaseRouter.LOGIN);
        }

        if (state.isFailure) {
          GetSnackBarUtils.createError(message: state.message);
          setState(() {
            autovalidate = true;
          });
        }
      },
      child: BlocBuilder<ForgotPasswordResetBloc, ForgotPasswordResetState>(
        builder: (context, state) {
          return Padding(
            padding: EdgeInsets.all(20),
            child: Form(
              child: Column(
                children: [
                  _buildTextFieldPassword(),
                  WidgetSpacer(
                    height: 10,
                  ),
                  _buildTextFieldConfirmPassword(),
                  WidgetSpacer(
                    height: 15,
                  ),
                  _buildButtonForgotPasswordReset(state)
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  bool isForgotPasswordResetButtonEnabled() {
    return _forgotPasswordBloc.state.isFormValid &&
        isPopulated &&
        !_forgotPasswordBloc.state.isSubmitting;
  }

  _buildButtonForgotPasswordReset(ForgotPasswordResetState state) {
    return WidgetLoginButton(
      onTap: () {
        if (isForgotPasswordResetButtonEnabled()) {
          _forgotPasswordBloc.add(ForgotPasswordResetSubmitted(
              username: widget.username,
              otpCode: widget.otpCode,
              password: _passwordController.text,
              confirmPassword: _confirmPasswordController.text));
          AppCommonUtils.disposeKeyboard();
        }
      },
      isEnable: isForgotPasswordResetButtonEnabled(),
      text: "Thay đổi",
    );
  }

  _buildTextFieldConfirmPassword() {
    return WidgetLoginInput(
      inputController: _confirmPasswordController,
      obscureText: true,
      autovalidate: autovalidate,
      validator: (_) {
        return !_forgotPasswordBloc.state.isConfirmPasswordValid
            ? "Nhập lại mật khẩu của bạn"
            : null;
      },
      hint: "Nhập lại mật khẩu *",
      leadIcon: new Icon(
        Icons.lock_outline,
        color: Colors.blue,
      ),
    );
  }

  _buildTextFieldPassword() {
    return WidgetLoginInput(
      inputController: _passwordController,
      obscureText: true,
      autovalidate: autovalidate,
      validator: (_) {
        return !_forgotPasswordBloc.state.isPasswordValid
            ? "Nhập mật khẩu của bạn"
            : null;
      },
      hint: "Mật khẩu *",
      leadIcon: new Icon(
        Icons.lock_outline,
        color: Colors.blue,
      ),
    );
  }

  void _onPasswordChange() {
    _forgotPasswordBloc.add(PasswordChanged(
        password: _passwordController.text,
        confirmPassword: _confirmPasswordController.text));
  }

  void _onConfirmPasswordChange() {
    _forgotPasswordBloc.add(ConfirmPasswordChanged(
        password: _passwordController.text,
        confirmPassword: _confirmPasswordController.text));
  }
}
