import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:memory_lifes/model/repo/user_repository.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_spacer.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_touch_hide_keyboard.dart';
import 'package:memory_lifes/presentation/screen/forgot_password_reset/bloc/forgot_password_reset_bloc.dart';
import 'package:memory_lifes/presentation/screen/forgot_password_reset/widget_forgot_password_reset_form.dart';

class ForgotPasswordResetScreen extends StatefulWidget {
  @override
  _ForgotPasswordResetScreenState createState() =>
      _ForgotPasswordResetScreenState();
}

class _ForgotPasswordResetScreenState extends State<ForgotPasswordResetScreen> {
  String _username;
  String _otpCode;

  _onArgument() {
    Future.delayed(Duration.zero, () async {
      final Map arguments = ModalRoute.of(context).settings.arguments as Map;
      setState(() {
        _username = arguments['username'];
        _otpCode = arguments['otp_code'];
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _onArgument();
  }

  @override
  Widget build(BuildContext context) {
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    return WidgetTouchHideKeyBoard(
      child: Scaffold(
        body: SafeArea(
          top: false,
          child: BlocProvider(
              create: (context) =>
                  ForgotPasswordResetBloc(userRepository: userRepository),
              child: Stack(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage('assets/images/background.png'),
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      // WidgetSpacer(
                      //   height: MediaQuery.of(context).size.height / 3,
                      // ),
                      Center(
                        child: Text(
                          "Thay đổi mật khẩu",
                          style: TextStyle(
                              color: Colors.blue,
                              fontSize: 18,
                              fontWeight: FontWeight.w700),
                        ),
                      ),
                      WidgetSpacer(
                        height: 10,
                      ),
                      _buildForgotPasswordResetForm(),
                      WidgetSpacer(
                        height: 20,
                      ),
                    ],
                  )
                ],
              )),
        ),
      ),
    );
  }

  _buildForgotPasswordResetForm() => WidgetForgotPasswordResetForm(
        username: _username,
        otpCode: _otpCode,
      );
}
