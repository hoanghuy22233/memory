import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:memory_lifes/app/constants/color/color.dart';
import 'package:memory_lifes/model/entity/new.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_touch_hide_keyboard.dart';
import 'package:memory_lifes/presentation/screen/diary_edit/diary_edit_form/bloc/bloc.dart';
import 'package:memory_lifes/presentation/screen/diary_edit/diary_edit_form/bloc/diary_edit_form_bloc.dart';
import 'package:memory_lifes/presentation/screen/diary_post/diary_post_form/barrel_diary_post_form.dart';
import 'package:memory_lifes/presentation/screen/diary_post/diary_post_form/bloc/bloc.dart';
import 'package:memory_lifes/presentation/screen/diary_post/widget_post_diary_appbar.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

import 'diary_edit_form/widget_edit_diary_form.dart';

class EditDiaryScreen extends StatefulWidget {
  @override
  _EditDiaryScreenState createState() => _EditDiaryScreenState();
}

class _EditDiaryScreenState extends State<EditDiaryScreen >
    with AutomaticKeepAliveClientMixin<EditDiaryScreen> {
  PanelController _panelController = new PanelController();
  News post;

  _onArgument() {
    Future.delayed(Duration.zero, () async {
      final Map arguments = ModalRoute.of(context).settings.arguments as Map;
      setState(() {
        post = arguments['post'];
      });
      BlocProvider.of<EditUpdateFormBloc>(context)
          .add(EditUpdateFormLoaded(post: post));

    });
  }

  @override
  void initState() {
    super.initState();
    _onArgument();
  }


  @override
  Widget build(BuildContext context) {
    return WidgetTouchHideKeyBoard(
      child: Scaffold(
        body: Container(
          child: _buildContent(),
        ),
      ),
    );
  }

  Widget _buildContent() {
    return Container(
        child: Column(children: [
      Container(),
      Expanded(child: BlocBuilder<EditUpdateFormBloc, EditUpdateFormState>(
        builder: (context, state) {
          return SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 0),
                width: double.infinity,
                color: AppColor.WHITE,
                child: _buildForm(),
              ));
        },
      ))
    ]));
  }

  Widget _buildForm() => WidgetEditDiaryForm(post: post,);

  @override
  bool get wantKeepAlive => true;
}
