import 'dart:io';

import 'package:camera_camera/page/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_picker/image_picker.dart';
import 'package:memory_lifes/model/entity/new.dart';
import 'package:memory_lifes/model/entity/run.dart';
import 'package:memory_lifes/model/repo/user_repository.dart';
import 'package:memory_lifes/presentation/screen/diary_edit/diary_edit_form/bloc/bloc.dart';
import 'package:memory_lifes/presentation/screen/diary_edit/diary_edit_form/bloc/diary_edit_form_bloc.dart';
import 'package:memory_lifes/presentation/screen/diary_edit/diary_edit_form/bloc/diary_edit_form_state.dart';
import 'package:memory_lifes/presentation/screen/diary_post/diary_post_form/widget_button_post.dart';
import 'package:memory_lifes/presentation/screen/diary_post/diary_post_form/widget_form_post_input.dart';
import 'package:memory_lifes/utils/handler/http_handler.dart';

class WidgetEditDiaryForm extends StatefulWidget {
  final News post;
  List<String> _images111 = [];

  WidgetEditDiaryForm({Key key, @required this.post}) : super(key: key);
//  @override
//  _WidgetProductUpdateFormState createState() =>
//      _WidgetProductUpdateFormState();

  @override
  State<StatefulWidget> createState() {
    return _WidgetEditDiaryFormState();
  }
}

class _WidgetEditDiaryFormState extends State<WidgetEditDiaryForm> {
  EditUpdateFormBloc _editUpdateFormBloc;
  final _picker = ImagePicker();
  PickedFile avatarFile;
  File croppedFile;

  final ImagePicker picker = ImagePicker();

  TextEditingController _nameTextController = TextEditingController();
  TextEditingController _descriptionTextController = TextEditingController();
  List<IconsItem> _companies = IconsItem.getIcon();
  List<DropdownMenuItem<IconsItem>> _dropdownMenuItems;
  IconsItem _selectedCompany;
  IconsItem icon;

  //get current Position
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

  Position _currentPosition;
  Position _diaryPosition;
  String _currentAddress;
  String _diaryAddress;

  _getCurrentLocation() {
    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
        print("${_currentPosition.latitude}, ${_currentPosition.longitude}");

        _getAddressFromLatLng();
      });
      BlocProvider.of<EditUpdateFormBloc>(context).add(LatLongsChanged(
          lat: _currentPosition.latitude, long: _currentPosition.longitude));
    }).catchError((e) {
      print(e);
    });
  }

  _getAddressFromLatLng() async {
    try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);

      Placemark place = p[0];

      setState(() {
        _currentAddress =
            "${place.name} ${place.subLocality} - ${place.subAdministrativeArea} - ${place.administrativeArea} - ${place.country}";
      });
    } catch (e) {
      print(e);
    }
  }

  _getAddressFromDiary() async {
    try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          _diaryPosition.latitude, _diaryPosition.longitude);

      Placemark place = p[0];

      setState(() {
        _diaryAddress =
        "${place.name} ${place.subLocality} - ${place.subAdministrativeArea} - ${place.administrativeArea} - ${place.country}";
      });
    } catch (e) {
      print(e);
    }
  }

  List<DropdownMenuItem<IconsItem>> buildDropdownMenuItems(List companies) {
    List<DropdownMenuItem<IconsItem>> items = List();
    for (IconsItem company in companies) {
      items.add(
        DropdownMenuItem(
          value: company,
          child: Image.asset(
            company.icon,
            height: 40,
            width: 40,
          ),
        ),
      );
    }
    return items;
  }

  onChangeDropdownItem(IconsItem selectedCompany) {
    setState(() {
      _selectedCompany = selectedCompany;
    });
    BlocProvider.of<EditUpdateFormBloc>(context)
        .add(IconsChanged(_selectedCompany.id));
  }

  @override
  void initState() {
    super.initState();
    _editUpdateFormBloc = BlocProvider.of<EditUpdateFormBloc>(context);
    _nameTextController.addListener(_onNameChange);
    _descriptionTextController.addListener(_onDescriptionChange);

    _nameTextController.text = widget.post.title;
    _descriptionTextController.text = widget.post.content;
    _dropdownMenuItems = buildDropdownMenuItems(_companies);

    if (widget.post.typeStatus > 0 && widget.post.typeStatus != null) {
      _selectedCompany = _dropdownMenuItems[widget.post.typeStatus - 1].value;
    } else {
      _selectedCompany = _dropdownMenuItems[0].value;
    }

    _diaryPosition = Position(latitude: widget.post.lat, longitude: widget.post.long);
    _getAddressFromDiary();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<EditUpdateFormBloc, EditUpdateFormState>(
      listener: (context, state) async {
        if (state.isSubmitting) {
          print('---isSubmitting---');
          print(state.status);
          await HttpHandler.resolve(status: state.status);
        }

        if (state.isSuccess) {
          print('---isSuccess---');
          print(state.status);

          await HttpHandler.resolve(status: state.status);
        }

        if (state.isFailure) {
          print('---isFailure---');
          print(state.status);
          await HttpHandler.resolve(status: state.status);
        }
      },
      child: BlocBuilder<EditUpdateFormBloc, EditUpdateFormState>(
          builder: (context, state) {
        return Container(
          child: Form(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 89,
                  // color: Colors.black,
                  child: Stack(
                    children: [
                      Positioned.fill(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(left: 20, top: 20),
                              child: GestureDetector(
                                onTap: () {
                                  Navigator.pop(context);
                                },
                                child: Image.asset(
                                  "assets/images/back_all.png",
                                  height: 25,
                                  width: 25,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Positioned.fill(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(right: 20, top: 20),
                              child: _buildSubmitButton(),
                            )
                          ],
                        ),
                      ),
                      Positioned.fill(
                          child: FractionallySizedBox(
                        widthFactor: .5,
                        child: Padding(
                          padding: EdgeInsets.only(top: 20),
                          child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                "Sửa bài viết",
                                style: TextStyle(
                                    color: Colors.black, fontSize: 16),
                              )),
                        ),
                      ))
                    ],
                  ),
                ),
                Divider(
                  height: 1,
                  thickness: 1,
                  color: Colors.black12,
                ),
                SizedBox(
                  height: 10,
                ),
                state.typeStatus!=null ?_buildContent(context, state):_buildContentEdit(context, state),
              //  _buildContent(context, state),
                state.lat!=null && state.long!=null?_buildPositionEdit(context, state):_buildPosition(context, state),

                Container(
                  padding: EdgeInsets.only(left: 20, right: 20, top: 20),
                  child: WidgetFormInput(
                    controller: _nameTextController,
                    text: '',
                    hint: "Tiêu đề",
                    // validator: _productPostFormBloc.state.name.isNotEmpty?? '',
                  ),
                ),
                Divider(
                  height: 1,
                  thickness: 1,
                  color: Colors.black12,
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  child: WidgetFormInput(
                    controller: _descriptionTextController,
                    text: '',
                    hint: "Bạn đang nghĩ gì?",

                    // validator: _productPostFormBloc.state.name.isNotEmpty?? '',
                  ),
                ),
                Stack(
                  children: [
                    widget.post.image != null
                        ? Container(
                            width: MediaQuery.of(context).size.width,
                            height: 300,
                            child: Image.network(
                              widget.post.image,
                              fit: BoxFit.cover,
                            ),
                          )
                        : Container(),
                    croppedFile != null
                        ? Container(
                            width: MediaQuery.of(context).size.width,
                            height: 300,
                            child: Image.file(
                              croppedFile,
                              fit: BoxFit.cover,
                            ),
                          )
                        : Container(),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      //camera
                      MaterialButton(
                        onPressed: () async {
                          _getImageFromCamera();
                        },
                        color: Colors.blue,
                        textColor: Colors.white,
                        child: Icon(Icons.camera_alt),
                        padding: EdgeInsets.all(10),
                        shape: CircleBorder(),
                      ),
                      Tooltip(
                        message: "Đính kèm",
                        child: MaterialButton(
                          onPressed: () {
                            _getImageFromGallery();
                          },
                          color: Colors.blue,
                          textColor: Colors.white,
                          child: Image.asset(
                            "assets/images/paperclip.png",
                            scale: 1.5,
                          ),
                          padding: EdgeInsets.all(10),
                          shape: CircleBorder(),
                        ),
                      ),
                      MaterialButton(
                        onPressed: () {
                          _getCurrentLocation();
                        },
                        color: Colors.blue,
                        textColor: Colors.white,
                        child: Image.asset(
                          "assets/images/local.png",
                          scale: 1.5,
                        ),
                        padding: EdgeInsets.all(10),
                        shape: CircleBorder(),
                      ),
                      Container(
                          child: new DropdownButtonHideUnderline(
                        child: DropdownButton(
                          value: _selectedCompany,
                          items: _dropdownMenuItems,
                          onChanged: onChangeDropdownItem,
                        ),
                      )),
                      // MaterialButton(
                      //   color: Colors.blue,
                      //   textColor: Colors.white,
                      //   child: Image.asset(
                      //     "assets/images/speed.png",
                      //     scale: 1.5,
                      //   ),
                      //   padding: EdgeInsets.all(10),
                      //   shape: CircleBorder(),
                      //   onPressed: () {
                      //     _askFavColor();
                      //   },
                      // ),
                    ],
                  ),
                )
              ],
            ),
          ),
        );
      }),
    );
  }

  _buildSubmitButton() {
    if (_editUpdateFormBloc.state.images != null && widget.post.image != null) {
      return WidgetButtonPost(
        onTap: () {
          if (_editUpdateFormBloc.state.checkReadyToOrder()) {
            _editUpdateFormBloc
                .add(EditUpdateFormSubmitted(widget.post.id, widget.post));
          }
        },
        isEnable: _editUpdateFormBloc.state.checkReadyToOrder(),
        text: "Lưu",
      );
    } else {
      return WidgetButtonPost(
        onTap: () {
          if (_editUpdateFormBloc.state.checkReadyToOrder()) {
            _editUpdateFormBloc
                .add(EditUpdateSubmitted(widget.post.id, widget.post));
          }
        },
        isEnable: _editUpdateFormBloc.state.checkReadyToOrder(),
        text: "Lưu",
      );
    }
  }

  void _onNameChange() {
    _editUpdateFormBloc.add(NameUpdateChanged(name: _nameTextController.text));
  }

  void _onDescriptionChange() {
    _editUpdateFormBloc.add(DesUpdateChanged(
      description: _descriptionTextController.text,
    ));
  }

  Future<Null> _getImageFromGallery() async {
    croppedFile = null;
    final pickedFile = await _picker.getImage(source: ImageSource.gallery,imageQuality: 25,);
    setState(() {
      croppedFile = File(pickedFile.path);
    });
    if (croppedFile != null) {
      BlocProvider.of<EditUpdateFormBloc>(context)
          .add(ImageUpdateLoaded(image: croppedFile));
    }
  }
  Future<Null> _getImageFromCamera() async {
    croppedFile = await showDialog(
        context: context,
        builder: (context) => Camera(
          mode: CameraMode.fullscreen,
          onChangeCamera: (direction, _) {
            print('--------------');
            print('$direction');
            print('--------------');
          },

          // imageMask: CameraFocus.square(
          //   color: Colors.black.withOpacity(0.5),
          // ),
        ));
    setState(() {
      //croppedFile = File(pickedFile.path);
    });
    if (croppedFile != null) {
      BlocProvider.of<EditUpdateFormBloc>(context)
          .add(ImageUpdateLoaded(image: croppedFile));
    }
  }


  @override
  void dispose() {
    _nameTextController.dispose();
    _descriptionTextController.dispose();
    super.dispose();
  }

  Widget _buildContent(BuildContext context, EditUpdateFormState state) {
    if (state.typeStatus == null) {
      return Container();
    }
      else {
        return Row(
          children: [
            SizedBox(
              width: 20,
            ),
            Text("Đang "),
            Image.asset(
              _selectedCompany.icon,
              height: 15,
              width: 15,
            ),
            Text(
              " cảm thấy ",
            ),
            Text(
              _selectedCompany.name,
              style: TextStyle(
                  color: Colors.blue,
                  fontSize: 16,
                  fontWeight: FontWeight.bold),
            ),
          ],
        );
      }

  }
  Widget _buildContentEdit(BuildContext context, EditUpdateFormState state) {
    if (widget.post.typeStatus != null && widget.post.typeStatus != -1) {
      return Row(
        children: [
          SizedBox(
            width: 20,
          ),
          Text("Đang "),
          Image.asset(
            _selectedCompany.icon,
            height: 15,
            width: 15,
          ),
          Text(
            " cảm thấy ",
          ),
          Text(
            _selectedCompany.name,
            style: TextStyle(
                color: Colors.blue,
                fontSize: 16,
                fontWeight: FontWeight.bold),
          ),
        ],
      );
    } else {
      return icon!=null?Row(
        children: [
          SizedBox(
            width: 20,
          ),
          Text("Đang "),
          Image.asset(
            icon.icon,
            height: 15,
            width: 15,
          ),
          Text(
            " cảm thấy ",
          ),
          Text(
            icon.name,
            style: TextStyle(
                color: Colors.blue,
                fontSize: 16,
                fontWeight: FontWeight.bold),
          ),
        ],
      ):Container();
    }
  }

  Widget _buildPosition(BuildContext context, EditUpdateFormState state) {
    if (widget.post.lat == null && widget.post.long == null &&
        state.lat != 0 &&
        state.long != 0) {
      return Container();
    } else{
      return  _diaryPosition != null && _diaryAddress != "" && _diaryAddress != null? Row(
        children: <Widget>[
          SizedBox(
            width: 20,
            height: 20,
          ),
          Icon(
            Icons.location_on,
            color: Colors.blue,
          ),
          SizedBox(
            width: 8,
          ),
          Expanded(
            child: Container(
              child: _diaryPosition != null && _diaryAddress != "" && _diaryAddress != null
                  ? Text(_diaryAddress,
                  style: Theme.of(context).textTheme.bodyText2)
                  : Container()
            ),
          ),
          SizedBox(
            width: 8,
          ),
        ],
      ):Container();
    }
  }
  Widget _buildPositionEdit(BuildContext context, EditUpdateFormState state) {
    if (state.lat == null &&
        state.lat == 0 &&
        state.long == null &&
        state.long == 0) {
      return Container();
    } else{
      return Row(
        children: <Widget>[
          SizedBox(
            width: 20,
            height: 20,
          ),
          Icon(
            Icons.location_on,
            color: Colors.blue,
          ),
          SizedBox(
            width: 8,
          ),
          Expanded(
            child: Container(
                child: _currentPosition != null && _currentAddress != "" && _currentAddress != null
                    ? Text(_currentAddress,
                    style: Theme.of(context).textTheme.bodyText2)
                    : Container()
            ),
          ),
          SizedBox(
            width: 8,
          ),
        ],
      );
    }
  }

  Future<Null> _askFavColor() async {
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (context) =>
                    EditUpdateFormBloc(userRepository: userRepository),
              ),
            ],
            child: AlertDialog(
              title: Text('Hoạt động'),
              content: Container(
                width: double.minPositive,
                child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: runItem.length,
                  itemBuilder: (BuildContext context, int index) {
                    return ListTile(
                      title: Text(runItem[index].name),
                      onTap: () {
                        Navigator.pop(context);
                        BlocProvider.of<EditUpdateFormBloc>(context)
                            .add(StatusChangeds(runItem[index].id));
                      },
                    );
                  },
                ),
              ),
            ),
          );
        });
  }
}

class IconsItem {
  final String icon, name;
  final int id;
  IconsItem({this.id, this.icon, this.name});

  static List<IconsItem> getIcon() {
    return <IconsItem>[
      IconsItem(
        id: 1,
        name: "Hạnh phúc",
        icon: "assets/images/happy.png",
      ),
      IconsItem(
        id: 2,
        name: "Bực bội",
        icon: "assets/images/angry.png",
      ),
      IconsItem(
        id: 3,
        name: "Lạnh lẽo",
        icon: "assets/images/cold.png",
      ),
      IconsItem(
        id: 4,
        name: "Đang yêu",
        icon: "assets/images/lovely.png",
      ),
      IconsItem(
        id: 5,
        name: "tuyệt vời",
        icon: "assets/images/smile.png",
      ),
      IconsItem(
        id: 6,
        name: "ngon lành",
        icon: "assets/images/yummy.png",
      ),
    ];
  }
}
