import 'package:flutter/material.dart';
import 'package:memory_lifes/app/constants/color/color.dart';
import 'package:memory_lifes/app/constants/style/style.dart';

class WidgetFormInputUpdate extends StatefulWidget {
  final String text;
  final String hint;
  final TextEditingController controller;
  final String validatorText;
  final bool validator;
  final TextInputType inputType;
  final String initialValue;
  final ValueChanged<String> onChange;

  const WidgetFormInputUpdate(
      {Key key,
      this.text,
      this.hint,
      this.controller,
      this.validatorText,
      this.validator = true,
      this.inputType,
      this.initialValue,
      this.onChange})
      : super(key: key);

  @override
  _WidgetFormInputUpdateState createState() => _WidgetFormInputUpdateState();
}

class _WidgetFormInputUpdateState extends State<WidgetFormInputUpdate> {
  final underline = new UnderlineInputBorder(
    borderSide: BorderSide(
      color: AppColor.GREY,
    ),
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            widget.text,
            style: AppStyle.DEFAULT_SMALL,
          ),
          TextFormField(
            initialValue: widget.initialValue,
            controller: widget.controller,
            onChanged: widget.onChange,
            autovalidate: true,
            maxLines: 1,
            keyboardType: widget.inputType ?? TextInputType.text,
            textAlign: TextAlign.left,
            validator: (a) {
              return !widget.validator ? widget.validatorText : '';
            },
            style: TextStyle(fontWeight: FontWeight.bold),
            decoration: InputDecoration.collapsed(
              hintText: widget.hint,
            ),
          ),
          SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }
}
