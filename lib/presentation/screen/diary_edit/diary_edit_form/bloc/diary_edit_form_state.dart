import 'dart:io';

import 'package:memory_lifes/utils/dio/dio_status.dart';

class EditUpdateFormState {
  final int id;
  final File images;
  final String title;
  final String content;
  final double lat;
  final double long;
  // final IconItem icon;
  // final Run run;
  final int typeStatus;
  final int typeWork;
  final bool isSubmitting;
  final bool isSuccess;
  final bool isFailure;
  final DioStatus status;

  EditUpdateFormState(
      {this.id,
      this.images,
      this.title,
      this.content,
      this.lat,
      this.long,
      this.typeStatus,
      this.typeWork,
      this.isSubmitting,
      this.isSuccess,
      this.isFailure,
      this.status});

  factory EditUpdateFormState.empty() {
    return EditUpdateFormState(
        id: null,
        images: null,
        title: null,
        content: null,
        lat: null,
        long: null,
        typeStatus: null,
        typeWork: null,
        isSubmitting: false,
        isSuccess: false,
        isFailure: false,
        status: null);
  }

  factory EditUpdateFormState.loading(EditUpdateFormState state) {
    return EditUpdateFormState(
        id: state.id,
        images: state.images,
        title: state.title,
        content: state.content,
        lat: state.lat,
        long: state.long,
        typeStatus: state.typeStatus,
        typeWork: state.typeWork,
        isSubmitting: true,
        isSuccess: false,
        isFailure: false,
        status: state.status);
  }

  factory EditUpdateFormState.failure(EditUpdateFormState state) {
    return EditUpdateFormState(
        id: state.id,
        images: state.images,
        title: state.title,
        content: state.content,
        lat: state.lat,
        long: state.long,
        typeStatus: state.typeStatus,
        typeWork: state.typeWork,
        isSuccess: false,
        isSubmitting: false,
        isFailure: true,
        status: state.status);
  }

  factory EditUpdateFormState.success(EditUpdateFormState state) {
    return EditUpdateFormState(
        id: state.id,
        images: state.images,
        title: state.title,
        content: state.content,
        lat: state.lat,
        long: state.long,
        typeStatus: state.typeStatus,
        typeWork: state.typeWork,
        isSuccess: true,
        isSubmitting: false,
        isFailure: false,
        status: state.status);
  }

  EditUpdateFormState update(
      {int id,
      File images,
      String title,
      String content,
      double lat,
      double long,
      int typeStatus,
      int typeWork,
      bool isSubmitting,
      bool isSuccess,
      bool isFailure,
      DioStatus status}) {
    return copyWith(
        id: id,
        images: images,
        title: title,
        content: content,
        lat: lat,
        long: long,
        typeStatus: typeStatus,
        typeWork: typeWork,
        isSubmitting: false,
        isSuccess: false,
        isFailure: false,
        status: status);
  }

  EditUpdateFormState copyWith({
    int id,
    File images,
    String title,
    String content,
    double lat,
    double long,
    int typeStatus,
    int typeWork,
    bool isSubmitting,
    bool isSuccess,
    bool isFailure,
    DioStatus status,
  }) {
    return EditUpdateFormState(
      id: id ?? this.id,
      images: images ?? this.images,
      title: title ?? this.title,
      content: content ?? this.content,
      lat: lat ?? this.lat,
      long: long ?? this.long,
      typeStatus: typeStatus ?? this.typeStatus,
      typeWork: typeWork ?? this.typeWork,
      isSubmitting: isSubmitting ?? this.isSubmitting,
      isSuccess: isSuccess ?? this.isSuccess,
      isFailure: isFailure ?? this.isFailure,
      status: status ?? this.status,
    );
  }

  @override
  String toString() {
    return 'ProductUpdateFormState{id:$id,images: $images, title: $title, content: $content,lat:$lat,long:$long,'
        '  typeStatus: $typeStatus,typeWork:$typeWork, '
        'isSubmitting: $isSubmitting, isSuccess: $isSuccess, isFailure: $isFailure, status: $status}';
  }

  bool checkReadyToOrder() {
    var ready = true;

//    if (address == null) {
//      ready = false;
//    }
//    if (wallet == null) {
//      ready = false;
//    }
    return ready;
  }
}
