import 'dart:io';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:memory_lifes/app/constants/endpoint/app_endpoint.dart';
import 'package:memory_lifes/app/constants/navigator/navigator.dart';
import 'package:memory_lifes/model/entity/new.dart';
import 'package:memory_lifes/model/repo/user_repository.dart';
import 'package:memory_lifes/presentation/screen/diary_edit/diary_edit_form/bloc/diary_edit_form_event.dart';
import 'package:memory_lifes/presentation/screen/diary_edit/diary_edit_form/bloc/diary_edit_form_state.dart';
import 'package:memory_lifes/utils/dio/dio_error_util.dart';
import 'package:memory_lifes/utils/dio/dio_status.dart';
import 'package:meta/meta.dart';

class EditUpdateFormBloc
    extends Bloc<EditUpdateFormEvent, EditUpdateFormState> {
  final UserRepository _userRepository;

  EditUpdateFormBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  EditUpdateFormState get initialState => EditUpdateFormState.empty();

  @override
  Stream<EditUpdateFormState> mapEventToState(
      EditUpdateFormEvent event) async* {
    if (event is InitEditUpdateForm) {
      yield* _mapInitEditUpdateFormToState();
    } else if (event is ImageUpdateLoaded) {
      yield* _mapLoadImageLoadedToState(event.image);
    } else if (event is EditUpdateFormLoaded) {
      yield* _mapEditUpdateFormLoadedToState(event.post);
    } else if (event is NameUpdateChanged) {
      yield* _mapNameChangedToState(event.name);
    } else if (event is DesUpdateChanged) {
      yield* _mapDesChangedToState(event.description);
    } else if (event is LatLongsChanged) {
      yield* _mapLoadLatLongLoadedToState(event.lat, event.long);
    } else if (event is IconsChanged) {
      yield* _mapIconChangedToState(event.typeStatus);
    } else if (event is StatusChangeds) {
      yield* _mapTypeWordChangedToState(event.typeWork);
    } else if (event is EditUpdateFormSubmitted) {
      yield* _mapEditUpdateFormSubmittedToState(event.id, event.news);
    } else if (event is EditUpdateSubmitted) {
      yield* _mapEditUpdateSubmittedToState(event.id, event.news);
    }
  }

  Stream<EditUpdateFormState> _mapInitEditUpdateFormToState() async* {
    yield EditUpdateFormState.empty();
  }

  Stream<EditUpdateFormState> _mapLoadImageLoadedToState(File images) async* {
    yield state.update(images: images);
  }

  Stream<EditUpdateFormState> _mapLoadLatLongLoadedToState(
      double lat, double long) async* {
    yield state.update(lat: lat ?? 0, long: long ?? 0);
  }

  Stream<EditUpdateFormState> _mapEditUpdateFormLoadedToState(
      News post) async* {
    yield state.update(id: post.id);
  }

  Stream<EditUpdateFormState> _mapNameChangedToState(String name) async* {
    yield state.update(title: name);
  }

  Stream<EditUpdateFormState> _mapDesChangedToState(String des) async* {
    yield state.update(content: des);
  }

  Stream<EditUpdateFormState> _mapTypeWordChangedToState(int typeWork) async* {
    yield state.update(typeWork: typeWork);
  }

  Stream<EditUpdateFormState> _mapIconChangedToState(int typeStatus) async* {
    yield state.update(typeStatus: typeStatus);
  }

  Stream<EditUpdateFormState> _mapEditUpdateFormSubmittedToState(
      int id, News news) async* {
    try {
      if (state.checkReadyToOrder()) {
        yield EditUpdateFormState.loading(state.copyWith(
            status:
                DioStatus(message: '', code: DioStatus.API_PROGRESS_NOTIFY)));

        var response = await _userRepository.editDiary(
            id: state.id,
            images: state.images,
            title: state.title,
            content: state.content,
            type: state?.typeStatus ?? -1,
            work: state?.typeWork ?? -1);

        if (response.status == Endpoint.SUCCESS) {
          yield EditUpdateFormState.success(state.copyWith(
              status: DioStatus(
                  message: response.message,
                  code: DioStatus.API_SUCCESS_NOTIFY)));
          Future.delayed(Duration(seconds: 2), () {
            AppNavigator.navigateNavigation();
          });
        } else {
          yield EditUpdateFormState.failure(state.copyWith(
              status: DioStatus(
                  message: response.message,
                  code: DioStatus.API_FAILURE_NOTIFY)));
        }
      }
    } catch (e) {
      yield EditUpdateFormState.failure(
          state.update(status: DioErrorUtil.handleError(e)));
    }
  }

  Stream<EditUpdateFormState> _mapEditUpdateSubmittedToState(
      int id, News news) async* {
    try {
      if (state.checkReadyToOrder()) {
        yield EditUpdateFormState.loading(state.copyWith(
            status:
                DioStatus(message: '', code: DioStatus.API_PROGRESS_NOTIFY)));

        var response = await _userRepository.editDiaryNoImage(
            id: state.id,
            title: state.title,
            content: state.content,
            type: state?.typeStatus ?? -1,
            work: state?.typeWork ?? -1);

        if (response.status == Endpoint.SUCCESS) {
          yield EditUpdateFormState.success(state.copyWith(
              status: DioStatus(
                  message: response.message,
                  code: DioStatus.API_SUCCESS_NOTIFY)));
          Future.delayed(Duration(seconds: 2), () {
            AppNavigator.navigateNavigation();
          });
        } else {
          yield EditUpdateFormState.failure(state.copyWith(
              status: DioStatus(
                  message: response.message,
                  code: DioStatus.API_FAILURE_NOTIFY)));
        }
      }
    } catch (e) {
      yield EditUpdateFormState.failure(
          state.update(status: DioErrorUtil.handleError(e)));
    }
  }
}
