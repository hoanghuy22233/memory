import 'dart:io';

import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:memory_lifes/model/entity/new.dart';

class EditUpdateFormEvent extends Equatable {
  const EditUpdateFormEvent();

  List<Object> get props => [];
}

class InitEditUpdateForm extends EditUpdateFormEvent {
  @override
  String toString() {
    return 'InitEditUpdateForm{}';
  }
}

class EditUpdateFormLoaded extends EditUpdateFormEvent {
  final News post;

  const EditUpdateFormLoaded({@required this.post});

  @override
  List<Object> get props => [post];

  @override
  String toString() {
    return 'EditUpdateFormLoaded{post: $post}';
  }
}

class LatLongsChanged extends EditUpdateFormEvent {
  final double lat, long;

  LatLongsChanged({this.lat, this.long});

  List<Object> get props => [lat, long];

  @override
  String toString() {
    return 'LatLongsChanged{lat: $lat,long:$long}';
  }
}

class ImageUpdateLoaded extends EditUpdateFormEvent {
  final File image;

  ImageUpdateLoaded({this.image});

  List<Object> get props => [image];

  @override
  String toString() {
    return 'ImageLoaded{image: $image}';
  }
}

class NameUpdateChanged extends EditUpdateFormEvent {
  final String name;

  NameUpdateChanged({this.name});

  List<Object> get props => [name];

  @override
  String toString() {
    return 'NameChanged{name: $name}';
  }
}

class DesUpdateChanged extends EditUpdateFormEvent {
  final String description;

  DesUpdateChanged({this.description});

  List<Object> get props => [description];

  @override
  String toString() {
    return 'DesChanged{description: $description}';
  }
}

class IconsChanged extends EditUpdateFormEvent {
  final int typeStatus;

  IconsChanged(this.typeStatus);

  List<Object> get props => [typeStatus];

  @override
  String toString() {
    return 'IconsChanged{ typeStatus: $typeStatus}';
  }
}

class StatusChangeds extends EditUpdateFormEvent {
  final int typeWork;

  StatusChangeds(this.typeWork);

  List<Object> get props => [typeWork];

  @override
  String toString() {
    return 'StatusChangeds{typeWork: $typeWork}';
  }
}

class EditUpdateFormSubmitted extends EditUpdateFormEvent {
  final int id;
  final News news;

  EditUpdateFormSubmitted(this.id, this.news);

  @override
  List<Object> get props => [id, news];

  @override
  String toString() {
    return 'EditUpdateFormSubmitted{id:$id,products:$news}';
  }
}

class EditUpdateSubmitted extends EditUpdateFormEvent {
  final int id;
  final News news;

  EditUpdateSubmitted(this.id, this.news);

  @override
  List<Object> get props => [id, news];

  @override
  String toString() {
    return 'EditUpdateSubmitted{id:$id,products:$news}';
  }
}
