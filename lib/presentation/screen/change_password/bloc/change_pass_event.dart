import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class ChangePassEvent extends Equatable {
  const ChangePassEvent();

  @override
  List<Object> get props => [];
}

class PassWordOldChanged extends ChangePassEvent {
  final String Oldpass;

  const PassWordOldChanged({@required this.Oldpass});

  @override
  List<Object> get props => [Oldpass];

  @override
  String toString() {
    return 'PassOldChanged{email: $Oldpass}';
  }
}

class PasswordsChanged extends ChangePassEvent {
  final String password;
  final String confirmPassword;

  PasswordsChanged({@required this.password, @required this.confirmPassword});

  @override
  List<Object> get props => [];

  @override
  String toString() {
    return 'PasswordsChanged{password: $password, confirmPassword: $confirmPassword}';
  }
}

class ConfirmPasswordsChanged extends ChangePassEvent {
  final String password;
  final String confirmPassword;

  ConfirmPasswordsChanged(
      {@required this.password, @required this.confirmPassword});

  @override
  List<Object> get props => [];

  @override
  String toString() {
    return 'ConfirmPasswordChanged{password: $password, confirmPassword: $confirmPassword}';
  }
}

class ChangePassSubmitted extends ChangePassEvent {
  final String oldPass;
  final String password;
  final String confirmPassword;

  const ChangePassSubmitted({
    @required this.oldPass,
    @required this.password,
    @required this.confirmPassword,
  });

  @override
  List<Object> get props => [oldPass, password, confirmPassword];

  @override
  String toString() {
    return 'Submitted{email: $oldPass, password: $password, confirmPassword: $confirmPassword}';
  }
}
