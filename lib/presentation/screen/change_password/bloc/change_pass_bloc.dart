import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:memory_lifes/app/constants/barrel_constants.dart';
import 'package:memory_lifes/model/repo/barrel_repo.dart';
import 'package:memory_lifes/presentation/screen/change_password/bloc/change_pass_event.dart';
import 'package:memory_lifes/presentation/screen/change_password/bloc/change_pass_state.dart';
import 'package:memory_lifes/utils/utils.dart';
import 'package:meta/meta.dart';

class ChangePassBloc extends Bloc<ChangePassEvent, ChangPassState> {
  final UserRepository _userRepository;

  ChangePassBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  ChangPassState get initialState => ChangPassState.empty();

  @override
  Stream<ChangPassState> mapEventToState(ChangePassEvent event) async* {
    if (event is PassWordOldChanged) {
      yield* _mapPassWordOldChangedToState(event.Oldpass);
    } else if (event is PasswordsChanged) {
      yield* _mapPasswordChangedToState(event.password, event.confirmPassword);
    } else if (event is ConfirmPasswordsChanged) {
      yield* _mapConfirmPasswordChangedToState(
          event.password, event.confirmPassword);
    } else if (event is ChangePassSubmitted) {
      yield* _mapFormSubmittedToState(
          event.oldPass, event.password, event.confirmPassword);
    }
  }

  Stream<ChangPassState> _mapPassWordOldChangedToState(String passOld) async* {
    yield state.update(
      isPasswordOldValid: Validator.isValidPassword(passOld),
    );
  }

  Stream<ChangPassState> _mapPasswordChangedToState(
      String password, String confirmPassword) async* {
    var isPasswordValid = Validator.isValidPassword(password);
    var isMatched = true;

    if (confirmPassword.isNotEmpty) {
      isMatched = password == confirmPassword;
    }

    yield state.update(
        isPasswordValid: isPasswordValid, isConfirmPasswordValid: isMatched);
  }

  Stream<ChangPassState> _mapConfirmPasswordChangedToState(
      String password, String confirmPassword) async* {
    var isConfirmPasswordValid = Validator.isValidPassword(confirmPassword);
    var isMatched = true;

    if (password.isNotEmpty) {
      isMatched = password == confirmPassword;
    }

    yield state.update(
      isConfirmPasswordValid: isConfirmPasswordValid && isMatched,
    );
  }

  Stream<ChangPassState> _mapFormSubmittedToState(
      String passOld, String password, String confirmPassword) async* {
    yield ChangPassState.loading();

    try {
      var response = await _userRepository.changePassWord(
          oldPass: passOld,
          password: password,
          confirmPassword: confirmPassword);
      if (response.status == Endpoint.SUCCESS) {
        yield ChangPassState.success(message: response.message);
      } else {
        yield ChangPassState.failure(message: response.message);
      }
    } catch (e) {
      print("------ Register: $e");
      yield ChangPassState.failure();
    }

    //need refactor
//    var isValidUsername = Validator.isValidUsername(username);
//    var isValidPassword = Validator.isValidPassword(password);
//    var isValidConfirmPassword = Validator.isValidPassword(confirmPassword);
//    var isMatched = true;
//    if (isValidPassword && isValidConfirmPassword) {
//      isMatched = password == confirmPassword;
//    }
//
//    var newState = state.update(
//        isUsernameValid: isValidUsername,
//        isPasswordValid: isValidPassword,
//        isConfirmPasswordValid: isValidConfirmPassword && isMatched);
//
//    yield newState;
//
//    if (newState.isFormValid) {
//      try {
//        var response = await _userRepository.registerApp(
//            username: username,
//            password: password,
//            confirmPassword: confirmPassword);
//        if(response.status == Endpoint.SUCCESS){
//          yield RegisterState.success(message: response.message);
//        } else {
//          yield RegisterState.failure(message: response.message);
//        }
//      } catch (e) {
//        print("------ Register: $e");
//        yield RegisterState.failure();
//      }
//    }
  }
}
