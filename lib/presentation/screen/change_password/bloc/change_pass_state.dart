import 'package:flutter/cupertino.dart';

class ChangPassState {
  final bool isPasswordOldValid;
  final bool isPasswordValid;
  final bool isConfirmPasswordValid;
  final bool isSubmitting;
  final bool isSuccess;
  final bool isFailure;
  final String message;

  bool get isFormValid =>
      isPasswordOldValid && isPasswordValid && isConfirmPasswordValid;

  ChangPassState(
      {@required this.isPasswordOldValid,
      @required this.isPasswordValid,
      @required this.isConfirmPasswordValid,
      @required this.isSubmitting,
      @required this.isSuccess,
      @required this.isFailure,
      @required this.message});

  factory ChangPassState.empty() {
    return ChangPassState(
        isPasswordOldValid: true,
        isPasswordValid: true,
        isConfirmPasswordValid: true,
        isSubmitting: false,
        isSuccess: false,
        isFailure: false,
        message: '');
  }

  factory ChangPassState.loading() {
    return ChangPassState(
        isPasswordOldValid: true,
        isPasswordValid: true,
        isConfirmPasswordValid: true,
        isSubmitting: true,
        isSuccess: false,
        isFailure: false,
        message: '');
  }

  factory ChangPassState.failure({String message}) {
    return ChangPassState(
        isPasswordOldValid: true,
        isPasswordValid: true,
        isConfirmPasswordValid: true,
        isSuccess: false,
        isSubmitting: false,
        isFailure: true,
        message: message);
  }

  factory ChangPassState.success({String message}) {
    return ChangPassState(
        isPasswordOldValid: true,
        isPasswordValid: true,
        isConfirmPasswordValid: true,
        isSuccess: true,
        isSubmitting: false,
        isFailure: false,
        message: message);
  }

  ChangPassState update(
      {bool isPasswordOldValid,
      bool isPasswordValid,
      bool isConfirmPasswordValid,
      String message}) {
    return copyWith(
      isPasswordOldValid: isPasswordOldValid,
      isPasswordValid: isPasswordValid,
      isConfirmPasswordValid: isConfirmPasswordValid,
      isSubmitting: false,
      isSuccess: false,
      isFailure: false,
      message: message,
    );
  }

  ChangPassState copyWith({
    bool isPasswordOldValid,
    bool isPasswordValid,
    bool isConfirmPasswordValid,
    bool isSubmitting,
    bool isSuccess,
    bool isFailure,
    String message,
  }) {
    return ChangPassState(
      isPasswordOldValid: isPasswordOldValid ?? this.isPasswordOldValid,
      isPasswordValid: isPasswordValid ?? this.isPasswordValid,
      isConfirmPasswordValid:
          isConfirmPasswordValid ?? this.isConfirmPasswordValid,
      isSubmitting: isSubmitting ?? this.isSubmitting,
      isSuccess: isSuccess ?? this.isSuccess,
      isFailure: isFailure ?? this.isFailure,
      message: message ?? this.message,
    );
  }
}
