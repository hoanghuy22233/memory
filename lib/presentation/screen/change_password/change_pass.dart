import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:memory_lifes/app/constants/navigator/navigator.dart';
import 'package:memory_lifes/model/repo/user_repository.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_touch_hide_keyboard.dart';
import 'package:memory_lifes/presentation/screen/change_password/bloc/change_pass_bloc.dart';
import 'package:memory_lifes/presentation/screen/change_password/widget_change_pass_form.dart';
import 'package:memory_lifes/presentation/screen/sign_up/bloc/register_bloc.dart';
import 'package:memory_lifes/presentation/screen/sign_up/widget_register_form.dart';

class ChangePassScreen extends StatefulWidget {
  ChangePassScreen({Key key}) : super(key: key);
  @override
  _ChangePassScreenState createState() => _ChangePassScreenState();
}

class _ChangePassScreenState extends State<ChangePassScreen> {
  @override
  Widget build(BuildContext context) {
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    return WidgetTouchHideKeyBoard(
      child: Scaffold(
        body: BlocProvider(
          create: (context) => ChangePassBloc(userRepository: userRepository),
          child: Stack(
            alignment: Alignment.center,
            children: [
              Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/images/background.png'),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                child: _buildChangeForm(),
              )
            ],
          ),
        ),
      ),
    );
  }

  _buildChangeForm() => WidgetChangePassForm();
}
