import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:memory_lifes/app/constants/barrel_constants.dart';
import 'package:memory_lifes/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_login_button.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_login_input.dart';
import 'package:memory_lifes/presentation/screen/change_password/bloc/bloc.dart';
import 'package:memory_lifes/utils/snackbar/barrel_snack_bar.dart';
import 'package:memory_lifes/utils/utils.dart';

class WidgetChangePassForm extends StatefulWidget {
  @override
  _WidgetChangePassFormState createState() => _WidgetChangePassFormState();
}

class _WidgetChangePassFormState extends State<WidgetChangePassForm> {
  ChangePassBloc _changePassBloc;

  final TextEditingController _oldPassController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _confirmPasswordController =
      TextEditingController();

  bool obscurePassword = true;
  bool obscurePasswordOld = true;
  bool obscureConfirmPassword = true;
  bool autoValidate = false;

  bool get isPopulated =>
      _oldPassController.text.isNotEmpty &&
      _passwordController.text.isNotEmpty &&
      _confirmPasswordController.text.isNotEmpty;

  @override
  void initState() {
    super.initState();
    _changePassBloc = BlocProvider.of<ChangePassBloc>(context);
    _oldPassController.addListener(_onOldPassChange);
    _passwordController.addListener(_onPasswordChanged);
    _confirmPasswordController.addListener(_onPasswordConfirmChanged);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ChangePassBloc, ChangPassState>(
      listener: (context, state) async {
        if (state.isSubmitting) {
          GetSnackBarUtils.createProgress();
        }

        if (state.isSuccess) {
          await GetSnackBarUtils.createSuccess(message: state.message);
          AppNavigator.navigateLogin();
        }

        if (state.isFailure) {
          GetSnackBarUtils.createError(message: state.message);
          setState(() {
            autoValidate = true;
          });
        }
      },
      child: BlocBuilder<ChangePassBloc, ChangPassState>(
        builder: (context, state) {
          return Padding(
            padding: EdgeInsets.all(20),
            child: Form(
              child: Column(
                children: [
                  _buildTextFieldUsername(),
                  WidgetSpacer(
                    height: 10,
                  ),
                  _buildTextFieldPassword(),
                  WidgetSpacer(
                    height: 10,
                  ),
                  _buildTextFieldConfirmPassword(),
                  WidgetSpacer(
                    height: 20,
                  ),
                  _buildButtonRegister(state)
                ],
              ),
            ),
          );
        },
      ),
    );
  }

//  bool isRegisterButtonEnabled() {
//    return _registerBloc.state.isFormValid &&
//        isPopulated &&
//        !_registerBloc.state.isSubmitting;
//  }

  bool isRegisterButtonEnabled() {
    return _changePassBloc.state.isSubmitting;
  }



  _buildButtonRegister(ChangPassState state) {
    return WidgetChangePassButton(
      onTap: () {
        if (!isRegisterButtonEnabled()) {
          _changePassBloc.add(ChangePassSubmitted(
              oldPass: _oldPassController.text,
              password: _passwordController.text,
              confirmPassword: _confirmPasswordController.text));
          FocusScope.of(context).unfocus();
        }
      },
      isEnable: !isRegisterButtonEnabled(),
      text: "Thay đổi mật khẩu",
    );
  }

  _buildTextFieldConfirmPassword() {
    return WidgetLoginInput(
      inputController: _confirmPasswordController,
      validator: AppValidation.validatePassword("Nhập lại mật khẩu"),
      autovalidate: autoValidate,
      hint: "Nhập lại Mật khẩu *",
      obscureText: obscureConfirmPassword,
      endIcon: IconButton(
        icon: Icon(
          obscureConfirmPassword
              ? MaterialCommunityIcons.eye_outline
              : MaterialCommunityIcons.eye_off_outline,
          color: AppColor.GREY,
        ),
        onPressed: () {
          setState(() {
            obscureConfirmPassword = !obscureConfirmPassword;
          });
        },
      ),
      leadIcon: new Icon(
        Icons.lock_outline,
        color: Colors.blue,
      ),
    );
  }

  _buildTextFieldPassword() {
    return WidgetLoginInput(
      inputController: _passwordController,
      validator: AppValidation.validatePassword("Nhập mật khẩu mới"),
      autovalidate: autoValidate,
      hint: "Mật khẩu mới*",
      obscureText: obscurePassword,
      endIcon: IconButton(
        icon: Icon(
          obscurePassword
              ? MaterialCommunityIcons.eye_outline
              : MaterialCommunityIcons.eye_off_outline,
          color: AppColor.GREY,
        ),
        onPressed: () {
          setState(() {
            obscurePassword = !obscurePassword;
          });
        },
      ),
      leadIcon: new Icon(
        Icons.lock_outline,
        color: Colors.blue,
      ),
    );
  }

  _buildTextFieldUsername() {
    return WidgetLoginInput(
      inputController: _oldPassController,
      validator: AppValidation.validateUserName("Nhập mật khẩu cũ của bạn"),
      autovalidate: autoValidate,
      hint: "Nhập mật khẩu cũ *",
      endIcon: IconButton(
        icon: Icon(
          obscurePasswordOld
              ? MaterialCommunityIcons.eye_outline
              : MaterialCommunityIcons.eye_off_outline,
          color: AppColor.GREY,
        ),
        onPressed: () {
          setState(() {
            obscurePasswordOld = !obscurePasswordOld;
          });
        },
      ),
      leadIcon: new Icon(
        Icons.lock_outline,
        color: Colors.blue,
      ),
    );
  }

  void _onOldPassChange() {
    _changePassBloc.add(PassWordOldChanged(
      Oldpass: _oldPassController.text,
    ));
  }

  void _onPasswordChanged() {
    _changePassBloc.add(PasswordsChanged(
        password: _passwordController.text,
        confirmPassword: _confirmPasswordController.text));
  }

  void _onPasswordConfirmChanged() {
    _changePassBloc.add(ConfirmPasswordsChanged(
        password: _passwordController.text,
        confirmPassword: _confirmPasswordController.text));
  }

  @override
  void dispose() {
    _oldPassController.dispose();
    _passwordController.dispose();
    _confirmPasswordController.dispose();
    super.dispose();
  }
}

class WidgetChangePassButton extends StatelessWidget {
  final Function onTap;
  final String text;
  final isEnable;

  const WidgetChangePassButton({Key key, this.onTap, this.text, this.isEnable})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width / 2,
      height: AppValue.ACTION_BAR_HEIGHT,
      child: GestureDetector(
        onTap: onTap,
        child: Card(
          elevation: 2,
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
          color: isEnable ? Colors.blue : AppColor.BUTTON_DISABLE_COLOR,
          child: Center(
              child: Text(
                text,
                style: isEnable
                    ? AppStyle.DEFAULT_MEDIUM.copyWith(color: AppColor.WHITE)
                    : AppStyle.DEFAULT_MEDIUM.copyWith(color: AppColor.BLACK),
              )),
        ),
      ),
    );
  }
}
