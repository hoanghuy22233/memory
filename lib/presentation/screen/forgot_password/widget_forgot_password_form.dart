import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:memory_lifes/app/constants/barrel_constants.dart';
import 'package:memory_lifes/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_login_button.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_login_input.dart';
import 'package:memory_lifes/presentation/screen/forgot_password/bloc/bloc.dart';
import 'package:memory_lifes/utils/snackbar/barrel_snack_bar.dart';
import 'package:memory_lifes/utils/utils.dart';

class WidgetForgotPasswordForm extends StatefulWidget {
  @override
  _WidgetForgotPasswordFormState createState() =>
      _WidgetForgotPasswordFormState();
}

class _WidgetForgotPasswordFormState extends State<WidgetForgotPasswordForm> {
  ForgotPasswordBloc _forgotPasswordBloc;

  final TextEditingController _usernameController = TextEditingController();

  bool obscurePassword = true;
  bool obscureConfirmPassword = true;

  bool get isPopulated => _usernameController.text.isNotEmpty;

  @override
  void initState() {
    super.initState();
    _forgotPasswordBloc = BlocProvider.of<ForgotPasswordBloc>(context);
    _usernameController.addListener(_onUsernameChange);
  }

  @override
  void dispose() {
    _usernameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ForgotPasswordBloc, ForgotPasswordState>(
      listener: (context, state) async {
        if (state.isSubmitting) {
          GetSnackBarUtils.createProgress();
        }
        if (state.isSuccess) {
          await GetSnackBarUtils.createSuccess(message: state.message);
          AppNavigator.navigateForgotPasswordVerify(
              username: _usernameController.text);
        }

        if (state.isFailure) {
          GetSnackBarUtils.createError(message: state.message);
        }
      },
      child: BlocBuilder<ForgotPasswordBloc, ForgotPasswordState>(
        builder: (context, state) {
          return Padding(
            padding: EdgeInsets.all(20),
            child: Form(
              child: Column(
                children: [
                  _buildTextFieldUsername(),
                  WidgetSpacer(
                    height: 20,
                  ),
                  _buildButtonForgotPassword(state)
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  bool isForgotPasswordButtonEnabled() {
    return _forgotPasswordBloc.state.isFormValid &&
        isPopulated &&
        !_forgotPasswordBloc.state.isSubmitting;
  }

  _buildButtonForgotPassword(ForgotPasswordState state) {
    return WidgetLoginButton(
      onTap: () {
        if (isForgotPasswordButtonEnabled()) {
          _forgotPasswordBloc.add(ForgotPasswordSubmitted(
            username: _usernameController.text,
          ));
          FocusScope.of(context).unfocus();
        }
      },
      isEnable: isForgotPasswordButtonEnabled(),
      text: "Tìm tài khoản",
    );
  }

  _buildTextFieldUsername() {
    return WidgetLoginInput(
      inputType: TextInputType.emailAddress,
      inputController: _usernameController,
      validator: AppValidation.validateUserName("Nhập Email của bạn"),
      hint: "Email *",
      leadIcon: new Icon(
        Icons.mail,
        color: Colors.blue,
      ),
    );
  }

  void _onUsernameChange() {
    _forgotPasswordBloc.add(UsernameChanged(
      username: _usernameController.text,
    ));
  }
}
