import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:memory_lifes/model/repo/user_repository.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_spacer.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_touch_hide_keyboard.dart';
import 'package:memory_lifes/presentation/screen/forgot_password/bloc/forgot_password_bloc.dart';
import 'package:memory_lifes/presentation/screen/forgot_password/widget_forgot_password_form.dart';

class ForgotPassword extends StatefulWidget {
  ForgotPassword({Key key}) : super(key: key);
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  @override
  Widget build(BuildContext context) {
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    return WidgetTouchHideKeyBoard(
      child: Scaffold(
        body: SafeArea(
          top: false,
          child: BlocProvider(
            create: (context) =>
                ForgotPasswordBloc(userRepository: userRepository),
            child: Stack(
              children: [
                Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/images/background.png'),
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Center(
                      child: Text(
                        "Nhập email tài khoản",
                        style: TextStyle(
                            color: Colors.blue,
                            fontSize: 18,
                            fontWeight: FontWeight.w700),
                      ),
                    ),
                    WidgetSpacer(
                      height: 10,
                    ),
                    _buildForgotPasswordForm(),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  _buildForgotPasswordForm() => WidgetForgotPasswordForm();
}
