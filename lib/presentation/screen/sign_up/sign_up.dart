import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:memory_lifes/app/constants/navigator/navigator.dart';
import 'package:memory_lifes/model/repo/user_repository.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_touch_hide_keyboard.dart';
import 'package:memory_lifes/presentation/screen/sign_up/bloc/register_bloc.dart';
import 'package:memory_lifes/presentation/screen/sign_up/widget_register_form.dart';

class SignUp extends StatefulWidget {
  SignUp({Key key}) : super(key: key);
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  @override
  Widget build(BuildContext context) {
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    return WidgetTouchHideKeyBoard(
      child: Scaffold(
        body: BlocProvider(
          create: (context) => RegisterBloc(userRepository: userRepository),
          child: Stack(
            alignment: Alignment.center,
            children: [
              Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/images/background.png'),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    _buildRegisterForm(),
                    Align(
                      alignment: Alignment.bottomCenter,
                    child: Center(
                      child: RichText(
                        text: TextSpan(
                            text: 'Bạn đã có tài khoản?  ',
                            style: TextStyle(color: Colors.black),
                            children: <TextSpan>[
                              TextSpan(
                                  text: 'Đăng nhập',
                                  style: TextStyle(
                                      color: Colors.blueAccent,
                                      fontWeight: FontWeight.bold,
                                      decoration: TextDecoration.underline),
                                  recognizer: TapGestureRecognizer()
                                    ..onTap = () {
                                      AppNavigator.navigateLogin();
                                    })
                            ]),
                      ),
                    ),),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  _buildRegisterForm() => WidgetRegisterForm();
}
