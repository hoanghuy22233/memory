import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:memory_lifes/app/constants/barrel_constants.dart';
import 'package:memory_lifes/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_login_button.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_login_input.dart';
import 'package:memory_lifes/presentation/screen/sign_up/bloc/register_bloc.dart';
import 'package:memory_lifes/presentation/screen/sign_up/bloc/register_event.dart';
import 'package:memory_lifes/presentation/screen/sign_up/bloc/register_state.dart';
import 'package:memory_lifes/utils/snackbar/barrel_snack_bar.dart';
import 'package:memory_lifes/utils/utils.dart';

class WidgetRegisterForm extends StatefulWidget {
  @override
  _WidgetRegisterFormState createState() => _WidgetRegisterFormState();
}

class _WidgetRegisterFormState extends State<WidgetRegisterForm> {
  RegisterBloc _registerBloc;

  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _confirmPasswordController =
      TextEditingController();

  bool obscurePassword = true;
  bool obscureConfirmPassword = true;
  bool autoValidate = false;

  bool get isPopulated =>
      _usernameController.text.isNotEmpty &&
      _passwordController.text.isNotEmpty &&
      _confirmPasswordController.text.isNotEmpty;

  @override
  void initState() {
    super.initState();
    _registerBloc = BlocProvider.of<RegisterBloc>(context);
    _usernameController.addListener(_onUsernameChange);
    _passwordController.addListener(_onPasswordChanged);
    _confirmPasswordController.addListener(_onPasswordConfirmChanged);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<RegisterBloc, RegisterState>(
      listener: (context, state) async {
        if (state.isSubmitting) {
          GetSnackBarUtils.createProgress();
        }

        if (state.isSuccess) {
          await GetSnackBarUtils.createSuccess(message: state.message);
          AppNavigator.navigateLogin();
        }

        if (state.isFailure) {
          GetSnackBarUtils.createError(message: state.message);
          setState(() {
            autoValidate = true;
          });
        }
      },
      child: BlocBuilder<RegisterBloc, RegisterState>(
        builder: (context, state) {
          return Padding(
            padding: EdgeInsets.all(20),
            child: Form(
              child: Column(
                children: [
                  _buildTextFieldUsername(),
                  WidgetSpacer(
                    height: 10,
                  ),
                  _buildTextFieldPassword(),
                  WidgetSpacer(
                    height: 10,
                  ),
                  _buildTextFieldConfirmPassword(),
                  WidgetSpacer(
                    height: 20,
                  ),
                  _buildButtonRegister(state)
                ],
              ),
            ),
          );
        },
      ),
    );
  }

//  bool isRegisterButtonEnabled() {
//    return _registerBloc.state.isFormValid &&
//        isPopulated &&
//        !_registerBloc.state.isSubmitting;
//  }

  bool isRegisterButtonEnabled() {
    return _registerBloc.state.isFormValid &&
        isPopulated && !_registerBloc.state.isSubmitting;
  }

  _buildButtonRegister(RegisterState state) {
    return WidgetLoginButton(
      onTap: () {
        if (isRegisterButtonEnabled()) {
          _registerBloc.add(RegisterSubmitted(
              username: _usernameController.text,
              password: _passwordController.text,
              confirmPassword: _confirmPasswordController.text));
          FocusScope.of(context).unfocus();
        }
      },
      isEnable: isRegisterButtonEnabled(),
      text: "Đăng ký",
    );
  }

  _buildTextFieldConfirmPassword() {
    return WidgetLoginInput(
      inputController: _confirmPasswordController,
      validator: AppValidation.validatePassword("Nhập lại mật khẩu"),
      autovalidate: autoValidate,
      hint: "Nhập lại Mật khẩu *",
      obscureText: obscureConfirmPassword,
      endIcon: IconButton(
        icon: Icon(
          obscureConfirmPassword
              ? MaterialCommunityIcons.eye_outline
              : MaterialCommunityIcons.eye_off_outline,
          color: AppColor.GREY,
        ),
        onPressed: () {
          setState(() {
            obscureConfirmPassword = !obscureConfirmPassword;
          });
        },
      ),
      leadIcon: new Icon(
        Icons.lock_outline,
        color: Colors.blue,
      ),
    );
  }

  _buildTextFieldPassword() {
    return WidgetLoginInput(
      inputController: _passwordController,
      validator: AppValidation.validatePassword("Nhập mật khẩu"),
      autovalidate: autoValidate,
      hint: "Mật khẩu *",
      obscureText: obscurePassword,
      endIcon: IconButton(
        icon: Icon(
          obscurePassword
              ? MaterialCommunityIcons.eye_outline
              : MaterialCommunityIcons.eye_off_outline,
          color: AppColor.GREY,
        ),
        onPressed: () {
          setState(() {
            obscurePassword = !obscurePassword;
          });
        },
      ),
      leadIcon: new Icon(
        Icons.lock_outline,
        color: Colors.blue,
      ),
    );
  }

  _buildTextFieldUsername() {
    return WidgetLoginInput(
      inputType: TextInputType.emailAddress,
      inputController: _usernameController,
      validator: AppValidation.validateUserName("Nhập Email của bạn"),
      autovalidate: autoValidate,
      hint: "Email *",
      leadIcon: new Icon(
        Icons.mail,
        color: Colors.blue,
      ),
    );
  }

  void _onUsernameChange() {
    _registerBloc.add(UsernameChanged(
      username: _usernameController.text,
    ));
  }

  void _onPasswordChanged() {
    _registerBloc.add(PasswordChanged(
        password: _passwordController.text,
        confirmPassword: _confirmPasswordController.text));
  }

  void _onPasswordConfirmChanged() {
    _registerBloc.add(ConfirmPasswordChanged(
        password: _passwordController.text,
        confirmPassword: _confirmPasswordController.text));
  }

  @override
  void dispose() {
    _usernameController.dispose();
    _passwordController.dispose();
    _confirmPasswordController.dispose();
    super.dispose();
  }
}
