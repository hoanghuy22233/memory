
import 'package:equatable/equatable.dart';
import 'package:memory_lifes/model/entity/detail_news_data.dart';
import 'package:memory_lifes/model/entity/new.dart';
import 'package:memory_lifes/utils/dio/dio_status.dart';

abstract class NewsDetailState extends Equatable {
  const NewsDetailState();

  @override
  List<Object> get props => [];
}

class NewsDetailLoading extends NewsDetailState {}

class NewsDetailLoaded extends NewsDetailState {
  final News news;

  const NewsDetailLoaded({this.news});

  @override
  List<Object> get props => [news];

  @override
  String toString() {
    return 'NewsDetailLoaded{news: $news}';
  }
}

class NewsDetailNotLoaded extends NewsDetailState {
  final DioStatus status;

  NewsDetailNotLoaded(this.status);

  @override
  String toString() {
    return 'NewsDetailNotLoaded{error: $status}';
  }
}
