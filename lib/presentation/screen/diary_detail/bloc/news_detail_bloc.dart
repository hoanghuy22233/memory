
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:memory_lifes/model/repo/barrel_repo.dart';
import 'package:memory_lifes/presentation/screen/diary_detail/bloc/news_detail_event.dart';
import 'package:memory_lifes/presentation/screen/diary_detail/bloc/news_detail_state.dart';
import 'package:memory_lifes/utils/dio/dio_error_util.dart';
import 'package:meta/meta.dart';

class NewsDetailBloc extends Bloc<NewsDetailEvent, NewsDetailState> {
  final UserRepository userRepository;

  NewsDetailBloc({@required this.userRepository}) : super();

  @override
  NewsDetailState get initialState => NewsDetailLoading();

  @override
  Stream<NewsDetailState> mapEventToState(NewsDetailEvent event) async* {
    if (event is LoadNewsDetail) {
      yield* _mapLoadNewsDetailToState(event.newsId);
    } else if (event is RefreshNewsDetail) {
      yield NewsDetailLoading();
      yield* _mapLoadNewsDetailToState(event.newsId);
    }
  }

  Stream<NewsDetailState> _mapLoadNewsDetailToState(int newsId) async* {
    try {
      final response = await userRepository.getNewsDetail(newsId: newsId);
      yield NewsDetailLoaded(news: response.data);
    } catch (e) {
      yield NewsDetailNotLoaded(DioErrorUtil.handleError(e));
    }
  }
}
