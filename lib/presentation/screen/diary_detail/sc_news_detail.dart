import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:memory_lifes/app/constants/color/color.dart';
import 'package:memory_lifes/app/constants/value/value.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_circle_progress.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_screen_error.dart';
import 'package:memory_lifes/presentation/screen/diary_detail/bloc/news_detail_bloc.dart';
import 'package:memory_lifes/presentation/screen/diary_detail/bloc/news_detail_event.dart';
import 'package:memory_lifes/presentation/screen/diary_detail/widget_news_detail_appbar.dart';
import 'package:share/share.dart';

import 'bloc/news_detail_state.dart';

class NewDetailScreen extends StatefulWidget {
  @override
  _NewDetailScreenState createState() => _NewDetailScreenState();
}

class _NewDetailScreenState extends State<NewDetailScreen> {
  int _newsId;
  bool isReadDetail = false;

  _onArgument() {
    Future.delayed(Duration.zero, () async {
      final Map arguments = ModalRoute.of(context).settings.arguments as Map;
      setState(() {
        _newsId = arguments['id'];
        print('---id---');
        print(_newsId);

        BlocProvider.of<NewsDetailBloc>(context).add(LoadNewsDetail(_newsId));
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _onArgument();
    // BlocProvider.of<NewsDetailBloc>(context)
    //     .add(LoadNewsDetail(_newsId));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.black12,
        body: Column(
          children: [
            _buildAppbar(),
            Expanded(
                child: RefreshIndicator(
                    onRefresh: () async {
                      BlocProvider.of<NewsDetailBloc>(context)
                          .add(RefreshNewsDetail(_newsId));
                      await Future.delayed(AppValue.FAKE_TIME_RELOAD);
                      return true;
                    },
                    color: AppColor.PRIMARY_COLOR,
                    backgroundColor: AppColor.THIRD_COLOR,
                    child: BlocBuilder<NewsDetailBloc, NewsDetailState>(
                      builder: (context, state) {
                        return _buildContent(state);
                      },
                    ))),
          ],
        ));
  }

  Widget _buildAppbar() => WidgetNewsDetailAppbar();

  Widget _buildContent(NewsDetailState state) {
    if (state is NewsDetailLoaded) {
      return SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            state?.news?.image != null && state?.news?.image != ""
                ? Container(
                    height: MediaQuery.of(context).size.height / 1.5,
                    width: MediaQuery.of(context).size.width,
                    child: Image.network(
                      state?.news?.image ?? '',
                      fit: BoxFit.cover,
                    ),
                  )
                : Container(),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
              child: Row(
                children: [
                  Expanded(
                    flex: 8,
                    child: Text(
                      state?.news?.title ?? '',
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: Colors.blue),
                      maxLines: null,
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: state?.news?.image != null &&
                            state?.news?.image != ""
                        ? IconButton(
                            icon: Icon(
                              Icons.share,
                              color: Colors.blue,
                            ),
                            onPressed: () {
                              Share.share(state.news.image);
                            },
                          )
                        : IconButton(
                            icon: Icon(
                              Icons.share,
                              color: Colors.blue,
                            ),
                            onPressed: () {
                              Share.share(
                                  state.news.title + "\n" + state.news.content);
                            },
                          ),
                  ),
                ],
              ),
            ),
            state?.news?.typeStatus != null && state.news.typeStatus > 0
                ? Stack(
                    children: [
                      if (state?.news?.typeStatus == 1)
                        Row(
                          children: [
                            SizedBox(
                              width: 20,
                            ),
                            Text(" -" + " Đang ",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 12,
                                )),
                            Image.asset(
                              "assets/images/happy.png",
                              height: 15,
                              width: 15,
                            ),
                            Text(" cảm thấy ",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 12,
                                )),
                            Text(
                              "hạnh phúc ",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      if (state?.news?.typeStatus == 2)
                        Row(
                          children: [
                            SizedBox(
                              width: 20,
                            ),
                            Text(" -" + " Đang ",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 12,
                                )),
                            Image.asset(
                              "assets/images/angry.png",
                              height: 15,
                              width: 15,
                            ),
                            Text(" cảm thấy ",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 12,
                                )),
                            Text(
                              "bực bội ",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      if (state?.news?.typeStatus == 3)
                        Row(
                          children: [
                            SizedBox(
                              width: 20,
                            ),
                            Text(" -" + " Đang ",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 12,
                                )),
                            Image.asset(
                              "assets/images/cold.png",
                              height: 15,
                              width: 15,
                            ),
                            Text(" cảm thấy ",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 12,
                                )),
                            Text(
                              "lạnh lẽo",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      if (state?.news?.typeStatus == 4)
                        Row(
                          children: [
                            SizedBox(
                              width: 20,
                            ),
                            Text(" -" + " Đang ",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 12,
                                )),
                            Image.asset(
                              "assets/images/lovely.png",
                              height: 15,
                              width: 15,
                            ),
                            Text(" cảm thấy ",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 12,
                                )),
                            Text(
                              "đang yêu ",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      if (state?.news?.typeStatus == 5)
                        Row(
                          children: [
                            SizedBox(
                              width: 20,
                            ),
                            Text(" -" + " Đang ",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 12,
                                )),
                            Image.asset(
                              "assets/images/smile.png",
                              height: 15,
                              width: 15,
                            ),
                            Text(" cảm thấy ",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 12,
                                )),
                            Text(
                              "tuyệt vời ",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      if (state?.news?.typeStatus == 6)
                        Row(
                          children: [
                            SizedBox(
                              width: 20,
                            ),
                            Text(" -" + " Đang ",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 12,
                                )),
                            Image.asset(
                              "assets/images/yummy.png",
                              height: 15,
                              width: 15,
                            ),
                            Text(" cảm thấy ",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 12,
                                )),
                            Text(
                              "ngon lành ",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                    ],
                  )
                : Container(),
            state?.news?.coTypeWork != null &&
                    state?.news?.typeStatus == null &&
                    state.news.coTypeWork > 0 &&
                    state.news.typeStatus > 0
                ? Stack(
                    children: [
                      if (state?.news?.coTypeWork == 1)
                        Row(
                          children: [
                            Text(" -" + " Đang ",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 12,
                                )),
                            Text(
                              "Ẩm thực ",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      if (state?.news?.coTypeWork == 2)
                        Row(
                          children: [
                            Text(" -" + " Đang ",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 12,
                                )),
                            Text(
                              " dã ngoại ",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      if (state?.news?.coTypeWork == 3)
                        Row(
                          children: [
                            Text(" -" + " Đang ",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 12,
                                )),
                            Text(
                              "đi biển ",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      if (state?.news?.coTypeWork == 4)
                        Row(
                          children: [
                            Text(" -" + " Đang ",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 12,
                                )),
                            Text(
                              "đi biển ",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      if (state?.news?.coTypeWork == 5)
                        Row(
                          children: [
                            Text(" -" + " Đang ",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 12,
                                )),
                            Text(
                              "học tập ",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      if (state?.news?.coTypeWork == 6)
                        Row(
                          children: [
                            Text(" -" + " Đang ",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 12,
                                )),
                            Text(
                              "leo núi ",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      if (state?.news?.coTypeWork == 7)
                        Row(
                          children: [
                            Text(" -" + " Đang ",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 12,
                                )),
                            Text(
                              "nghỉ ngơi ",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      if (state?.news?.coTypeWork == 8)
                        Row(
                          children: [
                            Text(" -" + " Đang ",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 12,
                                )),
                            Text(
                              "party ",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      if (state?.news?.coTypeWork == 9)
                        Row(
                          children: [
                            Text(" -" + " Đang ",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 12,
                                )),
                            Text(
                              "thể thao ",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                    ],
                  )
                : Container(),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: 20,
              ),
              child: Container(
                height: isReadDetail ? null : 50,
                child: Text(
                  state.news.content,
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            state.news.content.length > 100
                ? Column(
                    children: [
                      SizedBox(
                        height: 10,
                      ),
                      Center(
                        child: GestureDetector(
                          onTap: () {
                            setState(() {
                              isReadDetail = !isReadDetail;
                            });
                          },
                          child: isReadDetail
                              ? Text(
                                  "Thu gọn",
                                  style: TextStyle(color: Colors.blue),
                                )
                              : Text(
                                  "Xem thêm",
                                  style: TextStyle(color: Colors.blue),
                                ),
                        ),
                      ),
                    ],
                  )
                : SizedBox(),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              child: Text(
                state?.news?.date ?? '',
                style: TextStyle(color: Colors.white, fontSize: 12),
              ),
            ),
          ],
        ),
      );
    } else if (state is NewsDetailLoading) {
      return Center(
        child: WidgetCircleProgress(),
      );
    } else if (state is NewsDetailNotLoaded) {
      return WidgetScreenError(
        status: state.status,
      );
    } else {
      return Center(
        child: Text('Unknown state'),
      );
    }
  }
}
