
import 'package:flutter/material.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_appbar.dart';
import 'package:memory_lifes/utils/locale/app_localization.dart';

class WidgetNewsDetailAppbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: WidgetAppbar(
        title: "Chi tiết nhật ký",
        left: [
          Padding(
            padding: EdgeInsets.only(left: 20, top: 20),
            child: GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Image.asset(
                "assets/images/back_all.png",
                height: 25,
                width: 25,
              ),
            ),
          )
        ],
      ),
    );
  }
}
