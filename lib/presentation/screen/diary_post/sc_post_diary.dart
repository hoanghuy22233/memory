import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:memory_lifes/app/constants/color/color.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_touch_hide_keyboard.dart';
import 'package:memory_lifes/presentation/screen/diary_post/diary_post_form/barrel_diary_post_form.dart';
import 'package:memory_lifes/presentation/screen/diary_post/diary_post_form/bloc/bloc.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

class PostDiaryScreen extends StatefulWidget {
  @override
  _PostDiaryScreenState createState() => _PostDiaryScreenState();
}

class _PostDiaryScreenState extends State<PostDiaryScreen>
    with AutomaticKeepAliveClientMixin<PostDiaryScreen> {
  PanelController _panelController = new PanelController();

  @override
  Widget build(BuildContext context) {
    return WidgetTouchHideKeyBoard(
      child: Scaffold(
        body: Container(
          child: _buildContent(),
        ),
      ),
    );
  }

  Widget _buildContent() {
    return Container(
        child: Column(children: [
      Container(),
      Expanded(child: BlocBuilder<DiaryPostFormBloc, DiaryPostFormState>(
        builder: (context, state) {
          return Container(
            padding: EdgeInsets.symmetric(horizontal: 0),
            width: double.infinity,
            color: AppColor.WHITE,
            child: _buildForm(),
          );
        },
      ))
    ]));
  }

  Widget _buildForm() => WidgetDiaryPostsForm();

  @override
  bool get wantKeepAlive => true;
}
