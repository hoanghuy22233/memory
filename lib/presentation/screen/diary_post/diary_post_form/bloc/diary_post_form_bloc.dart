import 'dart:io';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:memory_lifes/app/constants/endpoint/app_endpoint.dart';
import 'package:memory_lifes/app/constants/navigator/navigator.dart';
import 'package:memory_lifes/model/repo/user_repository.dart';
import 'package:memory_lifes/presentation/screen/diary_post/diary_post_form/bloc/diary_post_form_event.dart';
import 'package:memory_lifes/presentation/screen/diary_post/diary_post_form/bloc/diary_post_form_state.dart';
import 'package:memory_lifes/utils/dio/dio_error_util.dart';
import 'package:memory_lifes/utils/dio/dio_status.dart';
import 'package:meta/meta.dart';

class DiaryPostFormBloc extends Bloc<DiaryPostFormEvent, DiaryPostFormState> {
  final UserRepository _userRepository;

  DiaryPostFormBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  DiaryPostFormState get initialState => DiaryPostFormState.empty();

  @override
  Stream<DiaryPostFormState> mapEventToState(DiaryPostFormEvent event) async* {
    if (event is InitDiaryPostForm) {
      yield* _mapInitDiaryPostFormToState();
    } else if (event is ImageLoaded) {
      yield* _mapLoadImageLoadedToState(event.image);
    } else if (event is NameProductChanged) {
      yield* _mapNameProductChangedToState(event.name);
    } else if (event is DesProductChanged) {
      yield* _mapDesChangedToState(event.description);
    } else if (event is IconChanged) {
      yield* _mapIconChangedToState(event.typeStatus);
    } else if (event is StatusChanged) {
      yield* _mapTypeWordChangedToState(event.typeWork);
    } else if (event is LatLongChanged) {
      yield* _mapLoadLatLongLoadedToState(event.lat, event.long);
    } else if (event is DiaryPostFormSubmitted) {
      yield* _mapDiaryPostFormSubmittedToState();
    } else if (event is DiaryPostSubmitted) {
      yield* _mapDiaryPostSubmittedToState();
    }
  }

  Stream<DiaryPostFormState> _mapInitDiaryPostFormToState() async* {
    yield DiaryPostFormState.empty();
  }

  Stream<DiaryPostFormState> _mapLoadImageLoadedToState(File images) async* {
    yield state.update(images: images ?? null);
  }

  Stream<DiaryPostFormState> _mapLoadLatLongLoadedToState(
      double lat, double long) async* {
    yield state.update(lat: lat ?? 0, long: long ?? 0);
  }

  // Stream<DiaryPostFormState> _mapCategoriesSelectedToState(
  //     Category category) async* {
  //   yield state.update(category: category);
  // }
  //
  // Stream<DiaryPostFormState> _mapUnitsProductSelectedToState(
  //     Units units) async* {
  //   yield state.update(units: units);
  // }

  Stream<DiaryPostFormState> _mapNameProductChangedToState(String name) async* {
    yield state.update(title: name);
  }

  Stream<DiaryPostFormState> _mapTypeWordChangedToState(int typeWork) async* {
    yield state.update(typeWork: typeWork);
  }

  Stream<DiaryPostFormState> _mapIconChangedToState(int typeStatus) async* {
    yield state.update(typeStatus: typeStatus);
  }

  Stream<DiaryPostFormState> _mapDesChangedToState(String des) async* {
    yield state.update(content: des);
  }

  Stream<DiaryPostFormState> _mapDiaryPostFormSubmittedToState() async* {
    try {
      if (state.checkReadyToOrder()) {
        yield DiaryPostFormState.loading(state.copyWith(
            status:
                DioStatus(message: '', code: DioStatus.API_PROGRESS_NOTIFY)));

        var response = await _userRepository.postDiary(
          images: state.images,
          title: state.title,
          content: state.content,
          lat: state.lat,
          long: state.long,
          type: state.typeStatus,
          work: state.typeWork,
        );

        if (response.status == Endpoint.SUCCESS) {
          yield DiaryPostFormState.success(state.copyWith(
              status: DioStatus(
                  message: response.message,
                  code: DioStatus.API_SUCCESS_NOTIFY)));
          Future.delayed(Duration(seconds: 2), () {
            AppNavigator.navigateNavigation();
          });
        } else {
          yield DiaryPostFormState.failure(state.copyWith(
              status: DioStatus(
                  message: response.message,
                  code: DioStatus.API_FAILURE_NOTIFY)));
        }
      }
    } catch (e) {
      yield DiaryPostFormState.failure(
          state.update(status: DioErrorUtil.handleError(e)));
    }
  }

  Stream<DiaryPostFormState> _mapDiaryPostSubmittedToState() async* {
    try {
      if (state.checkReadyToOrder()) {
        yield DiaryPostFormState.loading(state.copyWith(
            status:
                DioStatus(message: '', code: DioStatus.API_PROGRESS_NOTIFY)));

        var response = await _userRepository.postDiaryNoImage(
          title: state.title,
          content: state.content,
          lat: state.lat,
          long: state.long,
          type: state.typeStatus,
          work: state.typeWork,
        );

        if (response.status == Endpoint.SUCCESS) {
          yield DiaryPostFormState.success(state.copyWith(
              status: DioStatus(
                  message: response.message,
                  code: DioStatus.API_SUCCESS_NOTIFY)));
          Future.delayed(Duration(seconds: 2), () {
            AppNavigator.navigateNavigation();
          });
        } else {
          yield DiaryPostFormState.failure(state.copyWith(
              status: DioStatus(
                  message: response.message,
                  code: DioStatus.API_FAILURE_NOTIFY)));
        }
      }
    } catch (e) {
      yield DiaryPostFormState.failure(
          state.update(status: DioErrorUtil.handleError(e)));
    }
  }
}
