import 'dart:io';

import 'package:equatable/equatable.dart';

class DiaryPostFormEvent extends Equatable {
  const DiaryPostFormEvent();

  List<Object> get props => [];
}

class InitDiaryPostForm extends DiaryPostFormEvent {
  @override
  String toString() {
    return 'InitDiaryPostForm{}';
  }
}

// class CategoriesSelected extends DiaryPostFormEvent {
//  // final Category category;
//
//   CategoriesSelected(this.category);
//
//   List<Object> get props => [category];
//
//   @override
//   String toString() {
//     return 'CategoriesSelected{category: $category}';
//   }
// }

class ImageLoaded extends DiaryPostFormEvent {
  final File image;

  ImageLoaded({this.image});

  List<Object> get props => [image];

  @override
  String toString() {
    return 'ImageLoaded{image: $image}';
  }
}

class NameProductChanged extends DiaryPostFormEvent {
  final String name;

  NameProductChanged({this.name});

  List<Object> get props => [name];

  @override
  String toString() {
    return 'NameProductChanged{name: $name}';
  }
}

class DesProductChanged extends DiaryPostFormEvent {
  final String description;

  DesProductChanged({this.description});

  List<Object> get props => [description];

  @override
  String toString() {
    return 'DesProductChanged{description: $description}';
  }
}

class LatLongChanged extends DiaryPostFormEvent {
  final double lat, long;

  LatLongChanged({this.lat, this.long});

  List<Object> get props => [lat, long];

  @override
  String toString() {
    return 'LatLongChanged{lat: $lat,long:$long}';
  }
}

class IconChanged extends DiaryPostFormEvent {
  final int typeStatus;

  IconChanged(this.typeStatus);

  List<Object> get props => [typeStatus];

  @override
  String toString() {
    return 'IconChanged{ typeStatus: $typeStatus}';
  }
}

class StatusChanged extends DiaryPostFormEvent {
  final int typeWork;

  StatusChanged(this.typeWork);

  List<Object> get props => [typeWork];

  @override
  String toString() {
    return 'StatusChanged{typeWork: $typeWork}';
  }
}

class DiaryPostFormSubmitted extends DiaryPostFormEvent {
  DiaryPostFormSubmitted();

  @override
  String toString() {
    return 'DiaryPostFormSubmitted{}';
  }
}

class DiaryPostSubmitted extends DiaryPostFormEvent {
  DiaryPostSubmitted();

  @override
  String toString() {
    return 'DiaryPostSubmitted{}';
  }
}
