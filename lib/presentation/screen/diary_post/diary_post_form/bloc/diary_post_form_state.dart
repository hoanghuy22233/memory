import 'dart:io';

import 'package:memory_lifes/utils/dio/dio_status.dart';

class DiaryPostFormState {
  final File images;
  final String title;
  final String content;
  final double lat;
  final double long;
  // final IconItem icon;
  // final Run run;
  final int typeStatus;
  final int typeWork;
  final bool isSubmitting;
  final bool isSuccess;
  final bool isFailure;
  final DioStatus status;

  DiaryPostFormState(
      {this.images,
      this.title,
      this.content,
      this.lat,
      this.long,
      this.typeStatus,
      this.typeWork,
      this.isSubmitting,
      this.isSuccess,
      this.isFailure,
      this.status});

  factory DiaryPostFormState.empty() {
    return DiaryPostFormState(
        images: null,
        title: null,
        content: null,
        lat: null,
        long: null,
        typeStatus: null,
        typeWork: null,
        isSubmitting: false,
        isSuccess: false,
        isFailure: false,
        status: null);
  }

  factory DiaryPostFormState.loading(DiaryPostFormState state) {
    return DiaryPostFormState(
        images: state.images,
        title: state.title,
        content: state.content,
        lat: state.lat,
        long: state.long,
        typeStatus: state.typeStatus,
        typeWork: state.typeWork,
        isSubmitting: true,
        isSuccess: false,
        isFailure: false,
        status: state.status);
  }

  factory DiaryPostFormState.failure(DiaryPostFormState state) {
    return DiaryPostFormState(
        images: state.images,
        title: state.title,
        content: state.content,
        lat: state.lat,
        long: state.long,
        typeStatus: state.typeStatus,
        typeWork: state.typeWork,
        isSuccess: false,
        isSubmitting: false,
        isFailure: true,
        status: state.status);
  }

  factory DiaryPostFormState.success(DiaryPostFormState state) {
    return DiaryPostFormState(
        images: state.images,
        title: state.title,
        content: state.content,
        lat: state.lat,
        long: state.long,
        typeStatus: state.typeStatus,
        typeWork: state.typeWork,
        isSuccess: true,
        isSubmitting: false,
        isFailure: false,
        status: state.status);
  }

  DiaryPostFormState update(
      {File images,
      String title,
      String content,
      double lat,
      double long,
      int typeStatus,
      int typeWork,
      bool isSubmitting,
      bool isSuccess,
      bool isFailure,
      DioStatus status}) {
    return copyWith(
        images: images,
        title: title,
        content: content,
        lat: lat,
        long: long,
        typeStatus: typeStatus,
        typeWork: typeWork,
        isSubmitting: false,
        isSuccess: false,
        isFailure: false,
        status: status);
  }

  DiaryPostFormState copyWith({
    File images,
    String title,
    String content,
    double lat,
    double long,
    int typeStatus,
    int typeWork,
    bool isSubmitting,
    bool isSuccess,
    bool isFailure,
    DioStatus status,
  }) {
    return DiaryPostFormState(
      images: images ?? this.images,
      title: title ?? this.title,
      content: content ?? this.content,
      lat: lat ?? this.lat,
      long: long ?? this.long,
      typeStatus: typeStatus ?? this.typeStatus,
      typeWork: typeWork ?? this.typeWork,
      isSubmitting: isSubmitting ?? this.isSubmitting,
      isSuccess: isSuccess ?? this.isSuccess,
      isFailure: isFailure ?? this.isFailure,
      status: status ?? this.status,
    );
  }

  @override
  String toString() {
    return 'DiaryPostFormState{images: $images, title: $title, content: $content,lat:$lat,long:$long,'
        '  typeStatus: $typeStatus,typeWork:$typeWork, '
        'isSubmitting: $isSubmitting, isSuccess: $isSuccess, isFailure: $isFailure, status: $status}';
  }

  bool checkReadyToOrder() {
    var ready = true;

//    if (address == null) {
//      ready = false;
//    }
//    if (wallet == null) {
//      ready = false;
//    }
    return ready;
  }
}
