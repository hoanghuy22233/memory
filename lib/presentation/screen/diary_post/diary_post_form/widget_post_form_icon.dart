//
// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
//
// class WidgetProductPostForm extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       padding: EdgeInsets.symmetric(
//         vertical: 10,
//       ),
//       child: GestureDetector(
//         onTap: () {
//           AppNavigator.navigatePostCategory();
//         },
//         child: Row(
//           children: [
//             Container(
//               padding: EdgeInsets.symmetric(horizontal: 10),
//               child: Image.asset(
//                 "assets/images/list.png",
//                 height: 25,
//                 width: 25,
//               ),
//             ),
//             Expanded(
//               child: Padding(
//                 padding: const EdgeInsets.all(8.0),
//                 child: Text(
//                   AppLocalizations.of(context)
//                       .translate('post.product_category'),
//                   style: TextStyle(
//                       color: Color(0xff4e4e4e),
//                       fontSize: 16,
//                       fontWeight: FontWeight.bold),
//                 ),
//               ),
//             ),
//             GestureDetector(
//               onTap: () {
//                 AppNavigator.navigatePostCategory();
//               },
//               child: Container(
//                 padding: const EdgeInsets.all(8.0),
//                 child: BlocBuilder<ProductPostFormBloc, ProductPostFormState>(
//                     builder: (context, state) {
//                   return _buildContent(context, state);
//                 }),
//
// //                  Row(
// //                    children: [
// //                      Expanded(
// //                        child: BlocBuilder<ProductPostFormBloc, ProductPostFormState>(
// //                            builder: (context, state) {
// //                              return _buildContent(context, state);
// //                            }),
// //                      ),
// //
// //                    ],
// //                  ),
//               ),
//             ),
//             Icon(Icons.chevron_right, color: Color(0xff4e4e4e))
//           ],
//         ),
//       ),
//     );
//   }
//
//   Widget _buildContent(BuildContext context, ProductPostFormState state) {
//     if (state.category == null) {
//       return Container();
//     } else {
//       return Text(
//         state.category.name,
//         style: TextStyle(fontSize: 16),
//       );
//     }
//   }
// }
