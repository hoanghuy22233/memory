import 'package:flutter/material.dart';
import 'package:memory_lifes/model/entity/icon.dart';

class WidgetIconSelected extends StatelessWidget {
  final IconItem icon;
  final Function onTap;

  const WidgetIconSelected({Key key, @required this.icon, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 20),
      child: GestureDetector(
        onTap: onTap,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Image.asset(
                  icon.icon,
                  height: 25,
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  child: Text(
                    icon.name,
                    style: TextStyle(fontSize: 16),
                  ),
                ),
              ],
            ),
            Container(
              child: Divider(
                height: 1,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
