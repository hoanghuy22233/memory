import 'dart:io';

import 'package:android_intent/android_intent.dart';
import 'package:camera_camera/page/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_picker/image_picker.dart';
import 'package:memory_lifes/model/entity/run.dart';
import 'package:memory_lifes/model/repo/user_repository.dart';
import 'package:memory_lifes/presentation/screen/diary_post/diary_post_form/bloc/bloc.dart';
import 'package:memory_lifes/presentation/screen/diary_post/diary_post_form/widget_button_post.dart';
import 'package:memory_lifes/presentation/screen/diary_post/diary_post_form/widget_form_post_input.dart';
import 'package:memory_lifes/utils/handler/http_handler.dart';
import 'package:sliverbar_with_card/sliverbar_with_card.dart';

import 'bloc/diary_post_form_event.dart';

class WidgetDiaryPostsForm extends StatefulWidget {
  @override
  _WidgetDiaryPostsFormState createState() => _WidgetDiaryPostsFormState();
}

class _WidgetDiaryPostsFormState extends State<WidgetDiaryPostsForm> {
  DiaryPostFormBloc _diaryPostFormBloc;
  final _picker = ImagePicker();
  PickedFile avatarFile;
  File croppedFile;

  bool _isButtonDisabled;

  final ImagePicker picker = ImagePicker();

  TextEditingController _nameTextController = TextEditingController();
  TextEditingController _descriptionTextController = TextEditingController();
  List<IconsItem> _companies = IconsItem.getIcon();
  List<DropdownMenuItem<IconsItem>> _dropdownMenuItems;
  IconsItem _selectedCompany;

  List<DropdownMenuItem<IconsItem>> buildDropdownMenuItems(List companies) {
    List<DropdownMenuItem<IconsItem>> items = List();
    for (IconsItem company in companies) {
      items.add(
        DropdownMenuItem(
          value: company,
          child: Image.asset(
            company.icon,
            height: 40,
            width: 40,
          ),
        ),
      );
    }
    return items;
  }

  onChangeDropdownItem(IconsItem selectedCompany) {
    setState(() {
      _selectedCompany = selectedCompany;
    });
    BlocProvider.of<DiaryPostFormBloc>(context)
        .add(IconChanged(_selectedCompany.id));
  }

  //get current Position
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

  Position _currentPosition;
  String _currentAddress;

  Future _checkGps() async {
    if (!(await Geolocator().isLocationServiceEnabled())) {
      if (Theme.of(context).platform == TargetPlatform.android) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("Không thể lấy vị trí hiện tại của bạn!"),
              content:
              const Text('Hãy bật GPS và thử lại'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Ok'),
                  onPressed: () {
                    final AndroidIntent intent = AndroidIntent(
                        action: 'android.settings.LOCATION_SOURCE_SETTINGS');
                    intent.launch();
                    Navigator.of(context, rootNavigator: true).pop();
                  },
                ),
              ],
            );
          },
        );
      }
    }
  }

  _getCurrentLocation() {
    _checkGps();
    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
        print("${_currentPosition.latitude}, ${_currentPosition.longitude}");

        _getAddressFromLatLng();
      });
      BlocProvider.of<DiaryPostFormBloc>(context).add(LatLongChanged(
          lat: _currentPosition.latitude, long: _currentPosition.longitude));
    }).catchError((e) {
      print(e);
    });
  }

  _getAddressFromLatLng() async {
    try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);

      Placemark place = p[0];

      setState(() {
        _currentAddress =
            "${place.name} ${place.subLocality} - ${place.subAdministrativeArea} - ${place.administrativeArea} - ${place.country}";
      });
    } catch (e) {
      print(e);
    }
  }

  @override
  void initState() {
    super.initState();
    _diaryPostFormBloc = BlocProvider.of<DiaryPostFormBloc>(context);
    _nameTextController.addListener(_onNameChange);
    _descriptionTextController.addListener(_onDescriptionChange);

    _dropdownMenuItems = buildDropdownMenuItems(_companies);
    _selectedCompany = _dropdownMenuItems[0].value;
    _isButtonDisabled = true;
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<DiaryPostFormBloc, DiaryPostFormState>(
      listener: (context, state) async {
        if (state.isSubmitting) {
          print('---isSubmitting---');
          print(state.status);
          await HttpHandler.resolve(status: state.status);
        }

        if (state.isSuccess) {
          print('---isSuccess---');
          print(state.status);
          await HttpHandler.resolve(status: state.status);
        }

        if (state.isFailure) {
          print('---isFailure---');
          print(state.status);
          await HttpHandler.resolve(status: state.status);
        }
      },
      child: BlocBuilder<DiaryPostFormBloc, DiaryPostFormState>(
          builder: (context, state) {
        return Form(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 89,
                  // color: Colors.black,
                  child: Stack(
                    children: [
                      Positioned.fill(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(left: 20, top: 20),
                              child: GestureDetector(
                                onTap: () {
                                  Navigator.pop(context);
                                },
                                child: Image.asset(
                                  "assets/images/back_all.png",
                                  height: 25,
                                  width: 25,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Positioned.fill(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(right: 20, top: 20),
                              child: _buildSubmitButton(),
                            )
                          ],
                        ),
                      ),
                      Positioned.fill(
                          child: FractionallySizedBox(
                        widthFactor: .5,
                        child: Padding(
                          padding: EdgeInsets.only(top: 20),
                          child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                "Tạo bài viết",
                                style: TextStyle(
                                    color: Colors.black, fontSize: 16),
                              )),
                        ),
                      ))
                    ],
                  ),
                ),
                Divider(
                  height: 1,
                  thickness: 1,
                  color: Colors.black12,
                ),
                SizedBox(
                  height: 10,
                ),
                _buildContent(context, state),
                _buildPosition(context, state),
                Container(
                  padding: EdgeInsets.only(left: 20, right: 20, top: 20),
                  child: WidgetFormInput(
                    controller: _nameTextController,
                    text: '',
                    hint: "Tiêu đề",
                    // validator: _productPostFormBloc.state.name.isNotEmpty?? '',
                  ),
                ),
                Divider(
                  height: 1,
                  thickness: 1,
                  color: Colors.black12,
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  child: WidgetFormInput(
                    controller: _descriptionTextController,
                    text: '',
                    hint: "Bạn đang nghĩ gì?",
                    // validator: _productPostFormBloc.state.name.isNotEmpty?? '',
                  ),
                ),
                croppedFile != null
                    ? Container(
                        width: MediaQuery.of(context).size.width,
                        height: 300,
                        child: Image.file(
                          croppedFile,
                          fit: BoxFit.cover,
                        ),
                      )
                    : Container(),
                SizedBox(
                  height: 20,
                ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      //camere
                      MaterialButton(
                        onPressed: () async {
                          _getImageFromCamera();
                          //_getCurrentLocation();
                        },
                        color: Colors.blue,
                        textColor: Colors.white,
                        child: Icon(Icons.camera_alt),
                        padding: EdgeInsets.all(10),
                        shape: CircleBorder(),
                      ),
                      Tooltip(
                        message: "Đính kèm",
                        child: MaterialButton(
                          onPressed: () {
                            _getImageFromGallery();
                          },
                          color: Colors.blue,
                          textColor: Colors.white,
                          child: Image.asset(
                            "assets/images/paperclip.png",
                            scale: 1.5,
                          ),
                          padding: EdgeInsets.all(10),
                          shape: CircleBorder(),
                        ),
                      ),
                      MaterialButton(
                        onPressed: () {
                          _getCurrentLocation();
                        },
                        color: Colors.blue,
                        textColor: Colors.white,
                        child: Image.asset(
                          "assets/images/local.png",
                          scale: 1.5,
                        ),
                        padding: EdgeInsets.all(10),
                        shape: CircleBorder(),
                      ),
                      Container(
                          child: new DropdownButtonHideUnderline(
                            child: DropdownButton(
                              value: _selectedCompany,
                              items: _dropdownMenuItems,
                              onChanged: onChangeDropdownItem,
                            ),
                          )),
                      // MaterialButton(
                      //   color: Colors.blue,
                      //   textColor: Colors.white,
                      //   child: Image.asset(
                      //     "assets/images/speed.png",
                      //     scale: 1.5,
                      //   ),
                      //   padding: EdgeInsets.all(10),
                      //   shape: CircleBorder(),
                      //   onPressed: () {
                      //     _askFavColor();
                      //   },
                      // ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      }),
    );
  }

  _buildSubmitButton() {
    if (_diaryPostFormBloc.state.images != null) {
      return WidgetButtonPost(
        onTap: () {
          _isButtonDisabled = false;
          if (_diaryPostFormBloc.state.checkReadyToOrder()) {
            _diaryPostFormBloc.add(DiaryPostFormSubmitted());
          }
        },
        isEnable: _isButtonDisabled,
        text: "Đăng",
      );
    } else {
      return WidgetButtonPost(
        onTap: () {
          _isButtonDisabled = false;
          if (_diaryPostFormBloc.state.checkReadyToOrder()) {
            _diaryPostFormBloc.add(DiaryPostSubmitted());
          }
        },
        isEnable: _isButtonDisabled,
        text: "Đăng",
      );
    }
  }

  void _onNameChange() {
    _diaryPostFormBloc.add(NameProductChanged(name: _nameTextController.text));
  }

  void _onDescriptionChange() {
    _diaryPostFormBloc.add(DesProductChanged(
      description: _descriptionTextController.text,
    ));
  }

  Future<Null> _getImageFromGallery() async {
    croppedFile = null;
    final pickedFile = await _picker.getImage(source: ImageSource.gallery,imageQuality: 25);
    setState(() {
      croppedFile = File(pickedFile.path);
    });
    if (croppedFile != null) {
      BlocProvider.of<DiaryPostFormBloc>(context)
          .add(ImageLoaded(image: croppedFile));
    }
  }

  Future<Null> _getImageFromCamera() async {
    croppedFile = await showDialog(
        context: context,
        builder: (context) => Camera(
          mode: CameraMode.fullscreen,
          onChangeCamera: (direction, _) {
            print('--------------');
            print('$direction');
            print('--------------');
          },

          // imageMask: CameraFocus.square(
          //   color: Colors.black.withOpacity(0.5),
          // ),
        ));
    setState(() {
      //croppedFile = File(pickedFile.path);
    });
    if (croppedFile != null) {
      BlocProvider.of<DiaryPostFormBloc>(context)
          .add(ImageLoaded(image: croppedFile));
    }
  }

  @override
  void dispose() {
    _nameTextController.dispose();
    _descriptionTextController.dispose();
    super.dispose();
  }

  Widget _buildContent(BuildContext context, DiaryPostFormState state) {
    if (state.typeStatus == null) {
      return Container();
    } else {
      return Row(
        children: [
          SizedBox(
            width: 20,
          ),
          Text("Đang "),
          Image.asset(
            _selectedCompany.icon,
            height: 15,
            width: 15,
          ),
          Text(
            " cảm thấy ",
          ),
          Text(
            _selectedCompany.name,
            style: TextStyle(
                color: Colors.blue, fontSize: 16, fontWeight: FontWeight.bold),
          ),
        ],
      );
    }
  }

  Widget _buildPosition(BuildContext context, DiaryPostFormState state) {
    if (state.lat == null &&
        state.lat == 0 &&
        state.long == null &&
        state.long == 0) {
      return Container();
    } else {
      return   _currentPosition != null &&
          _currentAddress != "" &&
          _currentAddress != null? Row(
        children: <Widget>[
          SizedBox(
            width: 20,
            height: 20,
          ),
          Icon(
            Icons.location_on,
            color: Colors.blue,
          ),
          SizedBox(
            width: 8,
          ),
          Expanded(
            child: Container(
              child: _currentPosition != null &&
                      _currentAddress != "" &&
                      _currentAddress != null
                  ? Text(_currentAddress ?? '',
                      style: Theme.of(context).textTheme.bodyText2)
                  : Container(),
            ),
          ),
          SizedBox(
            width: 8,
          ),
        ],
      ):Container();
    }
  }

  Future<Null> _askFavColor() async {
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return MultiBlocProvider(
            providers: [
              BlocProvider(
                create: (context) =>
                    DiaryPostFormBloc(userRepository: userRepository),
              ),
            ],
            child: AlertDialog(
              title: Text('Hoạt động'),
              content: Container(
                width: double.minPositive,
                child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: runItem.length,
                  itemBuilder: (BuildContext context, int index) {
                    return ListTile(
                      title: Text(runItem[index].name),
                      onTap: () {
                        Navigator.pop(context);
                        BlocProvider.of<DiaryPostFormBloc>(context)
                            .add(StatusChanged(runItem[index].id));
                      },
                    );
                  },
                ),
              ),
            ),
          );
        });
  }
}

class IconsItem {
  final String icon, name;
  final int id;
  IconsItem({this.id, this.icon, this.name});

  static List<IconsItem> getIcon() {
    return <IconsItem>[
      IconsItem(
        id: 1,
        name: "Hạnh phúc",
        icon: "assets/images/happy.png",
      ),
      IconsItem(
        id: 2,
        name: "Bực bội",
        icon: "assets/images/angry.png",
      ),
      IconsItem(
        id: 3,
        name: "Lạnh lẽo",
        icon: "assets/images/cold.png",
      ),
      IconsItem(
        id: 4,
        name: "Đang yêu",
        icon: "assets/images/lovely.png",
      ),
      IconsItem(
        id: 5,
        name: "tuyệt vời",
        icon: "assets/images/smile.png",
      ),
      IconsItem(
        id: 6,
        name: "ngon lành",
        icon: "assets/images/yummy.png",
      ),
    ];
  }
}
