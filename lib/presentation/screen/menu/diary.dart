import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:memory_lifes/app/constants/color/color.dart';
import 'package:memory_lifes/app/constants/navigator/navigator.dart';
import 'package:memory_lifes/presentation/screen/menu/account/bloc/profile_bloc.dart';
import 'package:memory_lifes/presentation/screen/menu/account/bloc/profile_event.dart';
import 'package:memory_lifes/presentation/screen/menu/diary/bloc/bloc.dart';
import 'package:memory_lifes/presentation/screen/menu/diary/widget_avatar.dart';
import 'package:memory_lifes/presentation/screen/menu/diary/widget_posts.dart';

class Diary extends StatefulWidget {
  @override
  DiaryState createState() => DiaryState();
}

class DiaryState extends State<Diary> {
  bool fab = true;
  @override
  void initState() {
    super.initState();
    BlocProvider.of<PostBloc>(context).add(LoadPost());
    BlocProvider.of<ProfileBloc>(context).add(LoadProfile());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        top: false,
        child: Column(
          children: <Widget>[
            Container(
              height: 170,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/background_home.png'),
                  fit: BoxFit.fill,
                ),
              ),
              child: Stack(
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width / 1.2,
                    margin: EdgeInsets.only(top: 100, left: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        WidgetAvatar(),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                            flex: 8,
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              textColor: Colors.black,
                              color: Colors.white,
                              onPressed: () {
                                AppNavigator.navigateCreateDiary();
                              },
                              child: Text(
                                'Hi! Bạn đang nghĩ gì?',
                                style: TextStyle(
                                    color: Colors.grey[600],
                                    fontWeight: FontWeight.w400),
                              ),
                            ))
                      ],
                    ),
                  )
                ],
              ),
            ),
            Expanded(
              child: RefreshIndicator(
                  onRefresh: () async {
                    BlocProvider.of<PostBloc>(context).add(RefreshPost());
                    await Future.delayed(Duration(seconds: 2));
                    return true;
                  },
                  color: AppColor.PRIMARY_COLOR,
                  backgroundColor: AppColor.THIRD_COLOR,
                  child: _buildContent()),
            ),
          ],
        ),
      ),
      floatingActionButton: fab
          ? FloatingActionButton(
              onPressed: () {
                AppNavigator.navigateCreateDiary();
              },
              tooltip: 'Thêm nhật kí',
              child: Icon(Icons.add),
            )
          : null,
    );
  }

  Widget _buildContent() => WidgetPostListItem();
}
