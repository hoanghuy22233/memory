import 'package:flutter/material.dart';
import 'package:memory_lifes/app/constants/navigator/navigator.dart';
import 'package:memory_lifes/model/entity/new.dart';
import 'package:shimmer/shimmer.dart';

class WidgetPostItem extends StatefulWidget {
  final News post;
  final Color backgroundColor;

  const WidgetPostItem({Key key, this.post, this.backgroundColor})
      : super(key: key);

  @override
  _WidgetPostItemState createState() => _WidgetPostItemState();
}

class _WidgetPostItemState extends State<WidgetPostItem> {
  bool isReadDetail = false;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        AppNavigator.navigateNewsDetail(widget.post.id);
      },
      child: Stack(
        alignment: Alignment.topRight,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Image.asset('assets/images/cricle.png'),
                  SizedBox(
                    width: 10,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width / 2,
                        child: Text(
                          widget.post?.title ?? '',
                          style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                              color: Colors.blue),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      widget.post?.typeStatus != null &&
                              widget.post.typeStatus > 0
                          ? Stack(
                              children: [
                                if (widget.post?.typeStatus == 1)
                                  Row(
                                    children: [
                                      Text("-" + " Đang "),
                                      Image.asset(
                                        "assets/images/happy.png",
                                        height: 15,
                                        width: 15,
                                      ),
                                      Text(
                                        " cảm thấy ",
                                      ),
                                      Text(
                                        "hạnh phúc ",
                                        style: TextStyle(
                                            color: Colors.blue,
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                if (widget.post?.typeStatus == 2)
                                  Row(
                                    children: [
                                      Text("-" + " Đang "),
                                      Image.asset(
                                        "assets/images/angry.png",
                                        height: 15,
                                        width: 15,
                                      ),
                                      Text(
                                        " cảm thấy ",
                                      ),
                                      Text(
                                        "bực bội ",
                                        style: TextStyle(
                                            color: Colors.blue,
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                if (widget.post?.typeStatus == 3)
                                  Row(
                                    children: [
                                      Text("-" + " Đang "),
                                      Image.asset(
                                        "assets/images/cold.png",
                                        height: 15,
                                        width: 15,
                                      ),
                                      Text(
                                        " cảm thấy ",
                                      ),
                                      Text(
                                        "lạnh lẽo",
                                        style: TextStyle(
                                            color: Colors.blue,
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                if (widget.post?.typeStatus == 4)
                                  Row(
                                    children: [
                                      Text("-" + " Đang "),
                                      Image.asset(
                                        "assets/images/lovely.png",
                                        height: 15,
                                        width: 15,
                                      ),
                                      Text(
                                        " cảm thấy ",
                                      ),
                                      Text(
                                        "đang yêu ",
                                        style: TextStyle(
                                            color: Colors.blue,
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                if (widget.post?.typeStatus == 5)
                                  Row(
                                    children: [
                                      Text("-" + " Đang "),
                                      Image.asset(
                                        "assets/images/smile.png",
                                        height: 15,
                                        width: 15,
                                      ),
                                      Text(
                                        " cảm thấy ",
                                      ),
                                      Text(
                                        "tuyệt vời ",
                                        style: TextStyle(
                                            color: Colors.blue,
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                if (widget.post?.typeStatus == 6)
                                  Row(
                                    children: [
                                      Text("-" + " Đang "),
                                      Image.asset(
                                        "assets/images/yummy.png",
                                        height: 15,
                                        width: 15,
                                      ),
                                      Text(
                                        " cảm thấy ",
                                      ),
                                      Text(
                                        "ngon lành ",
                                        style: TextStyle(
                                            color: Colors.blue,
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                              ],
                            )
                          : Container(),
                      widget.post?.coTypeWork != null &&
                              widget.post.typeStatus == null &&
                              widget.post.coTypeWork > 0 &&
                              widget.post.typeStatus > 0
                          ? Stack(
                              children: [
                                if (widget.post?.coTypeWork == 1)
                                  Row(
                                    children: [
                                      Text("-" + " Đang "),
                                      Text(
                                        "Đi chơi ",
                                        style: TextStyle(
                                            color: Colors.blue,
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                if (widget.post?.coTypeWork == 2)
                                  Row(
                                    children: [
                                      Text("-" + " Đang "),
                                      Text(
                                        "nghe nhạc ",
                                        style: TextStyle(
                                            color: Colors.blue,
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                if (widget.post?.coTypeWork == 3)
                                  Row(
                                    children: [
                                      Text("-" + " Đang "),
                                      Text(
                                        "xem phim ",
                                        style: TextStyle(
                                            color: Colors.blue,
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                              ],
                            )
                          : Container(),
                      Text(
                        widget.post?.date ?? '',
                        style: TextStyle(
                          fontSize: 10,
                        ),
                        maxLines: 3,
                      )
                    ],
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: 20,
                    ),
                    child: Container(
                      height: isReadDetail ? null : 50,
                      child: Text(
                        widget.post?.content ?? '',
                      ),
                    ),
                  ),
                  widget.post.content.length > 100
                      ? Column(
                          children: [
                            SizedBox(
                              height: 10,
                            ),
                            Center(
                              child: GestureDetector(
                                onTap: () {
                                  setState(() {
                                    isReadDetail = !isReadDetail;
                                  });
                                },
                                child: isReadDetail
                                    ? Text(
                                        "Thu gọn",
                                        style: TextStyle(color: Colors.blue),
                                      )
                                    : Text(
                                        "Xem thêm",
                                        style: TextStyle(color: Colors.blue),
                                      ),
                              ),
                            ),
                          ],
                        )
                      : SizedBox(),
                  widget.post?.image != null && widget.post?.image != ""
                      ? Align(
                          alignment: Alignment.centerRight,
                          child: _buildProductImage(),
                        )
                      : Container(),
                ],
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 20),
                child: Divider(
                  color: Colors.grey,
                  height: 10,
                ),
              )
            ],
          ),
          IconButton(
            icon: Icon(
              Icons.edit,
              size: 15,
              color: Colors.blue,
            ),
            onPressed: () {
              AppNavigator.navigateDiaryUpdate(widget.post);
            },
          ),
        ],
      ),
    );
  }

  Widget _buildProductImage() {
    return Container(
      height: 200,
      width: double.infinity,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(8),
        child: Image.network(
          widget.post?.image ?? '',
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
