import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_circle_progress.dart';
import 'package:memory_lifes/presentation/screen/menu/account/bloc/bloc.dart';

class WidgetAvatar extends StatefulWidget {
  @override
  _WidgetAvatarState createState() => _WidgetAvatarState();
}

class _WidgetAvatarState extends State<WidgetAvatar> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfileBloc, ProfileState>(
      builder: (context, state) {
        return Container(
          child: _buildContent(state),
        );
      },
    );
  }

  _buildContent(ProfileState state) {
    if (state is ProfileLoaded) {
      return Container(
        child: _buildAvatar(state),
      );
    } else if (state is ProfileLoading) {
      return Center(
        child: WidgetCircleProgress(),
      );
    } else if (state is ProfileNotLoaded) {
      return Center(
        child: Text('${state.error}'),
      );
    } else {
      return Center(
        child: Text('Unknown state'),
      );
    }
  }

  Widget _buildAvatar(ProfileLoaded state) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(500),
      child: Container(
        height: 45,
        width: 45,
        child: Image.network(
          state?.user?.avatar ?? '',
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
