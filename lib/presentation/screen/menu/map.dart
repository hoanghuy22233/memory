import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:memory_lifes/app/constants/navigator/navigator.dart';
import 'package:memory_lifes/model/entity/new.dart';
import 'package:memory_lifes/presentation/screen/menu/diary/bloc/post_bloc.dart';
import 'package:memory_lifes/presentation/screen/menu/diary/bloc/post_state.dart';
import 'package:memory_lifes/utils/handler/http_handler.dart';


class MapImage extends StatefulWidget {
  @override
  _MapImageState createState() => _MapImageState();
}

class _MapImageState extends State<MapImage> {
  GoogleMapController mapController;
  static LatLng _center = const LatLng(20.836648, 106.694363);
  final Set<Marker> _markers = {};
  LatLng _currentMapPosition = _center;


  void _onCameraMove(CameraPosition position) {
    _currentMapPosition = position.target;
  }


  List<News> places;

  Completer<GoogleMapController> _controller = Completer();

  @override
  void initState() {
    super.initState();
  }

  @override

  Widget build(BuildContext context) {
    return BlocListener<PostBloc, PostState>(
      listener: (context, state) async {
        if (state.isLoading) {
          await HttpHandler.resolve(status: state.status);
        }

        if (state.isSuccess) {
          await HttpHandler.resolve(status: state.status);
          places =state.post;
          print(places);

        }

        if (state.isFailure) {
          await HttpHandler.resolve(status: state.status);
        }
      },
      child: BlocBuilder<PostBloc, PostState>(
        builder: (context, state) {
          return MaterialApp(
            debugShowCheckedModeBanner: false,
            home: Scaffold(
              body: Stack(
                children: <Widget>[
                  GoogleMap(
                      initialCameraPosition: CameraPosition(
                        target: _center,
                        zoom: 10.0,
                      ),
                      markers: _markers,
                      onCameraMove: _onCameraMove,
                      onMapCreated: (GoogleMapController controller) {
                        _controller.complete(controller);
                        setState(() {
                          for(int i=0; i < state.post.length; i++){
                            if(state.post[i].lat != null && state.post[i].long != null && state.post[i].lat != 0 && state.post[i].long != 0){
                              final marker = Marker(
                                markerId: MarkerId(state.post[i].id.toString()),
                                position: LatLng(state.post[i].lat, state.post[i].long),
                                infoWindow: InfoWindow(
                                  title: state.post[i].title,
                                  onTap: () => AppNavigator.navigateNewsDetail(state.post[i].id),
                                ),
                                icon: BitmapDescriptor.defaultMarker,
                              );
                              _markers.add(marker);
                            }
                          }
                        });
                      }
                  ),

                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
