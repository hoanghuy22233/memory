import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logger/logger.dart';
import 'package:memory_lifes/app/auth_bloc/authentication_bloc.dart';
import 'package:memory_lifes/app/auth_bloc/authentication_event.dart';
import 'package:memory_lifes/app/constants/barrel_constants.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_cached_image.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_circle_progress.dart';
import 'package:memory_lifes/presentation/router.dart';
import 'package:memory_lifes/presentation/screen/map_user/update_locations/bloc/update_location_bloc.dart';
import 'package:memory_lifes/presentation/screen/map_user/update_locations/bloc/update_location_event.dart';
import 'package:memory_lifes/presentation/screen/menu/account/bloc/profile_bloc.dart';
import 'package:memory_lifes/presentation/screen/menu/account/bloc/profile_event.dart';
import 'package:memory_lifes/presentation/screen/menu/account/bloc/profile_state.dart';
import 'package:memory_lifes/presentation/screen/view_image.dart';
import 'package:memory_lifes/utils/dialog/confirm_dialog.dart';

class Account extends StatefulWidget {
  GlobalKey<ScaffoldState> drawer;

  Account({this.drawer});
  @override
  _AccountState createState() => _AccountState();
}

class _AccountState extends State<Account>
    with AutomaticKeepAliveClientMixin<Account> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<ProfileBloc>(context).add(LoadProfile());
  }

  var logger = Logger(
    printer: PrettyPrinter(),
  );

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(body: BlocBuilder<ProfileBloc, ProfileState>(
      builder: (context, state) {
        if (state is ProfileLoaded) {
          return Column(
            children: [
              Container(
                  height: 200,
                  width: MediaQuery.of(context).size.width,
                  child: Stack(
                    alignment: Alignment.center,
                    overflow: Overflow.visible,
                    children: [
                      Positioned(
                          top: 0,
                          left: 0,
                          right: 0,
                          child: GestureDetector(
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (BuildContext context) => ViewImage(
                                        image: state.user.backgroundImage,
                                      )));
                            },
                            child: Image.network(
                              state.user.backgroundImage,
                              height: 200,
                              fit: BoxFit.cover,
                            ),
                          )),
                      Positioned(
                          top: 150,
                          child: GestureDetector(
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (BuildContext context) => ViewImage(
                                        image: state.user.avatar,
                                      )));
                            },
                            child: Container(
                              height: 100,
                              width: 100,
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(500),
                                child: WidgetCachedImage(
                                  url: state?.user?.avatar ?? '',
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          )),
                    ],
                  )),
              SizedBox(
                height: 60,
              ),
              Text(
                state?.user?.name ?? 'Chưa thiết lập',
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 15,
              ),
              Expanded(
                child: _buildMenu(state),
              ),
            ],
          );
        } else if (state is ProfileLoading) {
          return Center(
            child: WidgetCircleProgress(),
          );
        } else if (state is ProfileNotLoaded) {
          return Center(
            child: Text('${state.error}'),
          );
        } else {
          return Center(
            child: Text('Unknown state'),
          );
        }
      },
    ));
  }

  Widget _buildMenu(ProfileLoaded state) {
    return RefreshIndicator(
      onRefresh: () async {
        BlocProvider.of<ProfileBloc>(context).add(LoadProfile());
        await Future.delayed(Duration(seconds: 1));
        return true;
      },
      color: AppColor.PRIMARY_COLOR,
      backgroundColor: AppColor.THIRD_COLOR,
      child: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          children: [
            _createDrawerItem(
                icon: AssetImage("assets/images/user.png"),
                text: 'Hồ sơ cá nhân',
                onTap: () =>
                    Navigator.pushNamed(context, BaseRouter.PROFILE_USER)),
            _createDrawerItem(
                icon: AssetImage("assets/images/change_pin.png"),
                text: 'Thay đổi mật khẩu',
                onTap: () {
                  AppNavigator.navigateChangePassword();
                }),
            _createDrawerItem(
                icon: AssetImage("assets/images/local.png"),
                text: 'Vị trí người dùng',
                onTap: () {
                  AppNavigator.navigateUserLocation();
                  BlocProvider.of<UpdateLocationBloc>(context).add(
                      UpdateLocation(
                          lat: (state.user.lat + 1).toString(),
                          long: (state.user.long + 1).toString()));
                }),
            MaterialButton(
              minWidth: 80.0,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(25.0),
              ),
              textColor: Colors.white,
              color: Colors.red,
              onPressed: () async {
                var result = await ConfirmDialog.show(
                    context, 'Bạn muốn đăng xuất tài khoản?',
                    ok: 'OK', cancel: 'Huỷ');
                if (result == true) {
                  BlocProvider.of<AuthenticationBloc>(context).add(LoggedOut());
                  //AppNavigator.navigateLogin();
                  Navigator.popUntil(
                      context, (r) => r.settings.name == BaseRouter.LOGIN);
                }
              },
              child: Text(
                'Đăng xuất',
                style: TextStyle(
                  fontSize: 16,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _buildContent(ProfileState state) {
    if (state is ProfileLoaded) {
      return Container(
        child: _buildMenu(state),
      );
    } else if (state is ProfileLoading) {
      return Center(
        child: WidgetCircleProgress(),
      );
    } else if (state is ProfileNotLoaded) {
      return Center(
        child: Text('${state.error}'),
      );
    } else {
      return Center(
        child: Text('Unknown state'),
      );
    }
  }

  Widget _createDrawerItem(
      {AssetImage icon, String text, GestureTapCallback onTap}) {
    return ListTile(
      title: Row(
        children: <Widget>[
          ImageIcon(
            icon,
            color: Colors.blue,
          ),
          Padding(
            padding: EdgeInsets.only(left: 10.0),
            child: Text(
              text,
              style: TextStyle(fontSize: 16, color: Colors.black),
            ),
          )
        ],
      ),
      onTap: onTap,
      dense: true,
    );
  }
}
