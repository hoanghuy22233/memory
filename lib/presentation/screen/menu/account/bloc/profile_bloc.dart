import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:memory_lifes/model/repo/barrel_repo.dart';
import 'package:memory_lifes/presentation/screen/menu/account/bloc/profile_event.dart';
import 'package:memory_lifes/presentation/screen/menu/account/bloc/profile_state.dart';
import 'package:memory_lifes/utils/dio/dio_error_util.dart';
import 'package:meta/meta.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  final UserRepository _userRepository;

  ProfileBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  ProfileState get initialState => ProfileLoading();

  @override
  Stream<ProfileState> mapEventToState(ProfileEvent event) async* {
    if (event is LoadProfile) {
      yield* _mapLoadProfileToState();
    } else if (event is RefreshProfile) {
      yield ProfileLoading();
      yield* _mapLoadProfileToState();
    }
  }

  Stream<ProfileState> _mapLoadProfileToState() async* {
    try {
      final profileResponse = await _userRepository.getProfile();
      print('-----------------');
      yield ProfileLoaded(profileResponse.data);
    } catch (e) {
      yield ProfileNotLoaded(DioErrorUtil.handleError(e));
//      yield ProfileNotLoaded(e);
    }
  }
}
