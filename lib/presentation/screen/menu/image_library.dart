import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:memory_lifes/app/constants/color/color.dart';
import 'package:memory_lifes/app/constants/navigator/navigator.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_circle_progress.dart';
import 'package:memory_lifes/presentation/screen/menu/imageStock/bloc/bloc.dart';
import 'package:memory_lifes/presentation/screen/menu/imageStock/bloc/images_bloc.dart';
import 'package:memory_lifes/presentation/screen/menu/imageStock/bloc/images_state.dart';
import 'package:memory_lifes/utils/handler/http_handler.dart';

class ImageLibrary extends StatefulWidget {
  @override
  GlobalKey<ScaffoldState> drawer;

  ImageLibrary({this.drawer});

  _ImageLibraryState createState() => _ImageLibraryState();
}

class _ImageLibraryState extends State<ImageLibrary> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<ImagesBloc>(context).add(LoadImages());
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ImagesBloc, ImagesState>(
      listener: (context, state) async {
        if (state.isLoading) {
          await HttpHandler.resolve(status: state.status);
        }

        if (state.isSuccess) {
          await HttpHandler.resolve(status: state.status);
        }

        if (state.isFailure) {
          await HttpHandler.resolve(status: state.status);
        }
      },
      child: BlocBuilder<ImagesBloc, ImagesState>(
        builder: (context, state) {
          return Container(
            child: RefreshIndicator(
                onRefresh: () async {
                  BlocProvider.of<ImagesBloc>(context).add(RefreshImages());
                  await Future.delayed(Duration(seconds: 2));
                  return true;
                },
                color: AppColor.PRIMARY_COLOR,
                backgroundColor: AppColor.THIRD_COLOR,
                child: _buildContent(state)),
          );
        },
      ),
    );
  }


  Widget _buildContent(ImagesState state) {
    if (state.post != null&&state?.post?.length!=0) {
      return GridView.builder(
        shrinkWrap: true,
        padding: EdgeInsets.only(top: 10, left: 5, right: 5),
        itemBuilder: (context, index) {
          return GestureDetector(
              onTap: () {
                AppNavigator.navigateNewsDetail(state.post[index].id);
              },
              child: Container(
                margin: EdgeInsets.all(5),
                child: Image.network(
                  state.post[index].image,
                  fit: BoxFit.cover,
                ),
              ));
        },
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
        ),
        physics: NeverScrollableScrollPhysics(),
        itemCount: state.post.length,
      );
    } else {
      return Center(child: Text("Bạn chưa có ảnh!"));
    }
  }
}
