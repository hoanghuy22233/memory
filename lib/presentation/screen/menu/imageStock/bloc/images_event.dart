import 'package:equatable/equatable.dart';

class ImagesEvent extends Equatable {
  const ImagesEvent();

  List<Object> get props => [];
}

class LoadImages extends ImagesEvent {}

class RefreshImages extends ImagesEvent {}

class RemoveImages extends ImagesEvent {
  final int id;

  RemoveImages(this.id);

  List<Object> get props => [id];

  @override
  String toString() {
    return 'RemovePost{id: $id}';
  }
}
