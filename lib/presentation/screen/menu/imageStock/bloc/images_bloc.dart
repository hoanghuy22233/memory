import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:memory_lifes/model/repo/user_repository.dart';
import 'package:memory_lifes/presentation/screen/menu/imageStock/bloc/images_event.dart';
import 'package:memory_lifes/presentation/screen/menu/imageStock/bloc/images_state.dart';
import 'package:memory_lifes/utils/dio/dio_error_util.dart';
import 'package:memory_lifes/utils/dio/dio_status.dart';
import 'package:meta/meta.dart';

class ImagesBloc extends Bloc<ImagesEvent, ImagesState> {
  final UserRepository userRepository;

  ImagesBloc({@required this.userRepository});

  @override
  ImagesState get initialState => ImagesState.empty();

  @override
  Stream<ImagesState> mapEventToState(ImagesEvent event) async* {
    if (event is LoadImages) {
      yield* _mapLoadImagesToState();
    } else if (event is RefreshImages) {
      yield ImagesState.loading(state.copyWith(
          status: DioStatus(
        code: DioStatus.API_PROGRESS,
      )));
      yield* _mapLoadImagesToState();
    }
  }

  Stream<ImagesState> _mapLoadImagesToState() async* {
    try {
      final response = await userRepository.getImages();
      yield ImagesState.success(state.update(
          post: response.data,
          status: DioStatus(
              code: DioStatus.API_SUCCESS, message: response.message)));
    } catch (e) {
      yield ImagesState.failure(
          state.update(status: DioErrorUtil.handleError(e)));
    }
  }

  // Stream<ImagesState> _mapRemoveImagesToState(int id) async* {
  //   try {
  //     final response = await userRepository.removePost(id: id);
  //     yield ImagesState.success(state.update(
  //         post: response.data,
  //         status: DioStatus(
  //             code: DioStatus.API_SUCCESS, message: response.message)));
  //   } catch (e) {
  //     yield ImagesState.failure(
  //         state.update(status: DioErrorUtil.handleError(e)));
  //   }
  // }
}
