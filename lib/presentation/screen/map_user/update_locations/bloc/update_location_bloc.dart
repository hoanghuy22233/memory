import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:memory_lifes/app/constants/endpoint/app_endpoint.dart';
import 'package:memory_lifes/model/repo/user_repository.dart';
import 'package:memory_lifes/presentation/screen/map_user/update_locations/bloc/update_location_event.dart';
import 'package:memory_lifes/presentation/screen/map_user/update_locations/bloc/update_location_state.dart';
import 'package:memory_lifes/utils/validator/validator.dart';
import 'package:meta/meta.dart';
import 'package:quiver/async.dart';

class UpdateLocationBloc
    extends Bloc<UpdateLocationEvent, UpdateLocationState> {
  final UserRepository _userRepository;

  StreamSubscription _streamSubscription;

  UpdateLocationBloc({@required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository;

  @override
  UpdateLocationState get initialState => UpdateLocationState.empty();

  @override
  Stream<UpdateLocationState> mapEventToState(
      UpdateLocationEvent event) async* {
    if (event is TimeInit) {
      yield* _mapTimeInitToState(event.time);
    } else if (event is TimeChanged) {
      yield* _mapTimeChangeToState(event.time);
    } else if (event is UpdateLocation) {
      yield* _mapUpdateLocationToState(event.lat, event.long);
    }
  }

  Stream<UpdateLocationState> _mapTimeInitToState(Duration time) async* {
    CountdownTimer _countDownTimer;
    _countDownTimer = new CountdownTimer(time, Duration(seconds: 1));
    _streamSubscription = _countDownTimer.listen(null);
    _streamSubscription.onData((duration) {
      print('time: ${duration.remaining.inSeconds}');
      this.add(TimeChanged(time: duration.remaining.inSeconds));
    });

    _streamSubscription.onDone(() async* {
      _streamSubscription.cancel();
    });
  }

  Stream<UpdateLocationState> _mapTimeChangeToState(int time) async* {
    yield state.update(
        time: time, isTimeValid: Validator.isValidResendOtp(time));
  }

  Stream<UpdateLocationState> _mapUpdateLocationToState(
      String lat, String long) async* {
    yield UpdateLocationState.loading();
    try {
      var response = await _userRepository.postLocation(lat: lat, long: long);

      if (response.status == Endpoint.SUCCESS) {
        yield UpdateLocationState.success(message: response.message);
      } else {
        yield UpdateLocationState.failure(message: response.message);
      }
    } catch (e) {
      print(e.toString());
      yield UpdateLocationState.failure(message: e.toString());
    }
  }

  @override
  Future<void> close() {
    _streamSubscription.cancel();
    return super.close();
  }
}
