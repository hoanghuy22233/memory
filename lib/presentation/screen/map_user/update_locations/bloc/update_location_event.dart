import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class UpdateLocationEvent extends Equatable {
  const UpdateLocationEvent();

  @override
  List<Object> get props => [];
}

class TimeInit extends UpdateLocationEvent {
  final Duration time;

  const TimeInit({@required this.time});

  @override
  List<Object> get props => [time];

  @override
  String toString() {
    return 'TimeInit{time: $time}';
  }
}

class TimeChanged extends UpdateLocationEvent {
  final int time;

  const TimeChanged({@required this.time});

  @override
  List<Object> get props => [time];

  @override
  String toString() {
    return 'TimeChanged{time: $time}';
  }
}

class UpdateLocation extends UpdateLocationEvent {
  final String lat;
  final String long;

  const UpdateLocation({@required this.lat, @required this.long});

  @override
  List<Object> get props => [lat, long];

  @override
  String toString() {
    return 'UpdateLocation{lat: $lat,long: $long,}';
  }
}
