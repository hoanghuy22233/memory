import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:memory_lifes/app/constants/value/value.dart';
import 'package:memory_lifes/utils/snackbar/get_snack_bar_utils.dart';

import 'bloc/update_location_bloc.dart';
import 'bloc/update_location_event.dart';
import 'bloc/update_location_state.dart';

class WidgetUpdateLocation extends StatefulWidget {
  final String lat;
  final String long;

  const WidgetUpdateLocation({Key key, this.lat, this.long}) : super(key: key);

  @override
  _WidgetUpdateLocationState createState() => _WidgetUpdateLocationState();
}

class _WidgetUpdateLocationState extends State<WidgetUpdateLocation> {
  UpdateLocationBloc _updateLocationBloc;

  @override
  void initState() {
    super.initState();
    _updateLocationBloc = BlocProvider.of<UpdateLocationBloc>(context)
      ..add(TimeInit(time: AppValue.VERIFY_RESEND_TIME));
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<UpdateLocationBloc, UpdateLocationState>(
      listener: (context, state) async {
        if (state.isSubmitting) {
          GetSnackBarUtils.createProgress();
        }

        if (state.isSuccess) {
          await GetSnackBarUtils.createSuccess(message: state.message);
          BlocProvider.of<UpdateLocationBloc>(context)
              .add(TimeInit(time: AppValue.VERIFY_RESEND_TIME));
        }

        if (state.isFailure) {
          GetSnackBarUtils.createError(message: state.message);
        }
      },
      child: BlocBuilder<UpdateLocationBloc, UpdateLocationState>(
        builder: (context, state) {
          return Padding(
            padding: EdgeInsets.all(20),
            child: Form(
              child: Column(
                children: [_buildContent(state)],
              ),
            ),
          );
        },
      ),
    );
  }

  bool isResendEnabled() {
    return _updateLocationBloc.state.isFormValid &&
        !_updateLocationBloc.state.isSubmitting;
  }

  Widget _buildContent(UpdateLocationState state) {
    return GestureDetector(
      onTap: () {
        BlocProvider.of<UpdateLocationBloc>(context)
            .add(UpdateLocation(lat: widget.lat, long: widget.long));
      },
      child: Text("test"),
    );
  }
}
