import 'package:memory_lifes/model/entity/images.dart';
import 'package:memory_lifes/model/entity/position.dart';
import 'package:memory_lifes/utils/dio/dio_status.dart';
import 'package:meta/meta.dart';

class PositionGetState {
  final List<Positions> position;
  final bool isLoading;
  final bool isSuccess;
  final bool isFailure;
  final DioStatus status;

  PositionGetState(
      {@required this.position,
      @required this.isLoading,
      @required this.isSuccess,
      @required this.isFailure,
      @required this.status});

  factory PositionGetState.empty() {
    return PositionGetState(
        position: null,
        isLoading: false,
        isSuccess: false,
        isFailure: false,
        status: null);
  }

  factory PositionGetState.loading(PositionGetState state) {
    return PositionGetState(
        position: state.position,
        isLoading: true,
        isSuccess: false,
        isFailure: false,
        status: state.status);
  }

  factory PositionGetState.failure(PositionGetState state) {
    return PositionGetState(
        position: state.position,
        isLoading: false,
        isSuccess: false,
        isFailure: true,
        status: state.status);
  }

  factory PositionGetState.success(PositionGetState state) {
    return PositionGetState(
        position: state.position,
        isLoading: false,
        isSuccess: true,
        isFailure: false,
        status: state.status);
  }

  PositionGetState update(
      {List<Positions> position,
      bool isLoading,
      bool isSuccess,
      bool isFailure,
      DioStatus status}) {
    return copyWith(
      position: position,
      isLoading: false,
      isSuccess: false,
      isFailure: false,
      status: status,
    );
  }

  PositionGetState copyWith({
    List<Positions> position,
    bool isLoading,
    bool isSuccess,
    bool isFailure,
    DioStatus status,
  }) {
    return PositionGetState(
      position: position ?? this.position,
      isLoading: isLoading ?? this.isLoading,
      isSuccess: isSuccess ?? this.isSuccess,
      isFailure: isFailure ?? this.isFailure,
      status: status ?? this.status,
    );
  }

  @override
  String toString() {
    return 'positionState{position: $position, isLoading: $isLoading, isSuccess: $isSuccess, isFailure: $isFailure, status: $status}';
  }
}
