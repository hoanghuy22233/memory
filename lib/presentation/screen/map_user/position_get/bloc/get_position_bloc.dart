import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:memory_lifes/model/repo/user_repository.dart';
import 'package:memory_lifes/presentation/screen/menu/imageStock/bloc/images_event.dart';
import 'package:memory_lifes/presentation/screen/menu/imageStock/bloc/images_state.dart';
import 'package:memory_lifes/utils/dio/dio_error_util.dart';
import 'package:memory_lifes/utils/dio/dio_status.dart';
import 'package:meta/meta.dart';

import 'get_position_event.dart';
import 'get_position_state.dart';

class PositionGetBloc extends Bloc<PositionGetEvent, PositionGetState> {
  final UserRepository userRepository;

  PositionGetBloc({@required this.userRepository});

  @override
  PositionGetState get initialState => PositionGetState.empty();

  @override
  Stream<PositionGetState> mapEventToState(PositionGetEvent event) async* {
    if (event is LoadPositionGet) {
      yield* _mapLoadPositionGetToState();
    } else if (event is RefreshPositionGet) {
      yield PositionGetState.loading(state.copyWith(
          status: DioStatus(
        code: DioStatus.API_PROGRESS,
      )));
      yield* _mapLoadPositionGetToState();
    }
  }

  Stream<PositionGetState> _mapLoadPositionGetToState() async* {
    try {
      final response = await userRepository.getPosition();
      yield PositionGetState.success(state.update(
          position: response.data,
          status: DioStatus(
              code: DioStatus.API_SUCCESS, message: response.message)));
    } catch (e) {
      yield PositionGetState.failure(
          state.update(status: DioErrorUtil.handleError(e)));
    }
  }

  // Stream<ImagesState> _mapRemoveImagesToState(int id) async* {
  //   try {
  //     final response = await userRepository.removePost(id: id);
  //     yield ImagesState.success(state.update(
  //         post: response.data,
  //         status: DioStatus(
  //             code: DioStatus.API_SUCCESS, message: response.message)));
  //   } catch (e) {
  //     yield ImagesState.failure(
  //         state.update(status: DioErrorUtil.handleError(e)));
  //   }
  // }
}
