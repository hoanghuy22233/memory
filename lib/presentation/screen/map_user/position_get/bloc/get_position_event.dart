import 'package:equatable/equatable.dart';

class PositionGetEvent extends Equatable {
  const PositionGetEvent();

  List<Object> get props => [];
}

class LoadPositionGet extends PositionGetEvent {}

class RefreshPositionGet extends PositionGetEvent {}

