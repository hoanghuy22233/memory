//
// import 'package:flutter/material.dart';
// import 'package:memory_lifes/utils/dio/dio_status.dart';
//
// class LocationState {
//   final bool isNameValid;
//   final bool isSubmitting;
//   final bool isSuccess;
//   final bool isFailure;
//   final DioStatus status;
//
//   bool get isFormValid => isNameValid;
//
//   LocationState({
//     @required this.isNameValid,
//     @required this.isSubmitting,
//     @required this.isSuccess,
//     @required this.isFailure,
//     @required this.status,
//   });
//
//   factory LocationState.empty() {
//     return LocationState(
//         isNameValid: true,
//         isSubmitting: false,
//         isSuccess: false,
//         isFailure: false,
//         status: null);
//   }
//
//   factory LocationState.loading() {
//     return LocationState(
//         isNameValid: true,
//         isSubmitting: true,
//         isSuccess: false,
//         isFailure: false,
//         status: null);
//   }
//
//   factory LocationState.failure(DioStatus status) {
//     return LocationState(
//         isNameValid: true,
//         isSuccess: false,
//         isSubmitting: false,
//         isFailure: true,
//         status: status);
//   }
//
//   factory LocationState.success(DioStatus status) {
//     return LocationState(
//         isNameValid: true,
//         isSubmitting: false,
//         isSuccess: true,
//         isFailure: false,
//         status: status);
//   }
//
//   LocationState update({bool isNameValid, DioStatus status}) {
//     return copyWith(
//         isNameValid: isNameValid,
//         isSubmitting: false,
//         isSuccess: false,
//         isFailure: false,
//         status: status);
//   }
//
//   LocationState copyWith({
//     bool isNameValid,
//     bool isPasswordValid,
//     bool isSubmitting,
//     bool isSuccess,
//     bool isFailure,
//     DioStatus status,
//   }) {
//     return LocationState(
//       isNameValid: isPasswordValid ?? this.isNameValid,
//       isSubmitting: isSubmitting ?? this.isSubmitting,
//       isSuccess: isSuccess ?? this.isSuccess,
//       isFailure: isFailure ?? this.isFailure,
//       status: status ?? this.status,
//     );
//   }
//
//   @override
//   String toString() {
//     return 'NameFormState{isNameValid: $isNameValid, isSubmitting: $isSubmitting, isSuccess: $isSuccess, isFailure: $isFailure, status: $status}';
//   }
// }
