//
// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:memory_lifes/app/constants/endpoint/app_endpoint.dart';
// import 'package:memory_lifes/model/repo/user_repository.dart';
// import 'package:memory_lifes/presentation/screen/map_user/bloc/location_event.dart';
// import 'package:memory_lifes/presentation/screen/map_user/bloc/location_state.dart';
// import 'package:memory_lifes/utils/dio/dio_error_util.dart';
// import 'package:memory_lifes/utils/dio/dio_status.dart';
// import 'package:memory_lifes/utils/validator/validator.dart';
// import 'package:rxdart/rxdart.dart';
//
// class LocationBloc extends Bloc<LocationEvent, LocationState> {
//   final UserRepository _userRepository;
//
//   LocationBloc({
//     @required UserRepository userRepository,
//   })  : assert(userRepository != null),
//         _userRepository = userRepository;
//
//   @override
//   get initialState =>LocationState.empty();
//
//   @override
//   Stream<Transition<LocationEvent, LocationState>> transformEvents(
//       Stream<LocationEvent> events, transitionFn) {
//     final nonDebounceStream = events.where((event) {
//       return (event is! LocationChanged);
//     });
//
//     final debounceStream = events.where((event) {
//       return (event is LocationChanged);
//     }).debounceTime(Duration(milliseconds: 300));
//
//     return super.transformEvents(
//         nonDebounceStream.mergeWith([debounceStream]), transitionFn);
//   }
//
//   @override
//   Stream<LocationState> mapEventToState(LocationEvent event) async* {
//     if (event is LocationSubmitEvent) {
//       yield* _mapLocationSubmitEventToState(event.lat, event.long);
//     }
//   }
//
//   Stream<LocationState> _mapLocationSubmitEventToState(double lat, double long) async* {
//     try {
//       yield LocationState.loading();
//
//       var response = await _userRepository.postLocation(lat: lat, long: long);
//       print('---token----');
//       print(response);
//
//       if (response.status == Endpoint.SUCCESS) {
//         yield LocationState.success(
//             DioStatus(
//                 message: response.message, code: DioStatus.API_SUCCESS_NOTIFY));
//       } else {
//         yield LocationState.failure(
//             DioStatus(
//                 message: response.message, code: DioStatus.API_FAILURE_NOTIFY));
//       }
//     } catch (e) {
//       yield LocationState.failure(DioErrorUtil.handleError(e));
//     }
//   }
//
// }
