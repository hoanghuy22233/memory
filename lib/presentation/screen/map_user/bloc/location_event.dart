// import 'package:equatable/equatable.dart';
// import 'package:flutter/cupertino.dart';
//
// abstract class LocationEvent extends Equatable {
//   const LocationEvent();
//
//   @override
//   List<Object> get props => [];
// }
//
// class LocationChanged extends LocationEvent {
//   final double lat;
//   final double long;
//
//   LocationChanged({@required this.lat, @required this.long});
//
//   @override
//   List<Object> get props => [lat, long];
//
//   @override
//   String toString() {
//     return 'Location: $lat, $long}';
//   }
// }
//
//
// class LocationSubmitEvent extends LocationEvent {
//   final double lat;
//   final double long;
//
//   LocationSubmitEvent(
//       {@required this.lat, @required this.long});
//
//   @override
//   List<Object> get props => [lat, long];
//
//   @override
//   String toString() {
//     return 'LocationEvent{lat: $lat, long:$long}';
//   }
// }
//
//
//
