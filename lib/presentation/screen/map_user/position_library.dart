import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:memory_lifes/app/constants/color/color.dart';
import 'package:memory_lifes/app/constants/navigator/navigator.dart';
import 'package:memory_lifes/model/entity/position.dart';
import 'package:memory_lifes/presentation/common_widgets/widget_circle_progress.dart';
import 'package:memory_lifes/presentation/screen/map_user/position_get/bloc/bloc.dart';
import 'package:memory_lifes/presentation/screen/map_user/position_get/bloc/get_position_bloc.dart';
import 'package:memory_lifes/presentation/screen/menu/imageStock/bloc/bloc.dart';
import 'package:memory_lifes/presentation/screen/menu/imageStock/bloc/images_bloc.dart';
import 'package:memory_lifes/presentation/screen/menu/imageStock/bloc/images_state.dart';
import 'package:memory_lifes/utils/handler/http_handler.dart';

class PositionsLibrary extends StatefulWidget {
  @override
  GlobalKey<ScaffoldState> drawer;

  PositionsLibrary({this.drawer});

  _PositionsLibraryState createState() => _PositionsLibraryState();
}

class _PositionsLibraryState extends State<PositionsLibrary> {
  @override

  GoogleMapController mapController;
  static LatLng _center = const LatLng(20.836648, 106.694363);
  final Set<Marker> _markers = {};
  LatLng _currentMapPosition = _center;


  void _onCameraMove(CameraPosition position) {
    _currentMapPosition = position.target;
  }

  List<Positions> places;

  Completer<GoogleMapController> _controller = Completer();
  void initState() {
    super.initState();
    BlocProvider.of<PositionGetBloc>(context).add(LoadPositionGet());
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<PositionGetBloc, PositionGetState>(
      listener: (context, state) async {
        if (state.isLoading) {
          await HttpHandler.resolve(status: state.status);
        }

        if (state.isSuccess) {
          await HttpHandler.resolve(status: state.status);
        }

        if (state.isFailure) {
          await HttpHandler.resolve(status: state.status);
        }
      },
      child: BlocBuilder<PositionGetBloc, PositionGetState>(
        builder: (context, state) {
          return Scaffold(
            body: Container(
              child: RefreshIndicator(
                  onRefresh: () async {
                    BlocProvider.of<PositionGetBloc>(context).add(RefreshPositionGet());
                    await Future.delayed(Duration(seconds: 2));
                    return true;
                  },
                  color: AppColor.PRIMARY_COLOR,
                  backgroundColor: AppColor.THIRD_COLOR,
                  child: _buildContent(state)),
            ),
          );
        },
      ),
    );
  }


  Widget _buildContent(PositionGetState state) {
    if (state.position != null&&state?.position?.length!=0) {
      return Stack(
        children: <Widget>[
          GoogleMap(
              initialCameraPosition: CameraPosition(
                target: _center,
                zoom: 10.0,
              ),
              markers: _markers,
              onCameraMove: _onCameraMove,
              onMapCreated: (GoogleMapController controller) {
                _controller.complete(controller);
                setState(() {
                  for(int i=0; i < state.position.length; i++){
                    if(state.position[i].lat != null && state.position[i].long != null && state.position[i].lat != 0 && state.position[i].long != 0){
                      final marker = Marker(
                        markerId: MarkerId(state.position[i].toString()),
                        position: LatLng(state.position[i].lat, state.position[i].long),
                        infoWindow: InfoWindow(
                          title: i.toString(),
                          // onTap: () => AppNavigator.navigateNewsDetail(state.location[i].id),
                        ),
                        icon: BitmapDescriptor.defaultMarker,
                      );
                      _markers.add(marker);
                    }
                  }
                });
              }
          ),

        ],
      );
    } else {
      return Center(child: Text("Bạn chưa có ảnh!"));
    }
  }
}
