// import 'dart:async';
//
// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:memory_lifes/app/constants/navigator/navigator.dart';
// import 'package:memory_lifes/model/entity/new.dart';
// import 'package:memory_lifes/model/entity/position.dart';
// import 'package:memory_lifes/presentation/screen/menu/diary/bloc/post_bloc.dart';
// import 'package:memory_lifes/presentation/screen/menu/diary/bloc/post_state.dart';
// import 'package:memory_lifes/utils/handler/http_handler.dart';
//
// import 'blocPositon/position_bloc.dart';
// import 'blocPositon/position_state.dart';
//
//
// class MapUser extends StatefulWidget {
//   @override
//   _MapUserState createState() => _MapUserState();
// }
//
// class _MapUserState extends State<MapUser> {
//   GoogleMapController mapController;
//   static LatLng _center = const LatLng(20.836648, 106.694363);
//   final Set<Marker> _markers = {};
//   LatLng _currentMapPosition = _center;
//
//
//   void _onCameraMove(CameraPosition position) {
//     _currentMapPosition = position.target;
//   }
//
//   List<Positions> places;
//
//   Completer<GoogleMapController> _controller = Completer();
//
//   @override
//   void initState() {
//     super.initState();
//   }
//
//   @override
//
//   Widget build(BuildContext context) {
//     return BlocListener<PositionBloc, PositionState>(
//       listener: (context, state) async {
//         if (state.isLoading) {
//           await HttpHandler.resolve(status: state.status);
//         }
//
//         if (state.isSuccess) {
//           await HttpHandler.resolve(status: state.status);
//           places = state.location;
//           print(places);
//
//         }
//
//         if (state.isFailure) {
//           await HttpHandler.resolve(status: state.status);
//         }
//       },
//       child: BlocBuilder<PositionBloc, PositionState>(
//         builder: (context, state) {
//           return MaterialApp(
//             debugShowCheckedModeBanner: false,
//             home: Scaffold(
//               body: Stack(
//                 children: <Widget>[
//                   GoogleMap(
//                       initialCameraPosition: CameraPosition(
//                         target: _center,
//                         zoom: 10.0,
//                       ),
//                       markers: _markers,
//                       onCameraMove: _onCameraMove,
//                       onMapCreated: (GoogleMapController controller) {
//                         _controller.complete(controller);
//                         setState(() {
//                           for(int i=0; i < state.location.length; i++){
//                             if(state.location[i].lat != null && state.location[i].long != null && state.location[i].lat != 0 && state.location[i].long != 0){
//                               final marker = Marker(
//                                 markerId: MarkerId(state.location[i].toString()),
//                                 position: LatLng(state.location[i].lat, state.location[i].long),
//                                 infoWindow: InfoWindow(
//                                   title: i.toString(),
//                                   // onTap: () => AppNavigator.navigateNewsDetail(state.location[i].id),
//                                 ),
//                                 icon: BitmapDescriptor.defaultMarker,
//                               );
//                               _markers.add(marker);
//                             }
//                           }
//                         });
//                       }
//                   ),
//
//                 ],
//               ),
//             ),
//           );
//         },
//       ),
//     );
//   }
// }
