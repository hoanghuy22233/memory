import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'images.g.dart';

@JsonSerializable()
class Images extends Equatable {
  @JsonKey(name: "diary_id")
  int id;
  @JsonKey(name: "image")
  String image;

  Images(this.id, this.image);

  factory Images.fromJson(Map<String, dynamic> json) => _$ImagesFromJson(json);

  Map<String, dynamic> toJson() => _$ImagesToJson(this);

  @override
  List<Object> get props => [id, image];

  @override
  String toString() {
    return 'New{id:$id,image:$image }';
  }
}
