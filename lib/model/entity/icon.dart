class IconItem {
  final String icon, name;
  final int id;
  IconItem({this.id, this.icon, this.name});
}

List<IconItem> iconSet = [
  IconItem(
    id: 1,
    name: "Hạnh phúc",
    icon: "images/happy.png",
  ),
  IconItem(
    id: 2,
    name: "Bực bội",
    icon: "images/angry.png",
  ),
  IconItem(
    id: 3,
    name: "Lạnh lẽo",
    icon: "images/cold.png",
  ),
  IconItem(
    id: 4,
    name: "Đang yêu",
    icon: "images/lovely.png",
  ),
  IconItem(
    id: 5,
    name: "tuyệt vời",
    icon: "images/smile.png",
  ),
  IconItem(
    id: 5,
    name: "ngon lành",
    icon: "images/yummy.png",
  ),
];
