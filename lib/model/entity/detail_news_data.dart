
import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:memory_lifes/model/entity/new.dart';

part 'detail_news_data.g.dart';

@JsonSerializable()
class detailNewsData extends Equatable {
  News detail;

  detailNewsData(this.detail);

  factory detailNewsData.fromJson(Map<String, dynamic> json) =>
      _$detailNewsDataFromJson(json);

  Map<String, dynamic> toJson() => _$detailNewsDataToJson(this);

  @override
  List<Object> get props => [detail];
}
