// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'detail_news_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

detailNewsData _$detailNewsDataFromJson(Map<String, dynamic> json) {
  return detailNewsData(
    json['detail'] == null
        ? null
        : News.fromJson(json['detail'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$detailNewsDataToJson(detailNewsData instance) =>
    <String, dynamic>{
      'detail': instance.detail,
    };
