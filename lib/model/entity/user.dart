import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user.g.dart';

@JsonSerializable()
class User extends Equatable {
  @JsonKey(name: "name")
  String name;
  @JsonKey(name: "email")
  String email;
  @JsonKey(name: "gender")
  int gender;
  @JsonKey(name: "phone_number")
  String phoneNumber;
  @JsonKey(name: "date_of_birth")
  String dateOfBirth;
  @JsonKey(name: "type")
  int type;
  @JsonKey(name: "avatar")
  String avatar;
  @JsonKey(name: "background_image")
  String backgroundImage;
  @JsonKey(name: "status")
  int status;
  @JsonKey(name: "lat")
  double lat;
  @JsonKey(name: "long")
  double long;

  User(this.name, this.email, this.gender,this.phoneNumber, this.dateOfBirth, this.type,this.avatar,this.backgroundImage,
      this.status, this.lat, this.long);

  @override
  String toString() {
    return 'User{name: $name, email: $email, gender: $gender,phoneNumber:$phoneNumber,  dateOfBirth: $dateOfBirth, type: $type,avatar: $avatar,backgroundImage: $backgroundImage, status: $status, lat: $lat, long: $long}';
  }

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);

  @override
  List<Object> get props => [
    name,
    email,
    gender,
    phoneNumber,
    dateOfBirth,
    type,
    avatar,
    backgroundImage,
    status,
    lat,
    long
  ];
}
