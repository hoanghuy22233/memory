class Run {
  final String name;
  final int id;
  Run({this.id, this.name});
}

List<Run> runItem = [
  Run(
    id: 1,
    name: "Ẩm thực",
  ),
  Run(
    id: 2,
    name: "Dã ngoại",
  ),
  Run(
    id: 3,
    name: "Đi biển",
  ),
  Run(
    id: 4,
    name: "Học tập",
  ),
  Run(
    id: 5,
    name: "Leo núi",
  ),
  Run(
    id: 6,
    name: "Nghỉ ngơi",
  ),
  Run(
    id: 7,
    name: "Party",
  ),
  Run(
    id: 8,
    name: "Thể thao",
  ),
];
