// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'new.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

News _$NewsFromJson(Map<String, dynamic> json) {
  return News(
    json['id'] as int,
    json['title'] as String,
    json['content'] as String,
    json['type_status'] as int,
    json['cotype_workntent'] as int,
    json['image'] as String,
    json['created_at'] as String,
    (json['lat'] as num)?.toDouble(),
    (json['long'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$NewsToJson(News instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'content': instance.content,
      'type_status': instance.typeStatus,
      'cotype_workntent': instance.coTypeWork,
      'image': instance.image,
      'created_at': instance.date,
      'lat': instance.lat,
      'long': instance.long,
    };
