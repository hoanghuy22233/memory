// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'delete_post_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DeletePostResponse _$DeletePostResponseFromJson(Map<String, dynamic> json) {
  return DeletePostResponse()
    ..status = json['status'] as int
    ..message = json['message'] as String;
}

Map<String, dynamic> _$DeletePostResponseToJson(DeletePostResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
    };
