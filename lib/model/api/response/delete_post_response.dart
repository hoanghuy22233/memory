
import 'package:json_annotation/json_annotation.dart';
import 'package:memory_lifes/model/api/response/base_response.dart';

part 'delete_post_response.g.dart';

@JsonSerializable()
class DeletePostResponse extends BaseResponse {
  DeletePostResponse();

  factory DeletePostResponse.fromJson(Map<String, dynamic> json) =>
      _$DeletePostResponseFromJson(json);

  Map<String, dynamic> toJson() => _$DeletePostResponseToJson(this);
}
