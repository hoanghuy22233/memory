import 'package:json_annotation/json_annotation.dart';
import 'package:memory_lifes/model/api/response/base_response.dart';
import 'package:memory_lifes/model/entity/position.dart';

part 'location_response.g.dart';

@JsonSerializable()
class LocationResponse extends BaseResponse {
  List<Positions> data;

  LocationResponse(this.data);

  factory LocationResponse.fromJson(Map<String, dynamic> json) =>
      _$LocationResponseFromJson(json);

  Map<String, dynamic> toJson() => _$LocationResponseToJson(this);
}
