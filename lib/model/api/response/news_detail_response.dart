
import 'package:json_annotation/json_annotation.dart';
import 'package:memory_lifes/model/api/response/base_response.dart';
import 'package:memory_lifes/model/entity/detail_news_data.dart';
import 'package:memory_lifes/model/entity/new.dart';

part 'news_detail_response.g.dart';

@JsonSerializable()
class NewsDetailResponse extends BaseResponse {
  News data;

  NewsDetailResponse(this.data);

  factory NewsDetailResponse.fromJson(Map<String, dynamic> json) =>
      _$NewsDetailResponseFromJson(json);

  Map<String, dynamic> toJson() => _$NewsDetailResponseToJson(this);
}
