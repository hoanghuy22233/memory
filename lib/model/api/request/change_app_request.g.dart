// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'change_app_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChangeAppRequest _$ChangeAppRequestFromJson(Map<String, dynamic> json) {
  return ChangeAppRequest(
    oldPass: json['old_password'] as String,
    password: json['password'] as String,
    confirmPassword: json['confirm_password'] as String,
  );
}

Map<String, dynamic> _$ChangeAppRequestToJson(ChangeAppRequest instance) =>
    <String, dynamic>{
      'old_password': instance.oldPass,
      'password': instance.password,
      'confirm_password': instance.confirmPassword,
    };
