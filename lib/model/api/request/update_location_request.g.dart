// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'update_location_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UpdateLocationRequest _$UpdateLocationRequestFromJson(
    Map<String, dynamic> json) {
  return UpdateLocationRequest(
    lat: (json['lat'] as num)?.toDouble(),
    long: (json['long'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$UpdateLocationRequestToJson(
        UpdateLocationRequest instance) =>
    <String, dynamic>{
      'lat': instance.lat,
      'long': instance.long,
    };
