import 'package:json_annotation/json_annotation.dart';

part 'delete_post_request.g.dart';

@JsonSerializable()
class DeletePostRequest {
  @JsonKey(name: "diary_id")
  final int id;

  DeletePostRequest(this.id);

  factory DeletePostRequest.fromJson(Map<String, dynamic> json) =>
      _$DeletePostRequestFromJson(json);

  Map<String, dynamic> toJson() => _$DeletePostRequestToJson(this);

  @override
  String toString() {
    return 'DeletePostRequest{id: $id}';
  }
}
