// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rest_client.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _RestClient implements RestClient {
  _RestClient(this._dio, {this.baseUrl}) {
    ArgumentError.checkNotNull(_dio, '_dio');
    baseUrl ??= 'http://memorylife.xyz';
  }

  final Dio _dio;

  String baseUrl;

  @override
  Future<LoginResponse> loginApp(loginAppRequest) async {
    ArgumentError.checkNotNull(loginAppRequest, 'loginAppRequest');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(loginAppRequest?.toJson() ?? <String, dynamic>{});
    _data.removeWhere((k, v) => v == null);
    final _result = await _dio.request<Map<String, dynamic>>('/api/login',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'POST',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = LoginResponse.fromJson(_result.data);
    return value;
  }

  @override
  Future<LoginResponse> registerApp(registerAppRequest) async {
    ArgumentError.checkNotNull(registerAppRequest, 'registerAppRequest');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(registerAppRequest?.toJson() ?? <String, dynamic>{});
    _data.removeWhere((k, v) => v == null);
    final _result = await _dio.request<Map<String, dynamic>>('/api/register',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'POST',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = LoginResponse.fromJson(_result.data);
    return value;
  }

  @override
  Future<LoginResponse> changePassWord(changeAppRequest) async {
    ArgumentError.checkNotNull(changeAppRequest, 'changeAppRequest');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(changeAppRequest?.toJson() ?? <String, dynamic>{});
    _data.removeWhere((k, v) => v == null);
    final _result = await _dio.request<Map<String, dynamic>>(
        '/api/updatePassword',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'POST',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = LoginResponse.fromJson(_result.data);
    return value;
  }

  @override
  Future<ForgotPasswordResponse> forgotPassword(forgotPasswordRequest) async {
    ArgumentError.checkNotNull(forgotPasswordRequest, 'forgotPasswordRequest');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(forgotPasswordRequest?.toJson() ?? <String, dynamic>{});
    _data.removeWhere((k, v) => v == null);
    final _result = await _dio.request<Map<String, dynamic>>('/api/forgotPass',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'POST',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = ForgotPasswordResponse.fromJson(_result.data);
    return value;
  }

  @override
  Future<ForgotPasswordVerifyResponse> forgotPasswordVerify(
      forgotPasswordVerifyRequest) async {
    ArgumentError.checkNotNull(
        forgotPasswordVerifyRequest, 'forgotPasswordVerifyRequest');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(forgotPasswordVerifyRequest?.toJson() ?? <String, dynamic>{});
    _data.removeWhere((k, v) => v == null);
    final _result = await _dio.request<Map<String, dynamic>>(
        '/api/checkOtpInForgotPassword',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'POST',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = ForgotPasswordVerifyResponse.fromJson(_result.data);
    return value;
  }

  @override
  Future<ForgotPasswordResetResponse> forgotPasswordReset(
      forgotPasswordResetRequest) async {
    ArgumentError.checkNotNull(
        forgotPasswordResetRequest, 'forgotPasswordResetRequest');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(forgotPasswordResetRequest?.toJson() ?? <String, dynamic>{});
    _data.removeWhere((k, v) => v == null);
    final _result = await _dio.request<Map<String, dynamic>>(
        '/api/changeForgotPassword',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'POST',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = ForgotPasswordResetResponse.fromJson(_result.data);
    return value;
  }

  @override
  Future<NewsResponse> getNews() async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.request<Map<String, dynamic>>('/api/listDiary',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = NewsResponse.fromJson(_result.data);
    return value;
  }

  @override
  Future<ImagesResponse> getImages() async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.request<Map<String, dynamic>>(
        '/api/getListImageDiary',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = ImagesResponse.fromJson(_result.data);
    return value;
  }

  @override
  Future<ProfileResponse> getProfile() async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.request<Map<String, dynamic>>('/api/profile',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = ProfileResponse.fromJson(_result.data);
    return value;
  }

  @override
  Future<LocationResponse> getPosition() async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.request<Map<String, dynamic>>(
        '/api/getAllLatLongUser',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = LocationResponse.fromJson(_result.data);
    return value;
  }

  @override
  Future<NewsDetailResponse> getNewsDetail(newsId) async {
    ArgumentError.checkNotNull(newsId, 'newsId');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{r'diary_id': newsId};
    final _data = <String, dynamic>{};
    final _result = await _dio.request<Map<String, dynamic>>(
        '/api/getDetailDiary',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = NewsDetailResponse.fromJson(_result.data);
    return value;
  }

  @override
  Future<NewsResponse> removePost(deletePostRequest) async {
    ArgumentError.checkNotNull(deletePostRequest, 'deletePostRequest');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(deletePostRequest?.toJson() ?? <String, dynamic>{});
    _data.removeWhere((k, v) => v == null);
    final _result = await _dio.request<Map<String, dynamic>>('/api/deleteDiary',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'POST',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = NewsResponse.fromJson(_result.data);
    return value;
  }

  @override
  Future<UpdateNameResponse> updateName(updateNameRequest) async {
    ArgumentError.checkNotNull(updateNameRequest, 'updateNameRequest');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(updateNameRequest?.toJson() ?? <String, dynamic>{});
    _data.removeWhere((k, v) => v == null);
    final _result = await _dio.request<Map<String, dynamic>>(
        '/api/updateProfile',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'POST',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = UpdateNameResponse.fromJson(_result.data);
    return value;
  }

  @override
  Future<UpdatePhoneResponse> updatePhone(updatePhoneRequest) async {
    ArgumentError.checkNotNull(updatePhoneRequest, 'updatePhoneRequest');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(updatePhoneRequest?.toJson() ?? <String, dynamic>{});
    _data.removeWhere((k, v) => v == null);
    final _result = await _dio.request<Map<String, dynamic>>(
        '/api/updateProfile',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'POST',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = UpdatePhoneResponse.fromJson(_result.data);
    return value;
  }

  @override
  Future<UpdateEmailResponse> updateEmail(updateEmailRequest) async {
    ArgumentError.checkNotNull(updateEmailRequest, 'updateEmailRequest');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(updateEmailRequest?.toJson() ?? <String, dynamic>{});
    _data.removeWhere((k, v) => v == null);
    final _result = await _dio.request<Map<String, dynamic>>(
        '/api/updateProfile',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'POST',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = UpdateEmailResponse.fromJson(_result.data);
    return value;
  }

  @override
  Future<UpdateBirthdayResponse> updateBirthday(updateBirthdayRequest) async {
    ArgumentError.checkNotNull(updateBirthdayRequest, 'updateBirthdayRequest');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    _data.addAll(updateBirthdayRequest?.toJson() ?? <String, dynamic>{});
    _data.removeWhere((k, v) => v == null);
    final _result = await _dio.request<Map<String, dynamic>>(
        '/api/updateProfile',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'POST',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = UpdateBirthdayResponse.fromJson(_result.data);
    return value;
  }

  @override
  Future<UpdateAvatarResponse> updateAvatar(avatarFile) async {
    ArgumentError.checkNotNull(avatarFile, 'avatarFile');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = FormData();
    _data.files.add(MapEntry(
        'avatar',
        MultipartFile.fromFileSync(avatarFile.path,
            filename: avatarFile.path.split(Platform.pathSeparator).last)));
    final _result = await _dio.request<Map<String, dynamic>>(
        '/api/updateAvatar',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'POST',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = UpdateAvatarResponse.fromJson(_result.data);
    return value;
  }

  @override
  Future<UpdateBackgroundImageResponse> updateBackgroundImage(
      backgroundImageFile) async {
    ArgumentError.checkNotNull(backgroundImageFile, 'backgroundImageFile');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = FormData();
    _data.files.add(MapEntry(
        'background_image',
        MultipartFile.fromFileSync(backgroundImageFile.path,
            filename:
                backgroundImageFile.path.split(Platform.pathSeparator).last)));
    final _result = await _dio.request<Map<String, dynamic>>(
        '/api/updateBackgroundImage',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'POST',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = UpdateBackgroundImageResponse.fromJson(_result.data);
    return value;
  }

  @override
  Future<UpdateGenderResponse> genDer(superId) async {
    ArgumentError.checkNotNull(superId, 'superId');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = FormData();
    if (superId != null) {
      _data.fields.add(MapEntry('gender', superId.toString()));
    }
    final _result = await _dio.request<Map<String, dynamic>>(
        '/api/updateProfile',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'POST',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = UpdateGenderResponse.fromJson(_result.data);
    return value;
  }

  @override
  Future<UpdateLocationResponse> updateLocation(lat, long) async {
    ArgumentError.checkNotNull(lat, 'lat');
    ArgumentError.checkNotNull(long, 'long');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = FormData();
    if (lat != null) {
      _data.fields.add(MapEntry('lat', lat.toString()));
    }
    if (long != null) {
      _data.fields.add(MapEntry('long', long.toString()));
    }
    final _result = await _dio.request<Map<String, dynamic>>(
        '/api/updateLocation',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'POST',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = UpdateLocationResponse.fromJson(_result.data);
    return value;
  }

  @override
  Future<PostDiaryResponse> postDiary(
      storeImages, name, description, lat, long, icon, typeWork) async {
    ArgumentError.checkNotNull(storeImages, 'storeImages');
    ArgumentError.checkNotNull(name, 'name');
    ArgumentError.checkNotNull(description, 'description');
    ArgumentError.checkNotNull(lat, 'lat');
    ArgumentError.checkNotNull(long, 'long');
    ArgumentError.checkNotNull(icon, 'icon');
    ArgumentError.checkNotNull(typeWork, 'typeWork');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = FormData();
    _data.files.add(MapEntry(
        'image',
        MultipartFile.fromFileSync(storeImages.path,
            filename: storeImages.path.split(Platform.pathSeparator).last)));
    if (name != null) {
      _data.fields.add(MapEntry('title', name));
    }
    if (description != null) {
      _data.fields.add(MapEntry('content', description));
    }
    if (lat != null) {
      _data.fields.add(MapEntry('lat', lat.toString()));
    }
    if (long != null) {
      _data.fields.add(MapEntry('long', long.toString()));
    }
    if (icon != null) {
      _data.fields.add(MapEntry('type_status', icon.toString()));
    }
    if (typeWork != null) {
      _data.fields.add(MapEntry('type_work', typeWork.toString()));
    }
    final _result = await _dio.request<Map<String, dynamic>>('/api/postDiary',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'POST',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = PostDiaryResponse.fromJson(_result.data);
    return value;
  }

  @override
  Future<PostDiaryResponse> postDiaryNoImage(
      name, description, lat, long, icon, typeWork) async {
    ArgumentError.checkNotNull(name, 'name');
    ArgumentError.checkNotNull(description, 'description');
    ArgumentError.checkNotNull(lat, 'lat');
    ArgumentError.checkNotNull(long, 'long');
    ArgumentError.checkNotNull(icon, 'icon');
    ArgumentError.checkNotNull(typeWork, 'typeWork');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = FormData();
    if (name != null) {
      _data.fields.add(MapEntry('title', name));
    }
    if (description != null) {
      _data.fields.add(MapEntry('content', description));
    }
    if (lat != null) {
      _data.fields.add(MapEntry('lat', lat.toString()));
    }
    if (long != null) {
      _data.fields.add(MapEntry('long', long.toString()));
    }
    if (icon != null) {
      _data.fields.add(MapEntry('type_status', icon.toString()));
    }
    if (typeWork != null) {
      _data.fields.add(MapEntry('type_work', typeWork.toString()));
    }
    final _result = await _dio.request<Map<String, dynamic>>('/api/postDiary',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'POST',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = PostDiaryResponse.fromJson(_result.data);
    return value;
  }

  @override
  Future<PostDiaryResponse> editDiary(
      id, storeImages, name, description, lat, long, icon, typeWork) async {
    ArgumentError.checkNotNull(id, 'id');
    ArgumentError.checkNotNull(storeImages, 'storeImages');
    ArgumentError.checkNotNull(name, 'name');
    ArgumentError.checkNotNull(description, 'description');
    ArgumentError.checkNotNull(lat, 'lat');
    ArgumentError.checkNotNull(long, 'long');
    ArgumentError.checkNotNull(icon, 'icon');
    ArgumentError.checkNotNull(typeWork, 'typeWork');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = FormData();
    if (id != null) {
      _data.fields.add(MapEntry('diary_id', id.toString()));
    }
    _data.files.add(MapEntry(
        'image',
        MultipartFile.fromFileSync(storeImages.path,
            filename: storeImages.path.split(Platform.pathSeparator).last)));
    if (name != null) {
      _data.fields.add(MapEntry('title', name));
    }
    if (description != null) {
      _data.fields.add(MapEntry('content', description));
    }
    if (lat != null) {
      _data.fields.add(MapEntry('lat', lat.toString()));
    }
    if (long != null) {
      _data.fields.add(MapEntry('long', long.toString()));
    }
    if (icon != null) {
      _data.fields.add(MapEntry('type_status', icon.toString()));
    }
    if (typeWork != null) {
      _data.fields.add(MapEntry('type_work', typeWork.toString()));
    }
    final _result = await _dio.request<Map<String, dynamic>>('/api/editDiary',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'POST',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = PostDiaryResponse.fromJson(_result.data);
    return value;
  }

  @override
  Future<PostDiaryResponse> editDiaryNoImage(
      id, name, description, lat, long, icon, typeWork) async {
    ArgumentError.checkNotNull(id, 'id');
    ArgumentError.checkNotNull(name, 'name');
    ArgumentError.checkNotNull(description, 'description');
    ArgumentError.checkNotNull(lat, 'lat');
    ArgumentError.checkNotNull(long, 'long');
    ArgumentError.checkNotNull(icon, 'icon');
    ArgumentError.checkNotNull(typeWork, 'typeWork');
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = FormData();
    if (id != null) {
      _data.fields.add(MapEntry('diary_id', id.toString()));
    }
    if (name != null) {
      _data.fields.add(MapEntry('title', name));
    }
    if (description != null) {
      _data.fields.add(MapEntry('content', description));
    }
    if (lat != null) {
      _data.fields.add(MapEntry('lat', lat.toString()));
    }
    if (long != null) {
      _data.fields.add(MapEntry('long', long.toString()));
    }
    if (icon != null) {
      _data.fields.add(MapEntry('type_status', icon.toString()));
    }
    if (typeWork != null) {
      _data.fields.add(MapEntry('type_work', typeWork.toString()));
    }
    final _result = await _dio.request<Map<String, dynamic>>('/api/editDiary',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'POST',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = PostDiaryResponse.fromJson(_result.data);
    return value;
  }
}
