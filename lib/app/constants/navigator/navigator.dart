import 'package:get/get.dart';
import 'package:memory_lifes/model/entity/new.dart';
import 'package:memory_lifes/presentation/router.dart';

class AppNavigator {
  AppNavigator._();

  static navigateBack() async {
    Get.back();
  }

  static navigateSplash() async {
    var result = await Get.toNamed(BaseRouter.SPLASH);
    return result;
  }

  static navigateLogin() async {
    var result = await Get.toNamed(BaseRouter.LOGIN);
    return result;
  }

  static navigateBegin() async {
    var result = await Get.toNamed(BaseRouter.SPLASH_ONE);
    return result;
  }

  static navigateTwo() async {
    var result = await Get.toNamed(BaseRouter.SPLASH_TWO);
    return result;
  }

  static navigateThree() async {
    var result = await Get.toNamed(BaseRouter.SPLASH_THREE);
    return result;
  }

  static navigateOtp() async {
    var result = await Get.toNamed(BaseRouter.OTP);
    return result;
  }

  static navigateNavigation() async {
    var result = await Get.toNamed(BaseRouter.NAVIGATION);
    return result;
  }

  static navigateCreateDiary() async {
    var result = await Get.toNamed(BaseRouter.CREATE_DIARY);
    return result;
  }

  static navigateCreateNote() async {
    var result = await Get.toNamed(BaseRouter.CREATE_NOTE);
    return result;
  }

  static navigateChangePassword() async {
    var result = await Get.toNamed(BaseRouter.CHANGE_PASS);
    return result;
  }

  static navigateNewPassword() async {
    var result = await Get.toNamed(BaseRouter.NEW_PASS);
    return result;
  }

  static navigateForgotPassword() async {
    var result = await Get.toNamed(BaseRouter.FORGOT_PASS);
    return result;
  }

  static navigateSelectIcon() async {
    var result = await Get.toNamed(BaseRouter.SELECT_ICON);
    return result;
  }

  static navigateSelectRun() async {
    var result = await Get.toNamed(BaseRouter.SELECT_RUN);
    return result;
  }

  static navigateSearch() async {
    var result = await Get.toNamed(BaseRouter.SEARCH);
    return result;
  }


  static navigateForgotPasswordVerify({String username}) async {
    var result =
        await Get.toNamed(BaseRouter.FORGOT_PASSWORD_VERIFY, arguments: {
      'username': username,
    });
    return result;
  }

  static navigateDiaryUpdate(News post) async {
    var result =
        await Get.toNamed(BaseRouter.EDIT_DIARY, arguments: {'post': post});
    return result;
  }

  static navigateForgotPasswordReset({String username, String otpCode}) async {
    var result =
        await Get.toNamed(BaseRouter.FORGOT_PASSWORD_RESET, arguments: {
      'username': username,
      'otp_code': otpCode,
    });
    return result;
  }

  static navigateSignUp() async {
    var result = await Get.toNamed(BaseRouter.SIGN_UP);
    return result;
  }

  static navigateIntro() async {
    var result = await Get.toNamed(BaseRouter.INTRO);
    return result;
  }

  static navigateUserLocation() async {
    var result = await Get.toNamed(BaseRouter.GET_LOCATION);
    return result;
  }

  static navigateNewsDetail(int id) async {
    var result =
        await Get.toNamed(BaseRouter.NEWS_DETAIL, arguments: {'id': id});
    return result;
  }
}
